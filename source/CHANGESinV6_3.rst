CCP4 v6.3.0 Program Changes
===========================

INDEX

A list of general acknowledgements for testing CCP4 v6.3.0 can be found
`here <ACKNOWLEDGEMENTS_V6_1.html>`__.

--------------

New programs:

-  AIMLESS: scaling program, replacement for scala (0.1.26)
-  ProSMART: comparative analysis of protein structures and external
   restraint generation
-  AMPLE: ab initio molecular replacement
-  DIMPLE: difference map calculation for ligand location
-  ZANUDA: refinement data quality statistics
-  CMAPCUT: extract parts of maps
-  GESAMT: structure alignment
-  NAUTILUS: nucleic acid builder
-  COMIT: Composite omit maps
-  (VIEWHKL): mtz file display
-  (ArpNavigator): optional installation of ArpNavigator for ARP/wARP
   license holders

--------------

Program changes:

-  POINTLESS: updated to 1.6.18
-  REFMAC: updated to 5.7.0029
-  PHASER: updated to 2.5.1
-  MrBUMP: updated to include phaser.sculptor models
-  IPMOSFLM: updated to 7.0.9
-  OpenAstexViewer: v3.0 replaces AstexViewer

--------------

Graphical User Interface:

-  automation module
-  jligand interface
-  aimless interface
-  zanuda interface
-  prosmart integrated with refmac
-  viewhkl option for viewing mtz files
-  nautilus nucleic acid building pipeline
-  composite omit interface
-  imosflm updated to 1.0.7
