PROCHECK v.3.5
==============

Programs to check the Stereochemical Quality of Protein Structures
------------------------------------------------------------------

|image0|

Operating manual
================

|image1|

`Roman A Laskowski <http://www.biochem.ucl.ac.uk/~roman/>`__, `Malcolm W
MacArthur <http://www.biochem.ucl.ac.uk/~mac/>`__, David K Smith, `David
T Jones <http://globin.bio.warwick.ac.uk/~jones/>`__, `E Gail
Hutchinson <http://www.biochem.ucl.ac.uk/~gail/>`__, A Louise Morris,
`David S Moss <http://www.cryst.bbk.ac.uk/~ubcg05m/home.html>`__ &
`Janet M Thornton <http://www.biochem.ucl.ac.uk/~thornton/>`__

*April 1998*

--------------

Contents
--------

-  1. `Introduction <man1.html>`__
-  2. `How to run **PROCHECK** <man2.html>`__
-  3. `Input requirements <man3.html>`__
-  4. `**PROCHECK** outputs <man4.html>`__
-  5. `Customizing the **PROCHECK** plots <man5.html>`__
-  6. `Viewing G-factors in 3D <man6.html>`__

-  `References <manrefs.html>`__

-  `Appendix A <manappa.html>`__ - Stereochemical parameters
-  `Appendix B <manappb.html>`__ - Brookhaven **PDB** file format
-  `Appendix C <manappc.html>`__ - Program descriptions
-  `Appendix D <manappd.html>`__ - Residue-by-residue listing
-  `Appendix E <manappe.html>`__ - *G*-factors

-  `Sample plots <examples/index.html>`__

-  `Acknowledgements <manack.html>`__

--------------

**| | | | `PROCHECK Home
Page <http://www.biochem.ucl.ac.uk/~roman/procheck/procheck.html>`__**
(at UCL)

.. |image0| image:: procheck_long.gif
.. |image1| image:: qu_book.gif
.. | | image:: uupb.gif
   :target: http://www.biochem.ucl.ac.uk/~roman/procheck/procheck.html
.. | | image:: 12p.gif

