Stereochemical quality of protein structure coordinates.
========================================================

Morris AL; MacArthur MW; Hutchinson EG; Thornton JM
---------------------------------------------------

Department of Biochemistry and Molecular Biology, University College,
London, England.

**Proteins 12: 345-64 (1992)**

| **Abstract**
| Methods have been developed to assess the stereochemical quality of
  any protein structure both globally and locally using various
  criteria. Several parameters can be derived from the coordinates of a
  given structure. Global parameters include the distribution of phi,
  psi and chi 1 torsion angles, and hydrogen bond energies. There are
  clear correlations between these parameters and resolution; as the
  resolution improves, the distribution of the parameters becomes more
  clustered. These features show a broad distribution about ideal values
  derived from high-resolution structures. Some structures have tightly
  clustered distributions even at relatively low resolutions, while
  others show abnormal scatter though the data go to high resolution.
  Additional indicators of local irregularity include proline phi
  angles, peptide bond planarities, disulfide bond lengths, and their
  chi 3 torsion angles. These stereochemical parameters have been used
  to generate measures of stereochemical quality which provide a simple
  guide as to the reliability of a structure, in addition to the most
  important measures, resolution and R-factor. The parameters used in
  this evaluation are not novel, and are easily calculated from
  structure coordinates. A program suite is currently being developed
  which will quickly check a given structure, highlighting unusual
  stereochemistry and possible errors.
