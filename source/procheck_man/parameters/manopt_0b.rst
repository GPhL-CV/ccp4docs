|previous page| | | |plot parameters| | | |next page| | |\ *PROCHECK parameters*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

--------------

Which plots to produce
======================

There are **10** parameters defining which plots are required, one for
each of the **10** plots:-

--------------

::

    Which plots to produce
    ----------------------
    Y     <-  1. Ramachandran plot (Y/N)?
    Y     <-  2. Gly & Pro Ramachandran plots (Y/N)?
    Y     <-  3. Chi1-Chi2 plots (Y/N)?
    Y     <-  4. Main-chain parameters (Y/N)?
    Y     <-  5. Side-chain parameters (Y/N)?
    Y     <-  6. Residue properties (Y/N)?
    Y     <-  7. Main-chain bond length distributions (Y/N)?
    Y     <-  8. Main-chain bond angle distributions (Y/N)?
    Y     <-  9. RMS distances from planarity (Y/N)?
    Y     <- 10. Distorted geometry plots (Y/N)?

--------------

Description:-
-------------

Each of the **10** plots can be individually switched on or off by
entering **Y** or **N** next to that plot, as required.

--------------

|previous page| | | |plot parameters| | | |next page| | | *PROCHECK parameters*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. |previous page| image:: ../leftr.gif
   :target: manopt_0a.html
.. | | image:: ../12p.gif
.. |plot parameters| image:: ../uupr.gif
   :target: ../man5.html
.. |next page| image:: ../rightr.gif
   :target: manopt_01.html
.. |plot parameters| image:: ../uupr.gif
   :target: ../man5.html
.. |next page| image:: ../rightr.gif
   :target: manopt_01.html
