|previous page| | | |plot parameters| | | |next page| | |\ *PROCHECK parameters*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

--------------

2. Gly & Pro Ramachandran plots
===============================

There are **8** parameters defining the `**Gly & Pro Ramachandran
plots** <../examples/plot_02.html>`__ (which by default actually print
Ramachandrans for each of the 20 different residue types), as follows:-

--------------

::

    2. Gly & Pro Ramachandran plots
    -------------------------------
    -3.0  <- Cut-off value for labelling of residues
    Y     <- Plot all 20 Ramachandran plots (Y/N)?
    N     <- Produce a COLOUR PostScript file (Y/N)?
    WHITE        <- Background colour
    CREAM        <- Lightest shaded regions on plots
    GREEN        <- Darkest shaded regions on plots
    YELLOW       <- Colour of markers in favourable regions
    RED          <- Colour of markers in unfavourable regions

--------------

Description of options:-
------------------------

-  **Cut-off value for labelling of residues** - The first option
   defines the *G*-factor values below which residues on the
   Ramachandran plot will be labelled. The *G*-factor gives a measure of
   how far from the \`normal' regions of the plot each residue lies. Low
   *G*-factors indicate residues in unlikely conformations.
-  **Plot all 20 Ramachandran plots** - The second option allows you to
   show a separate phi-psi Ramachandran plot for each of the 20
   different residue types, rather than just for Gly and Pro. The
   default setting is **Y**.
-  **Produce a COLOUR PostScript file** - This option defines whether a
   colour or black-and-white plot is required. Note that if the `Colour
   all plots <manopt_0a.html>`__ option is set to **Y**, a colour
   PostScript file will be produced irrespective of the setting here.

   The colour definitions on the following lines use the \`names' of
   each colour as defined in the colour table at the bottom of the
   parameter file (see `Colours <manopt_cols.html>`__). If the name of a
   colour is mis-spelt, or does not appear in the colour table, then
   **white** will be taken instead. Each colour can be altered to suit
   your taste and aesthetic judgement, as described in the
   `Colours <manopt_cols.html>`__ section.

-  **Background colour** - This option defines the background colour of
   the page on which the plots are drawn.
-  **Colours** - The various additional colours on the lines that follow
   define the colours for the lightest- and darkest-shaded regions of
   the plot, and also the colours of the residue markers in the
   favourable and unfavourable regions.

--------------

|previous page| | | |plot parameters| | | |next page| | | *PROCHECK parameters*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. |previous page| image:: ../leftr.gif
   :target: manopt_01.html
.. | | image:: ../12p.gif
.. |plot parameters| image:: ../uupr.gif
   :target: ../man5.html
.. |next page| image:: ../rightr.gif
   :target: manopt_03.html
.. |plot parameters| image:: ../uupr.gif
   :target: ../man5.html
.. |next page| image:: ../rightr.gif
   :target: manopt_03.html
