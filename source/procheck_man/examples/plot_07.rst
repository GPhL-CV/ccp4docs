|previous listing| | | |plots listing| | | |next plot| | |\ *PROCHECK sample plots*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

--------------

Plot 7. Main-chain bond length distributions
============================================

|image4|

Description
-----------

The histograms on this plot show the distributions of each of the
different main-chain bond lengths in the structure. The **solid** line
in the centre of each plot corresponds to the **small-molecule mean
value**, while the **dashed** lines either side show the
**small-molecule standard deviation**, the data coming from `Engh &
Huber (1991) <../manrefs.html#ENGH>`__.

Highlighted bars correspond to values more than **2.0** standard
deviations from the mean, though the value of **2.0** can be changed by
editing the **procheck.prm** file.

If any of the histogram bars lie off the graph, to the left or to the
right, a large arrow indicates the number of these outliers (as in the
**C-O** plot above).

Significant outliers are shown on the **`Distorted geometry
plots <plot_10.html>`__**.

Options
-------

The main options for the plot are:-

-  Number of standard deviations for highlighting outliers (default is
   **2.0**).
-  The plot can be in colour or black-and-white.

These options can be altered by editing the parameter file,
**procheck.prm**, as described `here <../parameters/manopt_07.html>`__.

--------------

|previous listing| | | |plots listing| | | |next plot| | | *PROCHECK sample plots*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. |previous listing| image:: ../leftr.gif
   :target: plot_06.html
.. | | image:: ../12p.gif
.. |plots listing| image:: ../uupr.gif
   :target: index.html
.. |next plot| image:: ../rightr.gif
   :target: plot_08.html
.. |image4| image:: plot_07.gif
   :width: 306px
   :height: 434px
   :target: plot_07.gif
.. |plots listing| image:: ../uupr.gif
   :target: index.html
.. |next plot| image:: ../rightr.gif
   :target: plot_08.html
