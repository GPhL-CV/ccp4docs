|image0|

.. raw:: html

   <div align="center">

**P I S A**

.. raw:: html

   </div>

|image1|

.. raw:: html

   <div align="center">

*Protein Interfaces, Surfaces and Assemblies*

.. raw:: html

   </div>

.. raw:: html

   <div class="navbar">

`Contents <index.html>`__ • Frequently Asked Questions

.. raw:: html

   </div>

 

#. `Can I use PISA on macromolecular structures without crystal
   data? <#no-crystal>`__
#. `Why does PISA not give the correct oligomeric state for my
   structure? <#no-my-complex>`__
#. `I applied rotation to my structure, and figures for surface area in
   PISA have changed. Why? <#asa-errors>`__

 

--------------

***Can I use PISA on macromolecular structures without crystal data?***
    | 
    | Yes. If crystal data (space symmetry group and cell parameters) is
      not found in the input file, PISA will assess only interfaces that
      are found between `monomeric
      units <qtpisa-glossary.html#monomeric-unit>`__ in their positions
      as given in the file. This is fully equivalent to setting space
      group to P1 and using sufficiently large cell dimensions such that
      no interfaces can be formed between adjucent cells.

    Even if crystal data is given, PISA provides analysis of oligomeric
    states as if it was not, in addition to the "proper" analysis. The
    corresponding results are found in the "No crystal" tab of the
    `Control Panel <qtpisa-glossary.html#control-panel>`__.

     

***Why does PISA not give the correct `oligomeric
state <qtpisa-glossary.html#oligomeric-state>`__ for my structure?***
    This is expected to happen in some 5-10% of all instances, and may
    be due to a number of reasons:

    I.  There are natural limits for any computational procedure to
        match experimental observations, specifically in PISA:
         

        a. approximations in theoretical models describing
           macromolecular interactions.
        b. imperfect calibration of empirical and semiempirical
           parameters
        c. using the concept of "average conditions", where complexes
           are detected, with unspecified pH, salinity, ionic strength,
           presence of other solutes and other factors. "Average
           conditions" are implicitely hardcoded in a set of calibrated
           parameters of macromolecular interactions, used by PISA, and
           they cannot be good in all cases.

    II. There could also be reasons on the experimental side:
         

        a. poor resolution or refinement. PISA is sensitive to the
           quality of structural data in
           `interface <qtpisa-glossary.html#interface>`__ areas.
           Relatively modest (0.5-1Å) displacements may have a
           considetable effect on the binding energy calculations, by
           this leading PISA to wrong conclusions.
        b. `oligomeric state <qtpisa-glossary.html#oligomeric-state>`__
           may be measured in chemical conditions that are sufficiently
           different from those in crystallisation buffer to induce
           changes in the state.
        c. crystallisation was essentially stimulated by using strong
           precipitation agents, such as metal ions, which effectively
           produce additional binding. As a result, PISA can suggest a
           higher `oligomeric
           state <qtpisa-glossary.html#oligomeric-state>`__. In order to
           check this hypothesis, exclude precipitation agents from PISA
           analyses, as explained `here <qtpisa-howtorun.html>`__.
        d. crystallised protein is a mutant. Sometimes, even moderate
           mutations may lead to changing oligomeric state, even if
           those mutations are not found in
           `interface <qtpisa-glossary.html#interface>`__ areas. Removal
           part of protein sequence may change the oligomeric state,
           too.
        e. part of structure is disordered and is not seen in electron
           density. For PISA, this is equivalent to the modification
           (mutation) of the protein, which may result in changing
           `oligomeric state <qtpisa-glossary.html#oligomeric-state>`__.
        f. `oligomeric state <qtpisa-glossary.html#oligomeric-state>`__
           corresponds to weakly bound
           `complex <qtpisa-glossary.html#complex>`__. Such complex may
           get re-assembled in the course of crystallisation, with
           probability depending exponentially on the free Gibbs energy
           of dissociation (*cf* the corresponding publication (`E.
           Krissinel (2010) <qtpisa-citations.html>`__).
        g. the particular `oligomeric
           state <qtpisa-glossary.html#oligomeric-state>`__ is expected
           to be there only in certain concentration range. PISA's
           summary pages present
           `assemblies <qtpisa-glossary.html#assembly>`__ in "average
           conditions", which include unspecified concentration.
           However, by inspecting the concentration profile of `assembly
           stock <qtpisa-asmstock.html>`__, the correct answer may be
           found sometimes.

     
***I applied rotation to my structure, and figures for surface area in
PISA have changed. Why?***
    This effect is due to the approximate nature of surface area
    calculations and is normal.
    `Surface <qtpisa-glossary.html#surface-area>`__ and `interface
    areas <qtpisa-glossary.html#interface-area>`__ are calculated as a
    sum of finite-size fragments on atomic spheres that are accessible
    to solvent. These fragments are calculated in the "laboratory
    coordinate system" and, therefore, their configuration depends
    slightly on the orientation of structures. The fragments are chosen
    to be sufficiently small, such that surface area variations are
    found well within the physically reasonable limits of the surface
    area definition. Normally, the effect should be within 0.01% or
    less.
     

.. |image0| image:: qtpisa.png
   :width: 64px
   :height: 64px
.. |image1| image:: reference.png
   :width: 64px
   :height: 64px
