|image0|

.. raw:: html

   <div align="center">

**P I S A**

.. raw:: html

   </div>

|image1|

.. raw:: html

   <div align="center">

*Protein Interfaces, Surfaces and Assemblies*

.. raw:: html

   </div>

.. raw:: html

   <div class="navbar">

`Contents <index.html>`__ • Result Pages

.. raw:: html

   </div>

QtPISA output is arranged in pages, which are displayed in the `Working
Area <qtpisa-glossary.html#working-area>`__, on the right from the
`Control Panel <qtpisa-controlpanel.html>`__. Only one page, selected in
the `Result Tree <qtpisa-glossary.html#pisa-result-tree>`__ of the
Panel, may be displayed at time.

A typical Result Page contains one or several tables with various data;
a few pages also contain graphical output. Pages are groupped into a
hierarchical structure, represented by the `Result
Tree <qtpisa-glossary.html#pisa-result-tree>`__, according to their
contents.

| Note that some of QtPISA controls, such as buttons in the
  `Toolbar <qtpisa-toolbar.html>`__ and certain main menu items (such as
  Print/Save/View/Help), work in the context of the currently selected
  result page. This means that, e.g., View may address to either a
  `Monomeric Unit <qtpisa-glossary.html#monomeric-unit>`__, or an
  `Interface <qtpisa-glossary.html#interface>`__, or an
  `Assembly <qtpisa-glossary.html#assembly>`__, depending on the
  currently displayed page.
|  

--------------

| ***See also***

`Input data page <qtpisa-datapage.html>`__

`List of monomers <qtpisa-monlistpage.html>`__

-  `Monomer details <qtpisa-mondetpage.html>`__

`List of interfaces <qtpisa-intflistpage.html>`__

`Interface details and interaction radar <qtpisa-intfdetpage.html>`__

-  `Chemical bonds <qtpisa-intfbondspage.html>`__
-  `Interfacing residues <qtpisa-intfrespage.html>`__

Assembly stock

Crystal splits

Stable splits

-  Assembly details

Metastable splits

Unstable splits

No-crystal analysis

.. |image0| image:: qtpisa.png
   :width: 64px
   :height: 64px
.. |image1| image:: reference.png
   :width: 64px
   :height: 64px
