CMAKEREFERENCE (CCP4: Supported Program)
========================================

NAME
----

**cmakereference** - Generate reference structure for pirate/buccaneer.

SYNOPSIS
--------

| **cmakereference** **-pdbid** *accession-code* **-pdbin** *filename*
  **-cifin** *filename* **-colin-fo** *colpath* **-mtzout** *filename*
  **-pdbout** *filename* **-resolution** *reso/A* **[-stdin]**
| [`Keyworded input <#keywords>`__]

DESCRIPTION
-----------

'cmakereference' is used to generate reference structures for use by the
programs pirate/buccaneer. Three standard reference structures are now
provided as part of the distribution (based on 1AJR, 1JZ8 and 1TQW).
These are selected automatically by pirate/buccaneer, and are suitable
for common light-atom protein structures with varying solvent content
and resolution up to 1.85A. For exotic cases and high resolution cases
you will have to provide your own reference structures using
cmakereference.

The accession code is compulsory. If no -pdbin / -cifin for the
deposited structure are given, they are fetched by ftp if possible.

INPUT/OUTPUT FILES
------------------

-pdbin

Input PDB file containing the deposited coordinates. makereference
accepts either .ent or .ent.Z files. If you download a .ent.gz file,
then you will need to gunzip it first.

-cifin

Input mmCIF or MTZ file containing the deposited reflections. For mmCIF
reflection files, makereference accepts either .ent or .ent.Z files. If
you download a .ent.gz file, then you will need to gunzip it first. If
the (uncompressed) file extension is not .ent, then makereference will
try to open it as an MTZ file, in which case you will probably need to
specify `-colin-fo <#colin>`__ as well.

-mtzout

Output MTZ file containing the data for the reference structure. The
columns are F, sigF, and a set of Hendrickson-Lattman (HL) coefficients
describing the calculated phases from the final model. This becomes
-mtzin-ref for Pirate/Buccaneer.

-pdbout

Output PDB file containing the final model for the reference structure.
This becomes -pdbin-ref for Buccaneer.

KEYWORDED INPUT
---------------

See `Note on keyword input <#notekeywords>`__.

-colin-fo *colpath*
~~~~~~~~~~~~~~~~~~~

If cifin is an MTZ file, then specify the columns with this keyword. For
example: -colin-fo '/\*/\*/[FP,SIGFP]'

-resolution *reso*
~~~~~~~~~~~~~~~~~~

| Default: taken from cifin
| Resolution to truncate the data at.

Note on keyword input:
^^^^^^^^^^^^^^^^^^^^^^

Keywords may appear on the command line or, by specifying the '-stdin'
flag, on standard input. In the latter case, one keyword is given per
line and the '-' is optional, and the rest of the line is the argument
of that keyword if required, so quoting is not used in this case. End
input with <Ctrl-D> (END is not recognised).

AUTHOR
------

Kevin Cowtan, York.

SEE ALSO
--------

`pirate <cpirate.html>`__, `buccaneer <cbuccaneer.html>`__,
`cif2mtz <cif2mtz.html>`__
