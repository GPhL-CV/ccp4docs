.. raw:: html

   <div id="globalWrapper">

.. raw:: html

   <div id="column-content">

.. raw:: html

   <div id="content">

.. rubric:: Randy J. Read
   :name: randy-j.-read
   :class: firstHeading

.. raw:: html

   <div id="bodyContent">

.. rubric:: From Phaserwiki
   :name: siteSub

.. raw:: html

   <div id="contentSub">

.. raw:: html

   </div>

Our research is in two general areas. First, we develop new theory and
new methods in protein crystallography, based largely on the principle
of maximum likelihood. Second, we apply the technique of protein
crystallography to the study of medically-relevant proteins.
Crystallographic theory and methods

Many problems in crystallography can be approached in terms of the
principle of maximum likelihood. Intuitively, the principle of maximum
likelihood means that the best model is the one that agrees best with
the data. Agreement is measured by the probability of measuring the
data. So likelihood is the probability that you would measure the set of
observed data, given whatever model you have of the experiment.

Probability distributions in experimental science are often Gaussian,
and then likelihood becomes equivalent to least squares. In the case of
crystallography, the (phased) structure factors have Gaussian
distributions, but the phase information is lost when we measure
intensities, and the distributions of the measurements are no longer
Gaussian. This is why it is necessary to go to first principles and
apply likelihood.

It turns out that likelihood provides a good basis for a variety of
crystallographic experiments, including molecular replacement, model
phasing, experimental phasing with isomorphous replacement and anomalous
dispersion, and model refinement. We have developed or contributed to
likelihood-based programs in all these areas: model phasing (SIGMAA),
molecular replacement (Beast, Phaser), experimental phasing (Phaser) and
refinement (CNS).

Much of the background material can be obtained from the web pages from
the `macromolecular crystallography
course <http://www-structmed.cimr.cam.ac.uk/course.html>`__ that was
presented here in the 1999-2000 academic year. In addition, here are
links to overheads presented at lectures at a workshop on protein
structure refinement held in September 1997 in York.

-  `Practical aspects of maximum
   likelihood <http://www-structmed.cimr.cam.ac.uk/Personal/randy/html2/rjr2.html>`__
-  `Model-phased maps, bias and
   likelihood <http://www-structmed.cimr.cam.ac.uk/Personal/randy/html3/rjr3.html>`__

Currently, our efforts in crystallographic methods are focused on the
development of our program Phaser. More information can be found on the
Phaser website. Structural studies

In our structural work, we focus on the structures of medically-relevant
proteins, particularly those for which the structure may be useful in
the development of new therapies. For a number of years we have worked
on bacterial toxins, particularly pertussis toxin and the `Shiga-like
toxins <http://www-structmed.cimr.cam.ac.uk/Personal/randy/SLT/index.html>`__
involved in E. coli food poisoning (often called "hamburger disease" in
North America). Most recently, we have been working to understand the
enzymatic mechanism of pertussis toxin.

In an area largely led by Dr. Aiwu Zhou, we are working on a number of
serpins, with an emphasis on non-inhibitory members of the family. These
include the hormone-binding globulins and angiotensinogen.

`Online
publications <http://www-structmed.cimr.cam.ac.uk/Personal/randy/pubs/pubs.html>`__

.. raw:: html

   <div class="printfooter">

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="column-one">

.. raw:: html

   <div id="p-logo" class="portlet">

` <http://www.phaser.cimr.cam.ac.uk/index.php/Phaser_Crystallographic_Software>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-" class="portlet">

.. rubric:: 
   :name: section

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-PhaserWiki-Home">

   .. raw:: html

      </div>

   `PhaserWiki Home <../../../../index.html>`__
-  

   .. raw:: html

      <div id="n-Releases">

   .. raw:: html

      </div>

   `Releases <../../../../articles/r/e/l/Releases.html>`__
-  

   .. raw:: html

      <div id="n-Downloads">

   .. raw:: html

      </div>

   `Downloads <../../../../articles/d/o/w/Downloads.html>`__
-  

   .. raw:: html

      <div id="n-Manuals">

   .. raw:: html

      </div>

   `Manuals <../../../../articles/m/a/n/Manuals.html>`__
-  

   .. raw:: html

      <div id="n-Tutorials">

   .. raw:: html

      </div>

   `Tutorials <../../../../articles/t/u/t/Tutorials.html>`__
-  

   .. raw:: html

      <div id="n-FAQ">

   .. raw:: html

      </div>

   `FAQ <../../../../articles/f/a/q/FAQ_1fe9.html>`__
-  

   .. raw:: html

      <div id="n-Top-Ten-Tips">

   .. raw:: html

      </div>

   `Top Ten Tips <../../../../articles/t/o/p/Top_Ten_Tips_3fcd.html>`__
-  

   .. raw:: html

      <div id="n-Publications">

   .. raw:: html

      </div>

   `Publications <../../../../articles/p/u/b/Publications.html>`__
-  

   .. raw:: html

      <div id="n-External-Links">

   .. raw:: html

      </div>

   `External
   Links <../../../../articles/e/x/t/External_Links_ff0e.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-users" class="portlet">

.. rubric:: users
   :name: users

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-MR-Phasing">

   .. raw:: html

      </div>

   `MR
   Phasing <../../../../articles/m/o/l/Molecular_Replacement_a23a.html>`__
-  

   .. raw:: html

      <div id="n-SAD-Phasing">

   .. raw:: html

      </div>

   `SAD
   Phasing <../../../../articles/e/x/p/Experimental_Phasing_5400.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-developers" class="portlet">

.. rubric:: developers
   :name: developers

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Python-Interface">

   .. raw:: html

      </div>

   `Python
   Interface <../../../../articles/p/y/t/Python_Interface_5961.html>`__
-  

   .. raw:: html

      <div id="n-Contact-Developers">

   .. raw:: html

      </div>

   `Contact Developers <../../../../articles/c/o/n/Contact.html>`__
-  

   .. raw:: html

      <div id="n-Developer-Pages">

   .. raw:: html

      </div>

   `Developer Pages <../../../../articles/d/e/v/Developers.html>`__
-  

   .. raw:: html

      <div id="n-Licences">

   .. raw:: html

      </div>

   `Licences <../../../../articles/l/i/c/Licences.html>`__
-  

   .. raw:: html

      <div id="n-SVN-Access">

   .. raw:: html

      </div>

   `SVN Access <../../../../articles/s/v/n/SVN_Repository_93a5.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   <div id="footer">

.. raw:: html

   </div>

.. raw:: html

   </div>
