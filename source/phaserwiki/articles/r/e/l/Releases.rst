.. raw:: html

   <div id="globalWrapper">

.. raw:: html

   <div id="column-content">

.. raw:: html

   <div id="content">

.. rubric:: Releases
   :name: releases
   :class: firstHeading

.. raw:: html

   <div id="bodyContent">

.. rubric:: From Phaserwiki
   :name: siteSub

.. raw:: html

   <div id="contentSub">

.. raw:: html

   </div>

.. raw:: html

   <div id="mw-content-text" class="mw-content-ltr" lang="en" dir="ltr">

.. raw:: html

   <div style="margin-left: 25px; float: right;">

.. raw:: html

   <div id="toc" class="toc">

.. raw:: html

   <div id="toctitle">

.. rubric:: Contents
   :name: contents

.. raw:: html

   </div>

-  `1 2016 <#2016>`__
-  `2 2015 <#2015>`__
-  `3 2014 <#2014>`__
-  `4 2013 <#2013>`__
-  `5 2012 <#2012>`__
-  `6 2011 <#2011>`__
-  `7 2010 <#2010>`__
-  `8 2009 <#2009>`__
-  `9 2008 <#2008>`__
-  `10 2007 <#2007>`__
-  `11 2006 <#2006>`__
-  `12 2005 <#2005>`__
-  `13 2004 <#2004>`__
-  `14 2003 <#2003>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. rubric:: 2016
   :name: section

 September 2016
    **Phaser-2.7.14**

-  `Phaser-2.7 series
   changelog <../../../../articles/p/h/a/Phaser-2.7_series_changelog.html>`__

.. rubric:: 2015
   :name: section-1

 September 2015
    **Phaser-2.6.0 update released with Phenix Official Release 1.10**
    **Phaser-2.6.0 update released with CCP4 7.0**

.. rubric:: 2014
   :name: section-2

 11th May 2014
    **Phaser-2.5.6 update released with Phenix Official Release 1.9**
    **Phaser-2.5.6 update released with CCP4 6.4 and update 6.4.0-017**

 14th February 2014
    **Phaser-2.5.6 released with CCP4-6.4 update 008**

    -  Rotation Gyration Refinement (MODE GYRE) added
    -  Template matched solutions moved to template solution origin
    -  Phased Translation Function
    -  Exit value returned from executable and phenix.python wrapper

.. rubric:: 2013
   :name: section-3

 7th September 2013
    **Phaser-2.5.5 released with Phenix-1.8.3**

 23rd July 2013
    **Reduce size of phaser distribution from 80MBytes to 10MBytes**

    -  Shifted regression tests to another svn repository
    -  Papers no longer distributed with documentation

 18th June 2013
    **Speed Enhancements**

    -  Deep search peaks stored and used on the fly if required
    -  Top peak in FTF used to purge on the fly
    -  Bugfixes to amalgamation

 21st March 2013
    **KILL feature added**

    -  Phaser will be killed on a process if a file of a given filename
       is present

.. rubric:: 2012
   :name: section-4

14th August 2012
    **Bugfix**

    -  Packing test was incorrectly applied to reoriented molecules
       where there was PTNCS

6th August 2012
    **New Feature**

    -  Scattering from HETATM records that represent modified amino
       acids (e.g. SME SeMet) included so that maps no longer show holes
       for these HETATM records in the model.

 16th July 2012
    **Phaser-2.5.1 released with CCP4-6.3**

 27th June 2012 - date (active development)
    **Phaser-2.5.1 released with Phenix nightly builds**

    -  Features of Phaser-2.5.1

    -  **Domain Analysis** NMA used to identify domains in proteins

 23rd June 2012
    **Phaser-2.5.0 released with Phenix-1.8**

 24th April 2012
    Major update of Phaserwiki to reflect powerful new Phaser
    functionality (Phaser-2.5.0)

 23rd December 2011 - 23rd June 2012
    **Phaser-2.5.0 released with Phenix nightly builds**

    -  Features of Phaser-2.5.0

    -  Wiki **documentation** (using *wikidump*) distributed with source
       code
    -  **Expected LLG** used to optimize search strategy in FAST mode
    -  Babinet **sovent correction** terms can be refined.
    -  **Single Atom MR** enabled. Phaser ensembles can be single atoms.
       Suitable for high resolution data (better than 1.2A) only.
    -  Cumulative Intensity statistics (loggraphs) printed before and
       after anisotropy and tNCS correction to aid **twinning
       detection**
    -  Changed method for estimaing VRMS from sequence identity
       (22-3-12)
    -  Space group a function of the solution (2-2-12)
    -  **Translational NCS correction** terms applied by default. Phaser
       2.5.0 gives much higher signal-to-noise for molecular replacement
       solutions and SAD LLG maps where the crystal has
       pseudo-translational NCS (including pseudo-centring) (c.f.
       previous versions). (23-12-11)
    -  **CCP4 GUI interface** (ccp4i) files (for directories tasks,
       templates and scripts) now distributed with phaser source code
       (phaser svn). Copy (or link) files to ccp4i installation (in
       directories tasks, templates and scripts) to synchronise phaser
       source code/executable with ccp4i (23-12-11)

.. rubric:: 2011
   :name: section-5

 8th December 2011 - 23rd December 2011
    **Phaser-2.4.1 released with Phenix nightly builds**

    -  Features of Phaser-2.4.1

    -  Heterogen atoms carried through in PDB file in MR and not deleted
       (scattering not used, as before) (8-12-11)
    -  Bugfix for problem with packing function accepting cases where an
       ensemble with internal point group symmetry is on a special
       position

 7th December 2011
    **Phaser-2.4 released with Phenix-1.7.3**

    -  Features of Phaser-2.4

    -  Pseudo-translational NCS correction for MR and SAD phasing
       available, but not default (25-11-11)
    -  FAST search method turned on by default, replacing FULL search
       method default (17-11-11)
    -  MR possible with a model containing a flat molecule or single
       atom, for extending a MR solution to add flat ligands or metal
       ions (17-11-11)
    -  B-factor refinement turned on by default in molecular replacement
       (2-11-11)
    -  Bugfix for occasional and irreproducible segmentation faults for
       32 bit binary running on 64 bit Linux
    -  Packing function accepts cases where an ensemble with internal
       point group symmetry is on a special position, and pdb file
       output deletes atoms overlapped due to the symmetry of special
       position

 15th July 2011
    **Phaser-2.3 released with CCP4-6.2.0**

.. rubric:: 2010
   :name: section-6

 16th December 2010
    **Phaser-2.3 released with Phenix**

    -  Features of Phaser-2.3

    -  Option to refine variance of an ensemble ("RMS deviation" of
       target to model)
    -  Option for "FAST" search method that finds multiple copies of
       same ensemble more efficiently
    -  Significant speed up of fast rotation function
    -  Significant speed up of rigid body refinement (analytic gradients
       and hessian, rather than finite differences)
    -  Packing function - number of allowed clashes scales with the size
       of the molecules (percentage of atoms)
    -  Loggraphs of FOM vs resolution
    -  Option to give electron density as a model for SAD phasing

.. rubric:: 2009
   :name: section-7

 1st October 2009
    **Phaser-2.2 released**

    -  New features Phaser-2.2

       -  Reduced memory requirements
       -  For anisotropic data, map coefficients sharpened to falloff of
          strongest direction
       -  Improvements to SAD log-likelihood-gradient substructure
          completion
       -  Automated reassignment of atom types, when more than one type
          of scatterer
       -  Improved sensitivity of LLG completion protocol
       -  Facility to compare MR solutions with prior ("template")
          solution, e.g. from another program
       -  Refine relative B-factors of different components of MR
          solution
       -  Recognize internal symmetry in MR model, use to identify
          equivalent solutions
       -  Updated atom names consistent with PDBv3

.. rubric:: 2008
   :name: section-8

 14th January 2008
    **Phaser-2.1.2 released**

    -  Download from this website and with phenix-1.3b
    -  Patch to fix segmentation fault in non-linux executables

.. rubric:: 2007
   :name: section-9

 12th December 2007
    **CCP4 Automated Downloads**
    A Phaser module is now available through the CCP4 automated
    Downloads page and from the Daresbury ftp server.

 12th November 2007
    **Phaser-2.1.1 released**

    -  New Features Phaser-2.1.1

       -  Default packing criteria relaxed to allow a small number of
          clashes
       -  Default packing distance increased to identify intercalation
          of helices
       -  Composition can be estimated from solvent content
       -  Default composition corresponds to 50% solvent
       -  If automated MR search finds some, but not all, components,
          the partial solution is output
       -  Improved SAD phasing
       -  SAD phasing starting from MR partial model
       -  Output of PDB hybrid-36 atom numbers and col(21-22) chainids
          for largest structures

.. rubric:: 2006
   :name: section-10

 1st November 2006
    **Phaser-1.3.3 released**

    -  Download with ccp4 6.0.2.
    -  New Features Phaser-1.3,3

       -  Filename parsing accepts quoted strings, for Windows filenames
          that include spaces. Change synchronised with corresponding
          changes throughout the rest of the CCP4 suite.

1st July 2006
    **Phaser-2.0 released**

    -  Download with phenix-1.24-beta
    -  Minor Bug fixes from Phaser–1.3

1st February 2006
    **Phaser-1.3.2 released**

    -  Download with ccp4 6.0.0.
    -  Minor Bug fixes from Phaser–1.3.1

.. rubric:: 2005
   :name: section-11

 1st July 2005
    **Phaser-1.3.1 released**

    -  New Features Phaser-1.3.1

       -  Minor bug fixes after Phenix-1.1a release
       -  R-factor reported by MR\_RNP and MR\_LLG modes (verbose
          output)

15th June 2005
    **Phaser 1.3 released**

    -  Download with Phenix-1.1a
    -  New Features Phaser-1.3

       -  Release with Phenix-1.1a
       -  New Normal Mode Analysis of structures
       -  New Cell Content Analysis Mode
       -  Improved Automated MR
       -  Map coefficients appended to input mtz file
       -  Score stored in .sol file is Z-score rather than LLG
       -  Final phasing and refinement to full resolution on mtz file
       -  Packing of RNA/DNA
       -  Packing to form close packed oligomeric complexes
       -  Waters in pdb files excluded from packing
       -  Only the most similar model in an ensemble is used for packing
          analysis
       -  Reduced memory requirements
       -  Composition/MW determined from sequence
       -  Automated MR now available as a python script

1st June 2005
    **Phaser-1.2 expired**

    -  A typo in the expiry date in the source code caused the source to
       expire. A message explaining what happened and possible ways to
       work around it has been sent to all registered Phaser users. We
       apologise for the inconvenience, and we are working hard to get
       version 1.3 (which will not have an expiry date!) released as
       soon as possible.

.. rubric:: 2004
   :name: section-12

15th December 2004

-  **Congratulations to Richard Bunker, who registered as our 1000th
   user!**

 30th June 2004
    **Documentation updated**

    -  The README files for the installer, CCP4I GUI installation and
       the CCP4-4.2 extras have been clarified. Thanks for your
       feedback.

 25th June 2004
    **Phaser-1.2 released**

    -  Download from here.
    -  New Features Phaser-1.2

       -  Automated solution of structures
       -  New Likelihood Enhanced fast Translation Function (LETF)
       -  Ability to use electron density maps as molecular replacement
          models
       -  Rigid body solution refinement against maximum likelihood
          target
       -  Pruning of duplicate solutions from solution list
       -  Searching of multiple alternative space groups
       -  Searching of multiple alternative models
       -  Better minimizers

.. rubric:: 2003
   :name: section-13

 22nd December 2003
    **Phaser-cambridge-alpha-1-1a released**

    -  Phaser-cambridge-alpha-1-1 will stop working on the 1st of
       January 2004. We have therefore released a new version
       cambridge-alpha-1.1a that will work for another six months. This
       version is identical in all other areas to 1.1. We are currently
       working on a new and improved version (1.2) that will be released
       soon.

 4th November 2003
    **Documentation online**

26th September 2003
    **Phaser-cambridge-alpha-1-1 released**

    -  Bugfix in the anisotropy correction.

 6th September 2003
    **Phaser-cambridge-alpha-1-0 released**

3rd September 2003
    **Phaser logo launched**

 28th July 2003
    **Phaser helpdesk launched**

    -  Contact us on cimr-phaser@lists.cam.ac.uk

 14th April 2003
    **Phaser webpage launched**

.. raw:: html

   </div>

.. raw:: html

   <div class="printfooter">

.. raw:: html

   </div>

.. raw:: html

   <div id="catlinks">

.. raw:: html

   <div id="catlinks" class="catlinks catlinks-allhidden">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="column-one">

.. raw:: html

   <div id="p-cactions" class="portlet">

.. rubric:: Views
   :name: views

-  

   .. raw:: html

      <div id="ca-nstab-main">

   .. raw:: html

      </div>

   `Page <../../../../articles/r/e/l/Releases.html>`__
-  

   .. raw:: html

      <div id="ca-talk">

   .. raw:: html

      </div>

   `Discussion <http://localhost/index.php?title=Talk:Releases&action=edit&redlink=1>`__
-  

   .. raw:: html

      <div id="ca-current">

   .. raw:: html

      </div>

   `Latest revision <http://localhost/index.php/Releases>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-logo" class="portlet">

` <http://www.phaser.cimr.cam.ac.uk/index.php/Phaser_Crystallographic_Software>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-" class="portlet">

.. rubric:: 
   :name: section-14

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-PhaserWiki-Home">

   .. raw:: html

      </div>

   `PhaserWiki Home <../../../../index.html>`__
-  

   .. raw:: html

      <div id="n-Releases">

   .. raw:: html

      </div>

   `Releases <../../../../articles/r/e/l/Releases.html>`__
-  

   .. raw:: html

      <div id="n-Downloads">

   .. raw:: html

      </div>

   `Downloads <../../../../articles/d/o/w/Downloads.html>`__
-  

   .. raw:: html

      <div id="n-Manuals">

   .. raw:: html

      </div>

   `Manuals <../../../../articles/m/a/n/Manuals.html>`__
-  

   .. raw:: html

      <div id="n-Tutorials">

   .. raw:: html

      </div>

   `Tutorials <../../../../articles/t/u/t/Tutorials.html>`__
-  

   .. raw:: html

      <div id="n-FAQ">

   .. raw:: html

      </div>

   `FAQ <../../../../articles/f/a/q/FAQ.html>`__
-  

   .. raw:: html

      <div id="n-Top-Ten-Tips">

   .. raw:: html

      </div>

   `Top Ten Tips <../../../../articles/t/o/p/Top_Ten_Tips.html>`__
-  

   .. raw:: html

      <div id="n-Publications">

   .. raw:: html

      </div>

   `Publications <../../../../articles/p/u/b/Publications.html>`__
-  

   .. raw:: html

      <div id="n-External-Links">

   .. raw:: html

      </div>

   `External Links <../../../../articles/e/x/t/External_Links.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-users" class="portlet">

.. rubric:: users
   :name: users

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-MR-Phasing">

   .. raw:: html

      </div>

   `MR
   Phasing <../../../../articles/m/o/l/Molecular_Replacement.html>`__
-  

   .. raw:: html

      <div id="n-SAD-Phasing">

   .. raw:: html

      </div>

   `SAD
   Phasing <../../../../articles/e/x/p/Experimental_Phasing.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-developers" class="portlet">

.. rubric:: developers
   :name: developers

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Python-Interface">

   .. raw:: html

      </div>

   `Python
   Interface <../../../../articles/p/y/t/Python_Interface.html>`__
-  

   .. raw:: html

      <div id="n-Contact-Developers">

   .. raw:: html

      </div>

   `Contact Developers <../../../../articles/c/o/n/Contact.html>`__
-  

   .. raw:: html

      <div id="n-Developer-Pages">

   .. raw:: html

      </div>

   `Developer Pages <../../../../articles/d/e/v/Developers.html>`__
-  

   .. raw:: html

      <div id="n-Licences">

   .. raw:: html

      </div>

   `Licences <../../../../articles/l/i/c/Licences.html>`__
-  

   .. raw:: html

      <div id="n-SVN-Access">

   .. raw:: html

      </div>

   `SVN Access <../../../../articles/s/v/n/SVN_Repository.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-search" class="portlet">

.. rubric:: Search
   :name: search

.. raw:: html

   <div id="searchBody" class="pBody">

.. raw:: html

   <div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   <div id="footer">

.. raw:: html

   <div id="f-poweredbyico">

|Powered by MediaWiki|

.. raw:: html

   </div>

-  

   .. raw:: html

      <div id="f-credits">

   .. raw:: html

      </div>

   This page was last modified 12:23, 13 September 2016 by `Airlie
   McCoy <../../../../articles/a/i/r/User:Airlie.html>`__. Based on work
   by `WikiSysop <../../../../articles/w/i/k/User:WikiSysop.html>`__.
-  

   .. raw:: html

      <div id="f-about">

   .. raw:: html

      </div>

   `About
   Phaserwiki <../../../../articles/a/b/o/Phaserwiki:About.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. |Powered by MediaWiki| image:: ../../../../skins/common/images/poweredby_mediawiki_88x31.png
   :width: 88px
   :height: 31px
   :target: //www.mediawiki.org/
