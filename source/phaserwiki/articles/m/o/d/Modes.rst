.. raw:: html

   <div id="globalWrapper">

.. raw:: html

   <div id="column-content">

.. raw:: html

   <div id="content">

.. rubric:: Modes
   :name: modes
   :class: firstHeading

.. raw:: html

   <div id="bodyContent">

.. rubric:: From Phaserwiki
   :name: siteSub

.. raw:: html

   <div id="contentSub">

.. raw:: html

   </div>

.. raw:: html

   <div id="mw-content-text" class="mw-content-ltr" lang="en" dir="ltr">

.. raw:: html

   <div style="margin-left: 25px; float: right;">

.. raw:: html

   <div id="toc" class="toc">

.. raw:: html

   <div id="toctitle">

.. rubric:: Contents
   :name: contents

.. raw:: html

   </div>

-  `1 Phaser Executable <#Phaser_Executable>`__
-  `2 Python Interface <#Python_Interface>`__
-  `3 Modes <#Modes>`__

   -  `3.1 Automated Molecular
      Replacement <#Automated_Molecular_Replacement>`__
   -  `3.2 Rotation Function <#Rotation_Function>`__
   -  `3.3 Translation Function <#Translation_Function>`__
   -  `3.4 Packing <#Packing>`__
   -  `3.5 Refinement and Phasing <#Refinement_and_Phasing>`__
   -  `3.6 Anisotropy Correction <#Anisotropy_Correction>`__
   -  `3.7 Translational NCS and Twin
      analysis <#Translational_NCS_and_Twin_analysis>`__
   -  `3.8 Cell Content Analysis <#Cell_Content_Analysis>`__
   -  `3.9 Expected log-likelihood-gain (eLLG)
      Analysis <#Expected_log-likelihood-gain_.28eLLG.29_Analysis>`__
   -  `3.10 Normal Mode
      Analysis/SCEDS <#Normal_Mode_Analysis.2FSCEDS>`__
   -  `3.11 Automated Experimental
      Phasing <#Automated_Experimental_Phasing>`__
   -  `3.12 Single Atom Molecular
      Replacement <#Single_Atom_Molecular_Replacement>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

Phaser runs in different modes, which perform Phaser's different
functionalities. Modes can either be basic modes or modes that combine
the functionality of basic modes.

.. rubric:: Phaser Executable
   :name: phaser-executable

The Phaser executable runs in different modes, which perform Phaser's
different functionalities. The mode is selected with the
`MODE <../../../../articles/k/e/y/Keywords.html#MODE>`__ keyword.

.. rubric:: Python Interface
   :name: python-interface

Phaser can be compiled as a python library. The mode is selected by
calling the appropriate run-job. Input to the run-job is via
input-objects, which are passed to the run-job. Setter function on the
input objects are equivalent to the keywords for input to the phaser
executable. See `Python
Interface <../../../../articles/p/y/t/Python_Interface.html>`__ for
details.

.. rubric:: Modes
   :name: modes-1

.. rubric:: Automated Molecular Replacement
   :name: automated-molecular-replacement

MODE MR\_AUTO
Combines cell content analysis, anisotropy correction, translational NCS
analysis, rotation function, [optional rotation refinement], translation
function, packing, and refinement to automatically solve a structure by
molecular replacement
Searches for multiple components by looping through the steps listed
above
Two different search methods are available
**FAST** *Uses amalgamation to combine placements from a single
translation function*
**FULL' ** Heavily branched search without amalgamation**
::

     ResultMR r = runMR_AUTO(i=InputMR_AUTO)

.. rubric:: Rotation Function
   :name: rotation-function

MODE MR\_ROT
Combines the anisotropy correction, translational NCS analysis, and
EITHER likelihood-enhanced fast rotation function, by default rescored
with the full rotation likelihood function OR a brute-force search of
angles using the full likelihood rotation function
::

     ResultMR_RF r = runMR_FRF(i=InputMR_FRF)

.. rubric:: Translation Function
   :name: translation-function

MODE MR\_TRA
Combines the anisotropy correction, translational NCS analysis, and
EITHER likelihood-enhanced fast translation function, by default
rescored by the full likelihood translation function OR a brute force
search of positions using the full likelihood translation function to
find the position of a previously oriented model
::

     ResultMR_TF r = runMR_FTF(i=InputMR_FTF)

.. rubric:: Packing
   :name: packing

MODE MR\_PAK
Determines whether molecular replacement solutions pack in the unit cell
using a C-alpha clash test
::

     ResultMR r = runMR_PAK(i=InputMR_PAK)

.. rubric:: Refinement and Phasing
   :name: refinement-and-phasing

MODE MR\_RNP
Combines the anisotropy correction, translational NCS analysis, and
refinement against the likelihood function to optimize full or partial
molecular replacement solutions and phase the data. At the end of
refinement, the list of solutions is checked for duplicates, which are
pruned
::

     ResultMR r = runMR_RNP(i=InputMR_RNP)

MODE
`GYRE <http://www.jabberwocky.com/carroll/jabber/jabberwocky.html>`__
Combines the anisotropy correction, translational NCS analysis, and
refinement against the likelihood function to optimize the rotations and
relative translations (gyrations) between chains in an ensemble
::

     ResultGYRE r = runMR_GYRE(i=InputMR_RNP)

MODE
`GIMBLE <http://www.jabberwocky.com/carroll/jabber/jabberwocky.html>`__
Combines the anisotropy correction, translational NCS analysis, and
refinement against the likelihood function to optimize the rotations and
translations between chains in an ensemble
::

     ResultMR r = runMR_GMBL(i=InputMR_RNP)

MODE PRUNE
Combines the anisotropy correction, translational NCS analysis, and
occupancy refinement against the likelihood function to optimize the
occupancies of residues. Occupancy refinement is performed for
occupancies in sliding residue windows of a size determined by the
expected LLG. The resides that decrease the LLG are pruned.
::

     ResultMR r = runMR_OCC(i=InputMR_OCC)

.. rubric:: Anisotropy Correction
   :name: anisotropy-correction

MODE ANO
Corrects the experimental data (amplitude and associated sigma) for
anisotropy. First, the anisotropy is removed by scaling up data from
weak directions and scaling down data from strong directions, preserving
the overall equivalent isotropic B-factor. Then (as suggested by Strong
*et al.* (2006), PNAS 103:8060-8065), the data are resharpened to
restore the original falloff in the strong direction. The amount of
resharpening can be controlled with the RESHARP keyword.
::

     ResultANO r = runANO(i=InputANO)

.. rubric:: Translational NCS and Twin analysis
   :name: translational-ncs-and-twin-analysis

MODE NCS
Finds pseudo-translational NCS vectors and corrects the data for
intensity variations due to the ptNCS using likelihood methods. The data
are corrected for anisotropy first and analysed for twinning before and
after data correction.
::

     ResultNCS r = runNCS(i=InputNCS)

.. rubric:: Cell Content Analysis
   :name: cell-content-analysis

MODE CCA
Determines the composition of the crystals using the "new" Matthews
coefficients of Kantardjieff & Rupp (2003) ("Matthews coefficient
probabilities: Improved estimates for unit cell contents of proteins,
DNA and protein-nucleic acid complex crystals". Protein Science
12:1865-1871). The molecular weight of ONE complex or assembly to be
packed into the asymmetric unit is given and the possible Z values
(number of copies of the complex or assembly) that will fit in the
asymmetric unit and the relative frequency of their corresponding VM
values is reported.
::

     ResultCCA r = runCCA(i=InputCCA)

.. rubric:: Expected log-likelihood-gain (eLLG) Analysis
   :name: expected-log-likelihood-gain-ellg-analysis

MODE MR\_ELLG
Determines the eLLG for any models defined as ensembles, reporting the
eLLG at the full resolution of the data and the resolution predicted to
reach the target LLG.
::

     ResultELLG r = runMR_ELLG(i=InputMR_ELLG)

.. rubric:: Normal Mode Analysis/SCEDS
   :name: normal-mode-analysissceds

MODE NMA
Writes out coordinates of input structures that have been perturbed
along normal modes, in a procedure similar to that described by Suhre &
Sanejouand (Acta Cryst. D60, 796-799, 2004). Perturbed coordinates are
output in RMS increments for use in MR trials. Each run of the program
writes out a matrix FILEROOT.mat that contains the eigenvectors and
eigenvalues of the atomic Hessian, and can be read into subsequent runs
of the same job, to speed up the analysis.
::

     ResultNMA r = runNMAXYZ(i=InputNMA)

MODE SCEDS
Writes out domains generated by the SCEDS analysis of the coordinates
perturbed along normal modes.
::

     ResultNMA r = runSCEDS(i=InputNMA)

.. rubric:: Automated Experimental Phasing
   :name: automated-experimental-phasing

MODE EP\_AUTO
Combines anisotropy correction, translational NCS analysis, cell content
analysis, and SAD phasing to automatically solve a structure by
experimental phasing
::

     ResultEP r = runEP_AUTO(i=InputEP_AUTO)

.. rubric:: Single Atom Molecular Replacement
   :name: single-atom-molecular-replacement

MODE MR\_ATOM
Combines automated molecular replacement with log-likelihood gradient
completion to solve high resolution structures by single atom MR. The
data are prepared with runMR\_DAT and then the phasing is done withing
the EP\_AUTO mode.
::

     ResultEP r = runMR_ATOM(i=InputMR_ATOM)

.. raw:: html

   </div>

.. raw:: html

   <div class="printfooter">

.. raw:: html

   </div>

.. raw:: html

   <div id="catlinks">

.. raw:: html

   <div id="catlinks" class="catlinks catlinks-allhidden">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="column-one">

.. raw:: html

   <div id="p-cactions" class="portlet">

.. rubric:: Views
   :name: views

-  

   .. raw:: html

      <div id="ca-nstab-main">

   .. raw:: html

      </div>

   `Page <../../../../articles/m/o/d/Modes.html>`__
-  

   .. raw:: html

      <div id="ca-talk">

   .. raw:: html

      </div>

   `Discussion <http://localhost/index.php?title=Talk:Modes&action=edit&redlink=1>`__
-  

   .. raw:: html

      <div id="ca-current">

   .. raw:: html

      </div>

   `Latest revision <http://localhost/index.php/Modes>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-logo" class="portlet">

` <http://www.phaser.cimr.cam.ac.uk/index.php/Phaser_Crystallographic_Software>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-" class="portlet">

.. rubric:: 
   :name: section

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-PhaserWiki-Home">

   .. raw:: html

      </div>

   `PhaserWiki Home <../../../../index.html>`__
-  

   .. raw:: html

      <div id="n-Releases">

   .. raw:: html

      </div>

   `Releases <../../../../articles/r/e/l/Releases.html>`__
-  

   .. raw:: html

      <div id="n-Downloads">

   .. raw:: html

      </div>

   `Downloads <../../../../articles/d/o/w/Downloads.html>`__
-  

   .. raw:: html

      <div id="n-Manuals">

   .. raw:: html

      </div>

   `Manuals <../../../../articles/m/a/n/Manuals.html>`__
-  

   .. raw:: html

      <div id="n-Tutorials">

   .. raw:: html

      </div>

   `Tutorials <../../../../articles/t/u/t/Tutorials.html>`__
-  

   .. raw:: html

      <div id="n-FAQ">

   .. raw:: html

      </div>

   `FAQ <../../../../articles/f/a/q/FAQ.html>`__
-  

   .. raw:: html

      <div id="n-Top-Ten-Tips">

   .. raw:: html

      </div>

   `Top Ten Tips <../../../../articles/t/o/p/Top_Ten_Tips.html>`__
-  

   .. raw:: html

      <div id="n-Publications">

   .. raw:: html

      </div>

   `Publications <../../../../articles/p/u/b/Publications.html>`__
-  

   .. raw:: html

      <div id="n-External-Links">

   .. raw:: html

      </div>

   `External Links <../../../../articles/e/x/t/External_Links.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-users" class="portlet">

.. rubric:: users
   :name: users

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-MR-Phasing">

   .. raw:: html

      </div>

   `MR
   Phasing <../../../../articles/m/o/l/Molecular_Replacement.html>`__
-  

   .. raw:: html

      <div id="n-SAD-Phasing">

   .. raw:: html

      </div>

   `SAD
   Phasing <../../../../articles/e/x/p/Experimental_Phasing.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-developers" class="portlet">

.. rubric:: developers
   :name: developers

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Python-Interface">

   .. raw:: html

      </div>

   `Python
   Interface <../../../../articles/p/y/t/Python_Interface.html>`__
-  

   .. raw:: html

      <div id="n-Contact-Developers">

   .. raw:: html

      </div>

   `Contact Developers <../../../../articles/c/o/n/Contact.html>`__
-  

   .. raw:: html

      <div id="n-Developer-Pages">

   .. raw:: html

      </div>

   `Developer Pages <../../../../articles/d/e/v/Developers.html>`__
-  

   .. raw:: html

      <div id="n-Licences">

   .. raw:: html

      </div>

   `Licences <../../../../articles/l/i/c/Licences.html>`__
-  

   .. raw:: html

      <div id="n-SVN-Access">

   .. raw:: html

      </div>

   `SVN Access <../../../../articles/s/v/n/SVN_Repository.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-search" class="portlet">

.. rubric:: Search
   :name: search

.. raw:: html

   <div id="searchBody" class="pBody">

.. raw:: html

   <div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   <div id="footer">

.. raw:: html

   <div id="f-poweredbyico">

|Powered by MediaWiki|

.. raw:: html

   </div>

-  

   .. raw:: html

      <div id="f-credits">

   .. raw:: html

      </div>

   This page was last modified 13:23, 5 September 2016 by `Airlie
   McCoy <../../../../articles/a/i/r/User:Airlie.html>`__. Based on work
   by `Randy Read <../../../../articles/r/a/n/User:Randy.html>`__ and
   `WikiSysop <../../../../articles/w/i/k/User:WikiSysop.html>`__.
-  

   .. raw:: html

      <div id="f-about">

   .. raw:: html

      </div>

   `About
   Phaserwiki <../../../../articles/a/b/o/Phaserwiki:About.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. |Powered by MediaWiki| image:: ../../../../skins/common/images/poweredby_mediawiki_88x31.png
   :width: 88px
   :height: 31px
   :target: //www.mediawiki.org/
