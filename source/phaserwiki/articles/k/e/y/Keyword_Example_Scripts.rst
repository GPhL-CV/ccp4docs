.. raw:: html

   <div id="globalWrapper">

.. raw:: html

   <div id="column-content">

.. raw:: html

   <div id="content">

.. rubric:: Keyword Example Scripts
   :name: keyword-example-scripts
   :class: firstHeading

.. raw:: html

   <div id="bodyContent">

.. rubric:: From Phaserwiki
   :name: siteSub

.. raw:: html

   <div id="contentSub">

.. raw:: html

   </div>

.. raw:: html

   <div id="mw-content-text" class="mw-content-ltr" lang="en" dir="ltr">

.. raw:: html

   <div style="margin-left: 25px; float: right;">

.. raw:: html

   <div id="toc" class="toc">

.. raw:: html

   <div id="toctitle">

.. rubric:: Contents
   :name: contents

.. raw:: html

   </div>

-  `1 How to Define Models <#How_to_Define_Models>`__

   -  `1.1 Building an Ensemble from
      Coordinates <#Building_an_Ensemble_from_Coordinates>`__
   -  `1.2 Building an Ensemble from Electron
      Density <#Building_an_Ensemble_from_Electron_Density>`__

-  `2 How to Define Composition <#How_to_Define_Composition>`__

   -  `2.1 Composition by Molecular
      Weight <#Composition_by_Molecular_Weight>`__
   -  `2.2 Composition by Sequence <#Composition_by_Sequence>`__
   -  `2.3 Composition by Percentage
      Scattering <#Composition_by_Percentage_Scattering>`__

-  `3 How to Define Solutions <#How_to_Define_Solutions>`__

   -  `3.1 "sol" Files <#.22sol.22_Files>`__
   -  `3.2 "rlist" Files <#.22rlist.22_Files>`__
   -  `3.3 Fixed Partial Structure <#Fixed_Partial_Structure>`__

-  `4 Automated Molecular
   Replacement <#Automated_Molecular_Replacement>`__
-  `5 Non-Automated Molecular
   Replacement <#Non-Automated_Molecular_Replacement>`__

   -  `5.1 Fast Rotation Function <#Fast_Rotation_Function>`__
   -  `5.2 Brute Rotation Function <#Brute_Rotation_Function>`__
   -  `5.3 Fast Translation Function <#Fast_Translation_Function>`__
   -  `5.4 Brute Translation Function <#Brute_Translation_Function>`__
   -  `5.5 Refinement and Phasing <#Refinement_and_Phasing>`__
   -  `5.6 Log-Likelihood Gain <#Log-Likelihood_Gain>`__
   -  `5.7 Packing <#Packing>`__

-  `6 Automated Experimental
   Phasing <#Automated_Experimental_Phasing>`__
-  `7 Anisotropy Correction <#Anisotropy_Correction>`__
-  `8 Cell Content Analysis <#Cell_Content_Analysis>`__
-  `9 Translational NCS Analysis <#Translational_NCS_Analysis>`__
-  `10 Normal Mode Analysis <#Normal_Mode_Analysis>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

Phaser can be run from the command line

$prompt> phaser
After printing the Phaser Banner, the Preprocessor will ask:

ENTER KEYWORD INPUT FROM FILE OR FROM STANDARD INPUT
Enter the keyword input

At the end of the input, start Phaser with one of the commands

-  END
-  QUIT
-  STOP
-  KILL
-  EXIT
-  GO
-  RUN
-  START

Alternatively, phaser can be run from command scripts as below

| 

.. rubric:: How to Define Models
   :name: how-to-define-models

.. rubric:: Building an Ensemble from Coordinates
   :name: building-an-ensemble-from-coordinates

You have one structure as a model with 44% sequence identity to the
protein in the crystal.

::

      ENSEmble mol1 PDB structure1.pdb IDENtity .44

You have three structures as models with 44%, 39% and 35% identity to
the protein in the crystal.

::

      ENSEmble mol2   PDB structure1.pdb IDENtity .44 PDB structure2.pdb IDENtity .39 PDB structure3.pdb IDENtity .35

You have an NMR Ensemble as a model. There is no need to split the
coordinates in the pdb file provided that the models are separated by
MODEL and ENDMDL cards. In this case the sequence identity is not a good
indication of the rms deviation of the structural coordinates to the
target structure. You should use the RMS option; several test cases have
succeeded where the ID was close to 100% with an RMS value of about
1.5Å. Note that, for NMR ensembles in which not all of the structure is
well-defined, it is important to trim off parts of the models that
diverge significantly. This can be done easily in Ensembler, with the
trim=True option.

::

      ENSEmble mol3 PDB nmr.pdb RMS 1.5

.. rubric:: Building an Ensemble from Electron Density
   :name: building-an-ensemble-from-electron-density

You have low resolution electron density of your model. This density has
been cut out and converted to structure factors in a large cell.

::

      ENSEmble mol1  HKLIn mol1.mtz F = Fmol1 P = Pmol1 EXTEnt 23 25 29 RMS 2.0 CENTre 4 3 30 PROTein MW 10241 NUCLeic MW 0

.. rubric:: How to Define Composition
   :name: how-to-define-composition

.. rubric:: Composition by Molecular Weight
   :name: composition-by-molecular-weight

You have one protein (with MW 21022) in the asymmetric unit

::

    COMPosition PROTein MW 21022

You have three copies of a protein (with MW 21022) in the asymmetric
unit

::

      COMPosition PROTein MW 21022
      COMPosition PROTein MW 21022
      COMPosition PROTein MW 21022

Another way of entering the same thing is

::

      COMPosition PROTein MW 21022 NUMber 3

Yet another way of entering the same thing is

::

      COMPosition PROTein MW 63066

You have two copies of a protein (with MW 21022), two copies of a
protein (with MW 9843) and RNA with (MW 32004) in the asymmetric unit

::

      COMPosition PROTein MW 21022 NUMber 2
      COMPosition PROTein MW 9843 NUMber 2
      COMPosition NUCLeic MW 32004

.. rubric:: Composition by Sequence
   :name: composition-by-sequence

You have one protein (with sequence in fasta format in the file
prot1.seq) in the asymmetric unit

::

      COMPosition PROTein SEQuence prot1.seq

You have three copies of a protein (with sequence in fasta format in the
file prot1.seq) in the asymmetric unit

::

      COMPosition PROTein SEQuence prot1.seq
      COMPosition PROTein SEQuence prot1.seq
      COMPosition PROTein SEQuence prot1.seq

Another way of entering the same thing is

::

      COMPosition PROTein SEQuence prot1.seq NUMber 3

Yet another way of entering the same thing is to make a sequence file
with all the amino acids concatenated together (prot1.seq3)

::

      COMPosition PROTein SEQuence prot1.seq3

You have two copies of a protein (with sequence in fasta format in the
file prot1.seq), two copies of a protein (with sequence in fasta format
in the file prot2.seq) and RNA with (with sequence in fasta format in
the file nucl1.seq) in the asymmetric unit

::

      COMPosition PROTein SEQuence prot1.seq NUMber 2
      COMPosition PROTein SEQuence prot2.seq NUMber 2
      COMPosition NUCLeic SEQuence nucl1.seq

.. rubric:: Composition by Percentage Scattering
   :name: composition-by-percentage-scattering

Each copy of Ensemble mol1 gives 22% of the scattering

::

      COMPosition ENSEmble mol1 FRACtional 0.22

Each copy of Ensemble mol2 gives 78% of the scattering

::

      COMPosition ENSEmble mol2 FRACtional 0.78

.. rubric:: How to Define Solutions
   :name: how-to-define-solutions

To include the files you should use the preprocessor command @

::

      @ filename.sol
      @ filename.rlist

.. rubric:: "sol" Files
   :name: sol-files

One copy of mol1 with known orientation and position (fractional
coordinates)

::

      SOLUtion 6DIM ENSEmble mol1 EULEr 17 20 32 FRACtional 0.12 0.05 0.74

If the rotation function and translation function for mol1 were very
clear, then there will only be one type of 6DIM solution for mol1. If
the rotation and translation functions for mol2 were then not clear,
there will be a series of possible 6DIM solutions for mol2.

::

      SOLUtion SET 
      SOLUtion 6DIM ENSEmble mol1 EULEr 17 20 32 FRACtional 0.12 0.05 0.74
      SOLUtion 6DIM ENSEmble mol2 EULEr 5 183 230 FRACtional 0.71 0.54 0.81 
      SOLUtion SET
      SOLUtion 6DIM ENSEmble mol1 EULEr 17 20 32 FRACtional 0.12 0.05 0.74
      SOLUtion 6DIM ENSEmble mol2 EULEr 51 93 75 FRACtional 0.08 0.57 0.25 

.. rubric:: "rlist" Files
   :name: rlist-files

THere are three trial orientations to search

::

      SOLUtion TRIAl ENSEmble mol1 EULEr 17 20 32 SCORE 4.5
      SOLUtion TRIAl ENSEmble mol1 EULEr 67 65 51 SCORE 4.4
      SOLUtion TRIAl ENSEmble mol1 EULEr 67 112 81 SCORE 4.3

There are two possibilities for the position of the first molecule, and
two orientations to search for the first and three for the second.

::

      SOLUtion SET
      SOLUtion 6DIM ENSEmble mol1 EULEr 17 20 32 FRACtional 0.12 0.05 0.74
      SOLUtion TRIAl ENSEmble mol1 EULEr 44 20 32 SCORE 5.8
      SOLUtion TRIAl ENSEmble mol1 EULEr 67 65 51 SCORE 5.2
      SOLUtion SET
      SOLUtion 6DIM ENSEmble mol1 EULEr 17 20 32 FRACtional 0.13 0.55 0.76
      SOLUtion TRIAl ENSEmble mol1 EULEr 83 9 180 SCORE 6.3
      SOLUtion TRIAl ENSEmble mol1 EULEr 8 36 92 SCORE 4.2
      SOLUtion TRIAl ENSEmble mol1 EULEr 48 87 10 SCORE 4.0

.. rubric:: Fixed Partial Structure
   :name: fixed-partial-structure

If you have the coordinates of a partial solution with the pdb
coordinates of the known structure in the correct orientation and
position, then you can force Phaser to use these coordinates. Use this
pdb file to define an ensemble (named "mol1" in this example). Then
manually create a .sol file of the following form and include it in the
Phaser command script with the @filename preprocessor command (or
include it directly in the script)

::

      SOLUtion SET 
      SOLUtion 6DIM ENSEmble mol1 EULEr 0 0 0 FRACtional 0 0 0

.. rubric:: Automated Molecular Replacement
   :name: automated-molecular-replacement

Example command script for finding BETA and BLIP. This is the minimum
input, using all defaults (except the ROOT filename).

::

      #''beta_blip_auto.com''
      phaser << eof
      TITLe beta blip automatic
      MODE MR_AUTO
      HKLIn beta_blip.mtz
      LABIn F=Fobs SIGF=Sigma
      ENSEmble beta PDB beta.pdb IDENtity 100
      ENSEmble blip PDB blip.pdb IDENtity 100
      COMPosition PROTein SEQuence beta.seq NUM 1 #beta
      COMPosition PROTein SEQuence blip.seq NUM 1 #blip
      SEARch ENSEmble beta NUM 1
      SEARch ENSEmble blip NUM 1
      ROOT beta_blip_auto # not the default
      eof

.. rubric:: Non-Automated Molecular Replacement
   :name: non-automated-molecular-replacement

In special circumstances, you may need to run the steps of a structure
solution separately, to gain more control over the progress of the run
or to use specialized features. This can be illustrated by breaking up
the solution of the beta-lactamase:BLIP complex.

Here is a job to automatically find the beta-lactamase component, which
we would expect to be easier to find than BLIP (AUTO\_beta.com in the
tutorial directory).

::

     phaser << eof
     MODE MR_AUTO
     HKLIN beta_blip_P3221.mtz
     LABIN F = Fobs SIGF = Sigma
     ENSEMBLE beta PDBFILE beta.pdb IDENTITY 1.0
     COMPOSITION PROTEIN SEQUENCE beta.seq NUM 1
     COMPOSITION PROTEIN SEQUENCE blip.seq NUM 1
     SEARCH ENSEMBLE beta NUM 1 
     ROOT AUTO_beta
     eof

Compared to the fully automated job searching for both components, the
only important difference is the removal of the second SEARCH command.
We could have defined the ENSEMBLE for blip, but we aren't using it in
this job so it isn't necessary. Note that both COMPOSITION commands are
still needed so that Phaser knows the fraction of the structure
specified by beta!

Now we can use the information from the beta-lactamase solution in
carrying out a rotation search for the BLIP component.

::

     phaser << eof
     MODE MR_FRF
     HKLIN beta_blip_P3221.mtz
     LABIN F = Fobs SIGF = Sigma
     ENSEMBLE beta PDBFILE beta.pdb IDENTITY 1.0
     ENSEMBLE blip PDBFILE blip.pdb IDENTITY 1.0
     COMPOSITION PROTEIN SEQUENCE beta.seq NUM 1
     COMPOSITION PROTEIN SEQUENCE blip.seq NUM 1
     SOLUTION 6DIM ENSEMBLE beta EULER 199.95 41.50 184.08 FRAC -0.4974 -0.1588 -0.2808
     SEARCH ENSEMBLE blip
     ROOT ROT_blip_fixbeta
     eof

Note that the MODE is now MR\_FRF (Fast Rotation Function). The SOLUTION
6DIM command gives information about the solution for beta that is
contained in the output file AUTO\_beta.sol from running AUTO\_beta.com.
Take a look at AUTO\_beta.sol, if you ran that job. Notice that it
specifies the space group (important if we had tested both
possibilities, P3121 and P3221). The SOLU SET command can be used to
separate different potential solutions, each of which can be used as the
start of searches for further molecules, but in this case there is only
one.

Instead of copying the information from AUTO\_beta.sol, it is easier to
just include it using the @ command. @ is a Phaser preprocessor command
that allows you to read in external files and use the contents as if
they were explicitly included in the script file. The script is
ROT\_blip\_fixbeta.com in the tutorial directory.

::

     phaser << eof
     MODE MR_FRF
     HKLIN beta_blip_P3221.mtz
     LABIN F = Fobs SIGF = Sigma
     ENSEMBLE beta PDBFILE beta.pdb IDENTITY 1.0
     ENSEMBLE blip PDBFILE blip.pdb IDENTITY 1.0
     COMPOSITION PROTEIN SEQUENCE beta.seq NUM 1
     COMPOSITION PROTEIN SEQUENCE blip.seq NUM 1
     @AUTO_beta.sol
     SEARCH ENSEMBLE blip
     ROOT ROT_blip_fixbeta
     eof

Look at the file ROT\_blip\_fixbeta.rlist produced by running this job
("source ROT\_blip\_fixbeta.com" in the tutorial directory). This file
contains the rotation peaks (SOLU TRIAL commands) as well as the fixed
beta-lactamase solution (SOLU 6DIM command). We can include this file in
a job to run a translation search, still fixing the known beta-lactamase
solution.

::

     phaser << eof
     MODE MR_FTF
     HKLIN beta_blip_P3221.mtz
     LABIN F = Fobs SIGF = Sigma
     ENSEMBLE beta PDBFILE beta.pdb IDENTITY 1.0
     ENSEMBLE blip PDBFILE blip.pdb IDENTITY 1.0
     COMPOSITION PROTEIN SEQUENCE beta.seq NUM 1
     COMPOSITION PROTEIN SEQUENCE blip.seq NUM 1
     @ROT_blip_fixbeta.rlist
     ROOT TRA_blip_fixbeta
     eof

What has changed?

-  The MODE is now MR\_FTF (Molecular Replacement - Fast Translation
   Function) instead of MR\_FRF
-  The orientations from the rotation search have been included using
   the @ command
-  The SEARCH keyword has disappeared

Ok, that's all there is to it, so run this script
(TRA\_blip\_fixbeta.com) and see what output you get.

.. rubric:: Fast Rotation Function
   :name: fast-rotation-function

Example command script for fast rotation function to find the
orientation of BETA.

::

      
      #beta_frf.com
      phaser << eof
      TITLe beta FRF
      MODE MR_FRF
      HKLIn beta_blip.mtz
      LABIn F=Fobs SIGF=Sigma
      ENSEmble beta PDB beta.pdb IDENtity 100
      COMPosition PROTein SEQuence beta.seq NUM 1 #beta
      COMPosition PROTein SEQuence blip.seq NUM 1 #blip
      SEARCH ENSEmble beta
      ROOT beta_frf
      eof

Example command script for fast rotation function to find the
orientation of BLIP knowing the position and orientation of BETA, with
the position and orientation of BETA input from the command line.

::

      #blip_frf_with_beta.com
      phaser << eof
      TITLe blip FRF with beta rotation and translation
      MODE MR_FRF
      HKLIn beta_blip.mtz
      LABIn F=Fobs SIGF=Sigma
      ENSEmble beta PDB beta.pdb IDENtity 100
      ENSEmble blip PDB blip.pdb IDENtity 100
      COMPosition PROTein SEQuence beta.seq #beta
      COMPosition PROTein SEQuence blip.seq #blip
      SEARch ENSEmble blip
      SOLUtion 6DIM ENSEmble beta EULEr 201 41 184 FRACtional -0.49408 -0.15571 -0.28148
      ROOT blip_frf_with_beta
      eof

Example command script for fast rotation function to find the
orientation of BLIP knowing only the orientation of BETA, with the
orientation of BETA input using the output solution file from the
beta\_frf.com job above.

::

      #blip_frf_with_beta_rot.com
      phaser << eof
      TITLe blip FRF with beta R
      MODE MR_FRF
      HKLIn beta_blip.mtz
      LABIn F=Fobs SIGF=Sigma
      ENSEmble beta PDB beta.pdb IDENtity 100
      ENSEmble blip PDB blip.pdb IDENtity 100
      COMPosition PROTein SEQuence beta.seq NUM 1 #beta
      COMPosition PROTein SEQuence blip.seq NUM 1 #blip
      SEARch ENSEmble blip
      @beta_frf.sol # solution file output by phaser
      ROOT blip_frf_with_beta_rot
      eof

.. rubric:: Brute Rotation Function
   :name: brute-rotation-function

Example command script for brute rotation function to find the
orientation of BETA

::

      #beta_brf.com
      phaser << eof
      TITLe beta BRF
      MODE MR_FRF
      TARGET ROTATION BRUTE
      HKLIn beta_blip.mtz
      LABIn F=Fobs SIGF=Sigma
      ENSEmble beta PDB beta.pdb IDENtity 100
      COMPosition PROTein SEQuence beta.seq NUM 1 #beta
      COMPosition PROTein SEQuence blip.seq NUM 1 #blip
      SEARch ENSEmble beta
      ROOT beta_brf
      eof

Example command script for brute rotation function to find the optimal
orientation of BETA in a restricted search range and on a fine grid
around the position from the fast rotation search.

::

      #beta_brf_around.com
      phaser << eof
      TITLe beta BRF fine sampling
      MODE MR_FRF
      TARGET ROTATION BRUTE
      HKLIn beta_blip.mtz
      LABIn F=Fobs SIGF=Sigma
      ENSEmble beta PDB beta.pdb IDENtity 100
      ENSEmble blip PDB blip.pdb IDENtity 100
      COMPosition PROTein SEQuence beta.seq NUM 1 #beta
      COMPosition PROTein SEQuence blip.seq NUM 1 #blip
      SEARch ENSEmble beta
      ROTAte AROUnd EULEr 201 41 184 RANGE 10
      SAMPling ROTation 0.5
      XYZOut ON # not the default
      ROOT beta_brf_around
      eof

.. rubric:: Fast Translation Function
   :name: fast-translation-function

Example command script for finding the position of BETA after the
rotation function has been run and the results output to the file
beta\_frf.rlist

::

      #beta_ftf.com
      phaser << eof
      TITLe beta FTF
      MODE MR_FTF
      HKLIn beta_blip.mtz
      LABIn F=Fobs SIGF=Sigma
      ENSEmble beta PDB beta.pdb IDENtity 100
      ENSEmble blip PDB blip.pdb IDENtity 100
      COMPosition PROTein SEQuence beta.seq NUM 1 #beta
      COMPosition PROTein SEQuence blip.seq NUM 1 #blip
      @beta_frf.rlist
      ROOT beta_ftf
      eof 

Example command script for finding the position of BLIP after the
rotation function has been run and the results output to the file
blip\_frf\_with\_beta.rlist, which has the SOLUtion 6DIM keyword input
for BETA and the SOLUtion TRIAL keyword input for the orientations to
try for BLIP with the translation function.

::

      #blip_ftf_with_beta.com
      phaser << eof
      TITLe beta FTF
      MODE MR_FTF
      HKLIn beta_blip.mtz
      LABIn F=Fobs SIGF=Sigma
      ENSEmble beta PDB beta.pdb IDENtity 100
      ENSEmble blip PDB blip.pdb IDENtity 100
      COMPosition PROTein SEQuence beta.seq NUM 1 #beta
      COMPosition PROTein SEQuence blip.seq NUM 1 #blip
      @blip_frf_with_beta.rlist
      ROOT blip_ftf_with_beta
      eof

.. rubric:: Brute Translation Function
   :name: brute-translation-function

Example command script for brute Translation function to find the
position of BETA after the rotation function has been run

::

      
      #beta_btf.com
      phaser << eof
      TITLe beta BTF
      MODE MR_FTF
      TARGET TRANSLATION BRUTE
      HKLIn beta_blip.mtz
      LABIn F=Fobs SIGF=Sigma
      ENSEmble beta PDB beta.pdb IDENtity 100
      ENSEmble blip PDB blip.pdb IDENtity 100
      COMPosition PROTein SEQuence beta.seq NUM 1 #beta
      COMPosition PROTein SEQuence blip.seq NUM 1 #blip
      @beta_frf.rlist
      TRANslate AROUnd FRACtional POINt -0.49408 -0.15571 -0.28148 RANGe 5
      ROOT beta_btf
      eof 

.. rubric:: Refinement and Phasing
   :name: refinement-and-phasing

Example command script to refine a set of solutions

::

      #beta_blip_rnp.com
      phaser << eof
      TITLe beta blip rigid body refinement
      MODE MR_RNP
      HKLIn beta_blip.mtz
      LABIn F=Fobs SIGF=Sigma
      ENSEmble beta PDB beta.pdb IDENtity 100
      ENSEmble blip PDB blip.pdb IDENtity 100
      COMPosition PROTein SEQuence beta.seq NUM 1 #beta
      COMPosition PROTein SEQuence blip.seq NUM 1 #blip
      ROOT beta_blip_rnp # not the default
      HKLOut OFF # not the default
      XYZOut OFF # not the default
      @beta_blip_auto.sol
      eof 

.. rubric:: Log-Likelihood Gain
   :name: log-likelihood-gain

Example command script to rescore the solutions using a different
resolution range of data and a different spacegroup

::

      #beta_blip_llg.com
      phaser << eof
      TITLe beta blip solution 6A P3121
      MODE MR_LLG
      HKLIn beta_blip.mtz
      LABIn F=F SIGF = SIGF
      ENSEmble beta PDB beta.pdb IDENtity 100
      ENSEmble blip PDB blip.pdb IDENtity 100
      COMPosition PROTein SEQuence beta.seq NUM 1 #beta
      COMPosition PROTein SEQuence blip.seq NUM 1 #blip
      ROOT beta_blip_llg # not the default
      RESOlution 6.0
      SPACegroup P 31 2 1
      @beta_blip_auto.sol
      eof 

.. rubric:: Packing
   :name: packing

Example command script for determining whether a set of molecular
replacement solutions pack in the unit cell.

::

      #beta_blip_pak.com
      phaser << eof
      TITLe beta blip packing check
      MODE MR_PAK
      HKLIn beta_blip.mtz
      LABIn F=F SIGF=SIGF
      ENSEmble beta PDB beta.pdb IDENtity 100
      ENSEmble blip PDB blip.pdb IDENtity 100
      COMPosition PROTein SEQuence beta.seq NUM 1 #beta
      COMPosition PROTein SEQuence blip.seq NUM 1 #blip
      ROOT beta_blip_pak # not the default
      @beta_blip_auto.sol
      eof 

.. rubric:: Automated Experimental Phasing
   :name: automated-experimental-phasing

Do SAD phasing of insulin. This is the minimum input, using all defaults
(except the ROOT filename and specifying the wavelength explicitly).

::

      #insulin_auto.com
      phaser << eof
      MODE EP_AUTO
      TITLe sad phasing of insulin with intrinsic sulphurs
      HKLIn S-insulin.mtz
      COMPosition PROTein SEQ S-insulin.seq
      CRYStal insulin DATAset sad LABIn F+=F(+) SIG+=SIGF(+) F-=F(-) SIG-=SIGF(-)
      CRYStal insulin DATAset sad SCATtering CUKA # default: change if necessary
      LLGComplete CRYStal insulin COMPLETE ON SCATtering ELEMent S
      ATOM CRYStal insulin PDB S-insulin_hyss.pdb
      ROOT insulin_auto
      eof

.. rubric:: Anisotropy Correction
   :name: anisotropy-correction

Example command script to correct BETA-BLIP data for anisotropy

::

     
      #beta_blip_ano.com
      phaser << eof
      MODE ANO
      TITLe beta blip data correction
      HKLIn beta_blip.mtz
      LABIn F=Fobs SIGF=Sigma
      ROOT beta_blip_ano # not the default
      eof 

.. rubric:: Cell Content Analysis
   :name: cell-content-analysis

Example script for cell content analysis for BETA-BLIP

::

      #beta_blip_cca.com
      phaser << eof
      TITLe BETA-BLIP cell content analysis
      MODE CCA
      HKLIn beta_blip.mtz
      LABIn F=Fobs SIGF=Sigma
      COMPosition PROTein SEQuence beta.seq NUM 1 #beta
      COMPosition PROTein SEQuence blip.seq NUM 1 #blip
      ROOT beta_blip_cca # not the default
      eof 

.. rubric:: Translational NCS Analysis
   :name: translational-ncs-analysis

Example script for translational NCS analysis for BETA-BLIP

::

      #beta_blip_ncs.com
      phaser << eof
      TITLe BETA-BLIP translational NCS analysis
      MODE NCS  
      HKLIn beta_blip.mtz
      LABIn F=Fobs SIGF=Sigma
      COMPosition PROTein SEQuence beta.seq NUM 1 #beta
      COMPosition PROTein SEQuence blip.seq NUM 1 #blip
      ROOT beta_blip_ncs # not the default
      eof 

.. rubric:: Normal Mode Analysis
   :name: normal-mode-analysis

Do normal mode analysis, write out eigenfile and coordinates perturbed
by default movements along mode 7 only. (If you only wanted to prepare
the eigenfile but not coordinates, you could include the command "XYZOut
OFF").

::

      
      #beta_nma.com
      phaser << eof
      TITLe beta normal mode analysis
      MODE NMA
      ENSEmble beta PDB beta.pdb IDENtity 100
      ROOT beta_nma # not the default
      eof 

This example shows the use of several infrequently used options. Read in
previous eigenfile and write out pdb files perturbed in 0.5 Ångstrom rms
intervals in "forward" (positive dq values) direction only along modes 7
and 10 (and combinations of 7 and 10), up to a maximum rms shift of
1.2Å. Normally you would want to perturb the structure in both
directions, and modes 8 and 9 are more likely to be of interest than
mode 10, but something like this might be useful as part of a larger,
more exhaustive, exploration.

::

      #beta_nma_pdb.com
      phaser << eof
      TITLe beta normal mode analysis pdb file generation
      MODE NMA
      ENSEmble beta PDB beta.pdb IDENtity 100
      ROOT beta_nma_pdb # not the default
      EIGEn READ beta_nma.mat
      NMAPdb MODE 7 MODE 10 
      NMAPdb RMS STEP 0.5 
      NMAPdb RMS MAXRMS 1.2 
      NMAPDB RMS DIRECTION FORWARD
      eof

.. raw:: html

   </div>

.. raw:: html

   <div class="printfooter">

.. raw:: html

   </div>

.. raw:: html

   <div id="catlinks">

.. raw:: html

   <div id="catlinks" class="catlinks catlinks-allhidden">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="column-one">

.. raw:: html

   <div id="p-cactions" class="portlet">

.. rubric:: Views
   :name: views

-  

   .. raw:: html

      <div id="ca-nstab-main">

   .. raw:: html

      </div>

   `Page <../../../../articles/k/e/y/Keyword_Example_Scripts.html>`__
-  

   .. raw:: html

      <div id="ca-talk">

   .. raw:: html

      </div>

   `Discussion <http://localhost/index.php?title=Talk:Keyword_Example_Scripts&action=edit&redlink=1>`__
-  

   .. raw:: html

      <div id="ca-current">

   .. raw:: html

      </div>

   `Latest
   revision <http://localhost/index.php/Keyword_Example_Scripts>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-logo" class="portlet">

` <http://www.phaser.cimr.cam.ac.uk/index.php/Phaser_Crystallographic_Software>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-" class="portlet">

.. rubric:: 
   :name: section

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-PhaserWiki-Home">

   .. raw:: html

      </div>

   `PhaserWiki Home <../../../../index.html>`__
-  

   .. raw:: html

      <div id="n-Releases">

   .. raw:: html

      </div>

   `Releases <../../../../articles/r/e/l/Releases.html>`__
-  

   .. raw:: html

      <div id="n-Downloads">

   .. raw:: html

      </div>

   `Downloads <../../../../articles/d/o/w/Downloads.html>`__
-  

   .. raw:: html

      <div id="n-Manuals">

   .. raw:: html

      </div>

   `Manuals <../../../../articles/m/a/n/Manuals.html>`__
-  

   .. raw:: html

      <div id="n-Tutorials">

   .. raw:: html

      </div>

   `Tutorials <../../../../articles/t/u/t/Tutorials.html>`__
-  

   .. raw:: html

      <div id="n-FAQ">

   .. raw:: html

      </div>

   `FAQ <../../../../articles/f/a/q/FAQ.html>`__
-  

   .. raw:: html

      <div id="n-Top-Ten-Tips">

   .. raw:: html

      </div>

   `Top Ten Tips <../../../../articles/t/o/p/Top_Ten_Tips.html>`__
-  

   .. raw:: html

      <div id="n-Publications">

   .. raw:: html

      </div>

   `Publications <../../../../articles/p/u/b/Publications.html>`__
-  

   .. raw:: html

      <div id="n-External-Links">

   .. raw:: html

      </div>

   `External Links <../../../../articles/e/x/t/External_Links.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-users" class="portlet">

.. rubric:: users
   :name: users

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-MR-Phasing">

   .. raw:: html

      </div>

   `MR
   Phasing <../../../../articles/m/o/l/Molecular_Replacement.html>`__
-  

   .. raw:: html

      <div id="n-SAD-Phasing">

   .. raw:: html

      </div>

   `SAD
   Phasing <../../../../articles/e/x/p/Experimental_Phasing.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-developers" class="portlet">

.. rubric:: developers
   :name: developers

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Python-Interface">

   .. raw:: html

      </div>

   `Python
   Interface <../../../../articles/p/y/t/Python_Interface.html>`__
-  

   .. raw:: html

      <div id="n-Contact-Developers">

   .. raw:: html

      </div>

   `Contact Developers <../../../../articles/c/o/n/Contact.html>`__
-  

   .. raw:: html

      <div id="n-Developer-Pages">

   .. raw:: html

      </div>

   `Developer Pages <../../../../articles/d/e/v/Developers.html>`__
-  

   .. raw:: html

      <div id="n-Licences">

   .. raw:: html

      </div>

   `Licences <../../../../articles/l/i/c/Licences.html>`__
-  

   .. raw:: html

      <div id="n-SVN-Access">

   .. raw:: html

      </div>

   `SVN Access <../../../../articles/s/v/n/SVN_Repository.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-search" class="portlet">

.. rubric:: Search
   :name: search

.. raw:: html

   <div id="searchBody" class="pBody">

.. raw:: html

   <div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   <div id="footer">

.. raw:: html

   <div id="f-poweredbyico">

|Powered by MediaWiki|

.. raw:: html

   </div>

-  

   .. raw:: html

      <div id="f-credits">

   .. raw:: html

      </div>

   This page was last modified 10:23, 25 August 2016 by `Randy
   Read <../../../../articles/r/a/n/User:Randy.html>`__. Based on work
   by `Airlie McCoy <../../../../articles/a/i/r/User:Airlie.html>`__ and
   `WikiSysop <../../../../articles/w/i/k/User:WikiSysop.html>`__.
-  

   .. raw:: html

      <div id="f-about">

   .. raw:: html

      </div>

   `About
   Phaserwiki <../../../../articles/a/b/o/Phaserwiki:About.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. |Powered by MediaWiki| image:: ../../../../skins/common/images/poweredby_mediawiki_88x31.png
   :width: 88px
   :height: 31px
   :target: //www.mediawiki.org/
