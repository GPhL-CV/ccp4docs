.. raw:: html

   <div id="globalWrapper">

.. raw:: html

   <div id="column-content">

.. raw:: html

   <div id="content">

.. rubric:: Airlie J. McCoy
   :name: airlie-j.-mccoy
   :class: firstHeading

.. raw:: html

   <div id="bodyContent">

.. rubric:: From Phaserwiki
   :name: siteSub

.. raw:: html

   <div id="contentSub">

.. raw:: html

   </div>

.. raw:: html

   <div id="mw-content-text" class="mw-content-ltr" lang="en" dir="ltr">

.. raw:: html

   <div style="margin-left: 25px; float: right;">

.. raw:: html

   <div id="toc" class="toc">

.. raw:: html

   <div id="toctitle">

.. rubric:: Contents
   :name: contents

.. raw:: html

   </div>

-  `1 Publications <#Publications>`__

   -  `1.1 2016 <#2016>`__
   -  `1.2 2015 <#2015>`__
   -  `1.3 2014 <#2014>`__
   -  `1.4 2013 <#2013>`__
   -  `1.5 2012 <#2012>`__
   -  `1.6 2011 <#2011>`__
   -  `1.7 2010 <#2010>`__
   -  `1.8 2009 <#2009>`__
   -  `1.9 2008 <#2008>`__
   -  `1.10 2007 <#2007>`__
   -  `1.11 2006 <#2006>`__
   -  `1.12 2005 <#2005>`__
   -  `1.13 2004 <#2004>`__
   -  `1.14 2003 <#2003>`__
   -  `1.15 2002 <#2002>`__
   -  `1.16 2001 <#2001>`__
   -  `1.17 2000 <#2000>`__
   -  `1.18 1999 <#1999>`__
   -  `1.19 1998 <#1998>`__
   -  `1.20 1997 <#1997>`__
   -  `1.21 1996 <#1996>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. rubric:: Publications
   :name: publications

.. rubric:: 2016
   :name: section

Transient Fcho1/2⋅Eps15/R⋅AP-2 Nanoclusters Prime the AP-2 Clathrin
Adaptor for Cargo Binding.
    Ma L, Umasankar PK, Wrobel AG, Lymar A, McCoy AJ, Holkar SS, Jha A,
    Pradhan-Sundd T, Watkins SC, Owen DJ, Traub LM.
    Dev Cell. 2016 May 24. pii: S1534-5807(16)30280-5.
    `**link** <http://dx.doi.org/10.1016/j.devcel.2016.05.003>`__

.. rubric:: 2015
   :name: section-1

A log-likelihood-gain intensity target for crystallographic phasing that
accounts for experimental error.
    Read RJ, McCoy AJ.
    Acta Crystallogr D Struct Biol. 2016 Mar 1;72(Pt 3):375-87.
    `**link** <http://dx.doi.org/10.1107/S2059798315013236>`__. Epub
    2016 Mar 1.

Advances in experimental phasing.
    McCoy AJ, Schneider T.
    Acta Crystallogr D Struct Biol. 2016 Mar 1;72(Pt 3):291-2.
    `**link** <http://dx.doi.org/10.1107/S2059798316003375>`__. Epub
    2016

X-ray structure determination using low-resolution electron microscopy
maps for molecular replacement.
    Jackson RN, McCoy AJ, Terwilliger TC, Read RJ, Wiedenheft B.
    Nat Protoc. 2015 Sep;10(9):1275-84.
    `**link** <http://dx.doi.org/10.1038/nprot.2015.069>`__. Epub 2015
    Jul 30.

ANS complex of St John's wort PR-10 protein with 28 copies in the
asymmetric unit
    a fiendish combination of pseudosymmetry with tetartohedral
    twinning.
    Sliwiak J, Dauter Z, Kowiel M, McCoy AJ, Read RJ, Jaskolski M.
    Acta Crystallogr D Biol Crystallogr. 2015 Apr;71(Pt 4):829-43.
    `**link** <http://dx.doi.org/10.1107/S1399004715001388>`__ . Epub
    2015 Mar 26.

.. rubric:: 2014
   :name: section-2

Macromolecular X-ray structure determination using weak,
single-wavelength anomalous data.
    Bunkóczi G, McCoy AJ, Echols N, Grosse-Kunstleve RW, Adams PD,
    Holton JM, Read RJ, Terwilliger TC.
    Nat Methods. 2015 Feb;12(2):127-30.
    `**link** <http://dx.doi.org/10.1038/nmeth.3212>`__. Epub 2014 Dec
    22.

VARP is recruited on to endosomes by direct interaction with retromer,
where together they function in export to the cell surface.
    Hesketh GG, Pérez-Dorado I, Jackson LP, Wartosch L, Schäfer IB, Gray
    SR, McCoy AJ, Zeldin OB, Garman EF, Harbour ME, Evans PR, Seaman MN,
    Luzio JP, Owen DJ.
    Dev Cell. 2014 Jun 9;29(5):591-606. Epub 2014 May 22.
    `**Dev Cell
    link** <http://dx.doi.org/10.1016/j.devcel.2014.04.010>`__

Automated identification of elemental ions in macromolecular crystal
structures.
    Echols N, Morshed N, Afonine PV, McCoy AJ, Miller MD, Read RJ,
    Richardson JS, Terwilliger TC, Adams PD.
    Acta Crystallogr D Biol Crystallogr. 2014 Apr;70(Pt 4):1104-14. Epub
    2014 Mar 20.
    `**IUCr link** <http://dx.doi.org/10.1107/S1399004714001308>`__

Likelihood-based molecular-replacement solution for a highly
pathological crystal with tetartohedral twinning and sevenfold
translational noncrystallographic symmetry.
    Sliwiak 1, Jaskolski M, Dauter Z, McCoy AJ, Read RJ.
    Acta Crystallogr D Biol Crystallogr. 2014 Feb;70(Pt 2):471-80. Epub
    2014 Jan 29.
    `**IUCr link** <http://dx.doi.org/10.1107/S1399004713030319>`__

Automating crystallographic structure solution and refinement of
protein-ligand complexes.
    Echols N, Moriarty NW, Klei HE, Afonine PV, Bunkóczi G, Headd JJ,
    McCoy AJ, Oeffner RD, Read RJ, Terwilliger TC, Adams PD.
    Acta Crystallogr D Biol Crystallogr. 2014 Jan;70(Pt 1):144-54 Epub
    2013 Dec 25.
    `**IUCr link** <http://dx.doi.org/10.1107/S139900471302748X>`__

Structures of yeast mitochondrial ADP/ATP carriers support a
domain-based alternating-access transport mechanism.
    Ruprecht JJ, Hellawell AM, Harding M, Crichton PG, McCoy AJ, Kunji
    ER.
    Proc Natl Acad Sci U S A. 2014 Jan 28;111(4):E426-34 Epub 2014 Jan
    13.
    `**PNAS link** <http://dx.doi.org/10.1073/pnas.1320692111>`__

.. rubric:: 2013
   :name: section-3

Cytokine Spatzle binds to the Drosophila immunoreceptor Toll with a
neurotrophin-like specificity and couples receptor activation.
    Lewis M, Arnot CJ, Beeston H, McCoy A., Ashcroft AE, Gay NJ,
    Gangloff M.
    Proc Natl Acad Sci U S A. 2013 Dec 17;110(51):20461-6. Epub 2013 Nov
    26.
    `**download pdf** <http://dx.doi.org/10.1073/pnas.1317002110>`__

Phaser.MRage: automated molecular replacement.
    Bunkóczi G, Echols N, McCoy AJ, Oeffner RD, Adams PD, Read RJ.
    Acta Crystallogr D Biol Crystallogr. 2013 Nov;69(Pt 11):2276-86.
    Epub 2013 Oct 18.
    `**IUCr link** <http://dx.doi.org/10.1107/S0907444913022750>`__

SCEDS: protein fragments for molecular replacement in Phaser.
    McCoy AJ, Nicholls RA, Schneider TR.
    Acta Crystallogr D Biol Crystallogr. 2013 Nov;69(Pt 11):2216-25.
    `**doi** <http://dx.doi.org/10.1107/S0907444913021811>`__. Epub 2013
    Oct 4.
    `**IUCr link** <http://scripts.iucr.org/cgi-bin/paper?ba5209>`__

Improved estimates of coordinate error for molecular replacement.
    Oeffner RD, Bunkóczi G, McCoy AJ, Read RJ.
    Acta Crystallogr D Biol Crystallogr. 2013 Nov;69(Pt 11):2209-15.
    `**doi** <http://dx.doi.org/10.1107/S0907444913023512>`__. Epub 2013
    Oct 12.
    `**IUCr link** <http://scripts.iucr.org/cgi-bin/paper?ba5212>`__

Intensity statistics in the presence of translational
noncrystallographic symmetry.
    Read RJ, Adams PD, McCoy AJ.
    Acta Crystallogr D Biol Crystallogr 2013 Feb;69(Pt 2):176-83. Epub
    Jan 16.
    `**IUCr link** <http://scripts.iucr.org/cgi-bin/paper?dz5268>`__

.. rubric:: 2012
   :name: section-4

Graphical tools for macromolecular crystallography in PHENIX.
    Echols N, Grosse-Kunstleve RW, Afonine PV, Bunkóczi G, Chen VB,
    Headd JJ, McCoy AJ, Moriarty NW, Read RJ, Richardson DC, Richardson
    JS, Terwilliger TC, Adams PD.
    J Appl Crystallogr. 2012 Jun 1;45(Pt 3):581-586. Epub 2012 May 16.
    `**download
    pdf** <http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3359726/pdf/j-45-00581.pdf>`__

.. rubric:: 2011
   :name: section-5

The Phenix software for automated determination of macromolecular
structures.
    Adams PD, Afonine PV, Bunkóczi G, Chen VB, Echols N, Headd JJ, Hung
    LW, Jain S, Kapral GJ, Grosse Kunstleve RW, McCoy AJ, Moriarty NW,
    Oeffner RD, Read RJ, Richardson DC, Richardson JS, Terwilliger TC,
    Zwart PH.
    Methods. 2011 Sep;55(1):94-106. doi: 10.1016/j.ymeth.2011.07.005.
    Epub 2011 Jul 29.
    `**download
    pdf** <http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3193589/pdf/nihms319608.pdf>`__

Overview of the CCP4 suite and current developments
    Winn MD, Ballard CC, Cowtan KD, Dodson EJ, Emsley P, Evans PR,
    Keegan RM, Krissinel EB, Leslie AG, McCoy A, McNicholas SJ,
    Murshudov GN, Pannu NS, Potterton EA, Powell HR, Read RJ, Vagin A,
    Wilson KS.
    Acta Crystallogr D Biol Crystallogr. 2011 Apr;67(Pt 4):235-242
    `**Acta Cryst D** <http://scripts.iucr.org/cgi-bin/paper?dz5219>`__

Using SAD data in Phaser
    Read RJ, McCoy AJ
    Acta Crystallogr D Biol Crystallogr. 2011 Apr;67(Pt 4):338-344
    `**Acta Cryst D** <http://scripts.iucr.org/cgi-bin/paper?ba5159>`__

.. rubric:: 2010
   :name: section-6

A large-scale conformational change couples membrane recruitment to
cargo binding in the AP2 clathrin adaptor complex.
    Jackson LP, Kelly BT, McCoy AJ, Gaffry T, James LC, Collins BM,
    Höning S, Evans PR, Owen DJ.
    Cell. 2010 Jun 25;141(7):1220-9.
    `**Cell** <http://www.cell.com/abstract/S0092-8674(10)00542-8>`__
    (subscription required)

Experimental phasing: best practice and pitfalls.
    McCoy AJ, Read RJ.
    Acta Crystallogr D Biol Crystallogr. 2010 Apr;66(Pt 4):458-69. Epub
    2010 Mar 24.
    `**Acta Cryst D** <http://scripts.iucr.org/cgi-bin/paper?ba5142>`__

PHENIX: a comprehensive Python-based system for macromolecular structure
solution.
    Adams PD, Afonine PV, Bunkóczi G, Chen VB, Davis IW, Echols N, Headd
    JJ, Hung LW, Kapral GJ, Grosse-Kunstleve RW, McCoy AJ, Moriarty NW,
    Oeffner R, Read RJ, Richardson DC, Richardson JS, Terwilliger TC,
    Zwart PH.
    Acta Crystallogr D Biol Crystallogr. 2010 Feb;66(Pt 2):213-21. Epub
    2010 Jan 22.
    `**Acta Cryst D** <http://scripts.iucr.org/cgi-bin/paper?dz5186>`__

.. rubric:: 2009
   :name: section-7

Decision-making in structure solution using Bayesian estimates of map
quality: the PHENIX AutoSol wizard
    Terwilliger TC, Adams PD, Read RJ, McCoy AJ , Moriarty NW,
    Grosse-Kunstleve RW, Afonine PV, Zwart PH, Hung LW.
    Acta Crystallogr D Biol Crystallogr. 2009 Jun;65(Pt 6):582-601
    `**Acta Cryst** <http://scripts.iucr.org/cgi-bin/paper?ea5095>`__

.. rubric:: 2008
   :name: section-8

A structural explanation for the binding of endocytic dileucine motifs
by the AP2 complex
    Kelly BT, McCoy AJ, Späte K, Miller SE, Evans PR, Höning S, Owen DJ.
    Nature. 2008 Dec 18;456(7224):976-79
    `**Nature** <http://www.nature.com/nature/journal/v456/n7224/abs/nature07422.html>`__
    (subscription required)

An introduction to molecular replacement
    Evans P, McCoy A.
    Acta Crystallogr D Biol Crystallogr. 2008 Jan;64(Pt 1):1-10. Epub
    2007 Dec 5.
    `**Acta Cryst D** <http://scripts.iucr.org/cgi-bin/paper?ba5108>`__

Automated structure solution with the PHENIX suite
    Zwart PH, Afonine PV, Grosse-Kunstleve RW, Hung LW, Ioerger TR,
    McCoy AJ, McKee E, Moriarty NW, Read RJ, Sacchettini JC, Sauter NK,
    Storoni LC, Terwilliger TC, Adams PD.
    Methods Mol Biol. 2008;426:419-35.
    `**doi** <http://dx.doi.org/10.1007/978-1-60327-058-8>`__ Digital
    Object Identifier

.. rubric:: 2007
   :name: section-9

A SNARE-adaptor interaction is a new mode of cargo recognition in
clathrin-coated vesicles
    Miller SE, Collins BM, McCoy AJ, Robinson MS, Owen DJ.
    Nature. 2007 Nov 22;450(7169):570-4
    `**Nature** <http://www.nature.com/nature/journal/v450/n7169/abs/nature06353.html>`__
    (subscription required)

High-resolution structure prediction and the crystallographic phase
problem
    Qian B, Raman S, Das R, Bradley P, McCoy AJ, Read RJ, Baker D.
    Nature. 2007 Nov 8;450(7167):259-64. Epub 2007 Oct 14.
    `**Nature** <http://www.nature.com/nature/journal/v450/n7167/abs/nature06249.html>`__
    (subscription required)

Solving structures of protein complexes by molecular replacement with
Phaser
    McCoy AJ
    Acta Crystallogr D Biol Crystallogr. 2007 Jan;63(Pt 1):32-41. Epub
    2006 Dec 13.
    `**Acta Cryst D** <http://scripts.iucr.org/cgi-bin/paper?ba5095>`__

Phaser crystallographic software
    McCoy AJ, Grosse-Kunstleve RW, Adams PD, Winn MD, Storoni LC and
    Read RJ.
    J. Appl. Cryst. (2007). 40, 658-674.
    `**J Appl Cryst** <http://scripts.iucr.org/cgi-bin/paper?he5368>`__

β-Edge interactions in a pentadecameric human antibody Vκ domain.
    James LC, Jones PC, McCoy A, Tennent GA, Pepys MB, Famm K, Winter G.
    J Mol Biol. 2007 Mar 30;367(3):603-8. Epub 2006 Nov 3.
    `**doi** <http://dx.doi.org/10.1016/j.jmb.2006.10.093>`__ Digital
    Object Identifier

.. rubric:: 2006
   :name: section-10

Crystal structure of rab11 in complex with rab11 family interacting
protein 2
    Jagoe WN, Lindsay AJ, Read RJ, McCoy AJ, McCaffrey MW, Khan AR.
    Structure. 2006 Aug;14(8):1273-83.
    `**Structure** <http://www.structure.org/content/article/abstract?uid=PIIS0969212606002930>`__
    (subscription required)

.. rubric:: 2005
   :name: section-11

Likelihood-enhanced fast translation functions
    McCoy AJ, Grosse-Kunstleve RW, Storoni LC, Read RJ.
    Acta Crystallogr D Biol Crystallogr. 2005 Apr;61(Pt 4):458-64. Epub
    2005 Mar 24.
    `**Acta Cryst
    D** <http://scripts.iucr.org/cgi-bin/paper?gx5042.pdf>`__

.. rubric:: 2004
   :name: section-12

Liking Likelihood
    McCoy AJ
    Acta Crystallogr D Biol Crystallogr. 2004 Dec;60(Pt 12 Pt
    1):2169-83. Epub 2004 Nov 26.
    `**Acta Cryst D** <http://scripts.iucr.org/cgi-bin/paper?ba5064>`__

Simple algorithm for a maximum-likelihood SAD function
    McCoy AJ, Storoni LC, Read RJ.
    Acta Crystallogr D Biol Crystallogr. 2004 Jul;60(Pt 7):1220-8. Epub
    2004 Jun 22.
    `**Acta Cryst D** <http://scripts.iucr.org/cgi-bin/paper?ea5015>`__

Likelihood-enhanced fast rotation functions
    Storoni LC, McCoy AJ, Read RJ.
    Acta Crystallogr D Biol Crystallogr. 2004 Mar;60(Pt 3):432-8. Epub
    2004 Feb 25.
    `**Acta Cryst D** <http://scripts.iucr.org/cgi-bin/paper?ad5007>`__
    `**pdf** <http://www-structmed.cimr.cam.ac.uk/Personal/randy/pubs/ad5007.pdf>`__

.. rubric:: 2003
   :name: section-13

Recent developments in the PHENIX software for automated
crystallographic structure determination
    Adams PD, Gopal K, Grosse-Kunstleve RW, Hung LW, Ioerger TR, McCoy
    AJ, Moriarty NW, Pai RK, Read RJ, Romo TD, Sacchettini JC, Sauter
    NK, Storoni LC, Terwilliger TC.
    J Synchrotron Radiat. 2004 Jan 1;11(Pt 1):53-5. Epub 2003 Nov 28.
    `**J Synch
    Radia** <http://scripts.iucr.org/cgi-bin/paper?ys0029.pdf>`__

Application of the complex multivariate normal distribution to
crystallographic methods with insights into multiple isomorphous
replacement phasing
    Pannu NS, McCoy AJ, Read RJ.
    Acta Crystallogr D Biol Crystallogr. 2003 Oct;59(Pt 10):1801-8. Epub
    2003 Sep 19.
    `**Acta Cryst
    D** <http://scripts.iucr.org/cgi-bin/paper?wd5000.pdf>`__

Structure of β-antithrombin and the effect of glycosylation on
antithrombin's heparin affinity and activity
    McCoy AJ, Pei XY, Skinner R, Abrahams JP, Carrell RW.
    J Mol Biol. 2003 Feb 21;326(3):823-33.
    `**doi** <http://dx.doi.org/10.1016/S0022-2836(02)01382-7>`__
    Digital Object Identifier

.. rubric:: 2002
   :name: section-14

New applications of maximum likelihood and Bayesian statistics in
macromolecular crystallography
    McCoy AJ
    Curr Opin Struct Biol. 2002 Oct;12(5):670-3. Review.
    `**doi** <http://dx.doi.org/10.1016/S0959-440X(02)00373-1>`__
    Digital Object Identifier

PHENIX: building new software for automated crystallographic structure
determination
    Adams PD, Grosse-Kunstleve RW, Hung LW, Ioerger TR, McCoy AJ,
    Moriarty NW, Read RJ, Sacchettini JC, Sauter NK, Terwilliger TC.
    Acta Crystallogr D Biol Crystallogr. 2002 Nov;58(Pt 11):1948-54.
    Epub 2002 Oct 21.
    `**Acta Cryst D** <http://scripts.iucr.org/cgi-bin/paper?ba5027>`__

Molecular architecture and functional model of the endocytic AP2 complex
    Collins BM, McCoy AJ, Kent HM, Evans PR, Owen DJ.
    Cell. 2002 May 17;109(4):523-35.
    `**Cell** <http://www.structure.org/content/article/abstract?uid=PIIS0092867402007353>`__
    (subscription required)

.. rubric:: 2001
   :name: section-15

No publications
.. rubric:: 2000
   :name: section-16

ScFv multimers of the anti-neuraminidase antibody NC10: shortening of
the linker in single-chain Fv fragment assembled in V(L) to V(H)
orientation drives the formation of dimers, trimers, tetramers and
higher molecular mass multimers
    Dolezal O, Pearce LA, Lawrence LJ, McCoy AJ, Hudson PJ, Kortt AA.
    Protein Eng. 2000 Aug;13(8):565-74.
    `**doi** <http://dx.doi.org/10.1093/protein/13.8.565>`__ Digital
    Object Identifier

The conformational activation of antithrombin. A 2.85-A structure of a
fluorescein derivative reveals an electrostatic link between the hinge
and heparin binding regions.
    Huntington JA\*, McCoy A\*, Belzar KJ, Pei XY, Gettins PG, Carrell
    RW.
    \*authors contributed equally
    J Biol Chem. 2000 May 19;275(20):15377-83
    `**doi** <http://dx.doi.org/10.1074/jbc.275.20.15377>`__ Digital
    Object Identifier

.. rubric:: 1999
   :name: section-17

Structural basis for dimerization of the Dictyostelium gelation factor
(ABP120) rod
    McCoy AJ, Fucini P, Noegel AA, Stewart M.
    Nat Struct Biol. 1999 Sep;6(9):836-41.
    `**Nature Structural
    Biology** <http://www.nature.com/nsmb/journal/v6/n9/abs/nsb0999_836.html>`__
    (subscription required)

scFv multimers of the anti-neuraminidase antibody NC10: length of the
linker between VH and VL domains dictates precisely the transition
between diabodies and triabodies
    Atwell JL, Breheney KA, Lawrence LJ, McCoy AJ, Kortt AA, Hudson PJ.
    Protein Eng. 1999 Jul;12(7):597-604.
    `**doi** <http://dx.doi.org/10.1093/protein/12.7.597>`__ Digital
    Object Identifier

Engineered mutants in the switch II loop of Ran define the contribution
made by key residues to the interaction with nuclear transport factor 2
(NTF2) and the role of this interaction in nuclear protein import
    Kent HM, Moore MS, Quimby BB, Baker AM, McCoy AJ, Murphy GA, Corbett
    AH, Stewart M.
    J Mol Biol. 1999 Jun 11;289(3):565-77.
    `**doi** <http://dx.doi.org/10.1006/jmbi.1999.2775>`__ Digital
    Object Identifier

.. rubric:: 1998
   :name: section-18

The structure of the Q69L mutant of GDP-Ran shows a major conformational
change in the switch II loop that accounts for its failure to bind
nuclear transport factor 2 (NTF2)
    Stewart M, Kent HM, McCoy AJ.
    J Mol Biol. 1998 Dec 18;284(5):1517-27.
    `**doi** <http://dx.doi.org/10.1006/jmbi.1998.2204>`__ Digital
    Object Identifier

Three-dimensional structures of single-chain Fv-neuraminidase complexes
    Malby RL\*, McCoy AJ\*, Kortt AA, Hudson PJ, Colman PM.
    \*authors contributed equally
    J Mol Biol. 1998 Jun 19;279(4):901-10.
    `**doi** <http://dx.doi.org/10.1006/jmbi.1998.1794>`__ Digital
    Object Identifier

Structural basis for molecular recognition between nuclear transport
factor 2 (NTF2) and the GDP-bound form of the Ras-family GTPase Ran
    Stewart M, Kent HM, McCoy AJ.
    J Mol Biol. 1998 Apr 3;277(3):635-46.
    `**doi** <http://dx.doi.org/10.1006/jmbi.1997.1602>`__ Digital
    Object Identifier

Structural basis for amoeboid motility in nematode sperm
    Bullock TL, McCoy AJ, Kent HM, Roberts TM, Stewart M.
    Nat Struct Biol. 1998 Mar;5(3):184-9.
    `**Nature Structural
    Biology** <http://www.nature.com/nsmb/journal/v5/n3/abs/nsb0398-184.html>`__
    (subscription required)

.. rubric:: 1997
   :name: section-19

Crystallization and preliminary X-Ray diffraction characterization of a
dimerizing fragment of the rod domain of the Dictyostelium gelation
factor (ABP-120)
    Fucini P, McCoy AJ, Gomez-Ortiz M, Schleicher M, Noegel AA, Stewart
    M.
    J Struct Biol. 1997 Nov;120(2):192-5
    `**doi** <http://dx.doi.org/10.1006/jsbi.1997.3930>`__ Digital
    Object Identifier

Nuclear protein import is decreased by engineered mutants of nuclear
transport factor 2 (NTF2) that do not bind GDP-Ran
    Clarkson WD, Corbett AH, Paschal BM, Kent HM, McCoy AJ, Gerace L,
    Silver PA, Stewart M.
    J Mol Biol. 1997 Oct 10;272(5):716-30.
    `**doi** <http://dx.doi.org/10.1006/jmbi.1997.1255>`__ Digital
    Object Identifier

The 1.8 Å crystal structure of winged bean albumin 1, the major albumin
from *Psophocarpus tetragonolobus (L.) DC*
    McCoy AJ, Kortt AA.
    J Mol Biol. 1997 Jun 27;269(5):881-91.
    `**doi** <http://dx.doi.org/10.1006/jmbi.1997.1067>`__ Digital
    Object Identifier

Electrostatic complementarity at protein/protein interfaces
    McCoy AJ, Chandana Epa V, Colman PM.
    J Mol Biol. 1997 May 2;268(2):570-84.
    `**doi** <http://dx.doi.org/10.1006/jmbi.1997.0987>`__ Digital
    Object Identifier

Single-chain Fv fragments of anti-neuraminidase antibody NC10 containing
five- and ten-residue linkers form dimers and with zero-residue linker a
trimer
    Kortt AA, Lah M, Oddie GW, Gruen CL, Burns JE, Pearce LA, Atwell JL,
    McCoy AJ, Howlett GJ, Metzger DW, Webster RG, Hudson PJ.
    Protein Eng. 1997 Apr;10(4):423-33.
    `**doi** <http://dx.doi.org/10.1093/protein/10.4.423>`__ Digital
    Object Identifier

.. rubric:: 1996
   :name: section-20

Protein Structure and Interaction
    McCoy AJ
    Thesis (Ph. D)
    `**University of Melbourne
    Library** <http://cat.lib.unimelb.edu.au/search~S30?/amccoy+aj/amccoy+aj/-3%2C0%2C0%2CB/frameset&FF=amccoy+airlie+janet&1%2C1%2C/indexsort=->`__
    (not available for loan)

.. raw:: html

   </div>

.. raw:: html

   <div class="printfooter">

.. raw:: html

   </div>

.. raw:: html

   <div id="catlinks">

.. raw:: html

   <div id="catlinks" class="catlinks catlinks-allhidden">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="column-one">

.. raw:: html

   <div id="p-cactions" class="portlet">

.. rubric:: Views
   :name: views

-  

   .. raw:: html

      <div id="ca-nstab-main">

   .. raw:: html

      </div>

   `Page <../../../../articles/a/i/r/Airlie_J._McCoy.html>`__
-  

   .. raw:: html

      <div id="ca-talk">

   .. raw:: html

      </div>

   `Discussion <http://localhost/index.php?title=Talk:Airlie_J._McCoy&action=edit&redlink=1>`__
-  

   .. raw:: html

      <div id="ca-current">

   .. raw:: html

      </div>

   `Latest revision <http://localhost/index.php/Airlie_J._McCoy>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-logo" class="portlet">

` <http://www.phaser.cimr.cam.ac.uk/index.php/Phaser_Crystallographic_Software>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-" class="portlet">

.. rubric:: 
   :name: section-21

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-PhaserWiki-Home">

   .. raw:: html

      </div>

   `PhaserWiki Home <../../../../index.html>`__
-  

   .. raw:: html

      <div id="n-Releases">

   .. raw:: html

      </div>

   `Releases <../../../../articles/r/e/l/Releases.html>`__
-  

   .. raw:: html

      <div id="n-Downloads">

   .. raw:: html

      </div>

   `Downloads <../../../../articles/d/o/w/Downloads.html>`__
-  

   .. raw:: html

      <div id="n-Manuals">

   .. raw:: html

      </div>

   `Manuals <../../../../articles/m/a/n/Manuals.html>`__
-  

   .. raw:: html

      <div id="n-Tutorials">

   .. raw:: html

      </div>

   `Tutorials <../../../../articles/t/u/t/Tutorials.html>`__
-  

   .. raw:: html

      <div id="n-FAQ">

   .. raw:: html

      </div>

   `FAQ <../../../../articles/f/a/q/FAQ.html>`__
-  

   .. raw:: html

      <div id="n-Top-Ten-Tips">

   .. raw:: html

      </div>

   `Top Ten Tips <../../../../articles/t/o/p/Top_Ten_Tips.html>`__
-  

   .. raw:: html

      <div id="n-Publications">

   .. raw:: html

      </div>

   `Publications <../../../../articles/p/u/b/Publications.html>`__
-  

   .. raw:: html

      <div id="n-External-Links">

   .. raw:: html

      </div>

   `External Links <../../../../articles/e/x/t/External_Links.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-users" class="portlet">

.. rubric:: users
   :name: users

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-MR-Phasing">

   .. raw:: html

      </div>

   `MR
   Phasing <../../../../articles/m/o/l/Molecular_Replacement.html>`__
-  

   .. raw:: html

      <div id="n-SAD-Phasing">

   .. raw:: html

      </div>

   `SAD
   Phasing <../../../../articles/e/x/p/Experimental_Phasing.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-developers" class="portlet">

.. rubric:: developers
   :name: developers

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Python-Interface">

   .. raw:: html

      </div>

   `Python
   Interface <../../../../articles/p/y/t/Python_Interface.html>`__
-  

   .. raw:: html

      <div id="n-Contact-Developers">

   .. raw:: html

      </div>

   `Contact Developers <../../../../articles/c/o/n/Contact.html>`__
-  

   .. raw:: html

      <div id="n-Developer-Pages">

   .. raw:: html

      </div>

   `Developer Pages <../../../../articles/d/e/v/Developers.html>`__
-  

   .. raw:: html

      <div id="n-Licences">

   .. raw:: html

      </div>

   `Licences <../../../../articles/l/i/c/Licences.html>`__
-  

   .. raw:: html

      <div id="n-SVN-Access">

   .. raw:: html

      </div>

   `SVN Access <../../../../articles/s/v/n/SVN_Repository.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-search" class="portlet">

.. rubric:: Search
   :name: search

.. raw:: html

   <div id="searchBody" class="pBody">

.. raw:: html

   <div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   <div id="footer">

.. raw:: html

   <div id="f-poweredbyico">

|Powered by MediaWiki|

.. raw:: html

   </div>

-  

   .. raw:: html

      <div id="f-credits">

   .. raw:: html

      </div>

   This page was last modified 01:24, 3 June 2016 by `Airlie
   McCoy <../../../../articles/a/i/r/User:Airlie.html>`__. Based on work
   by `WikiSysop <../../../../articles/w/i/k/User:WikiSysop.html>`__.
-  

   .. raw:: html

      <div id="f-about">

   .. raw:: html

      </div>

   `About
   Phaserwiki <../../../../articles/a/b/o/Phaserwiki:About.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. |Powered by MediaWiki| image:: ../../../../skins/common/images/poweredby_mediawiki_88x31.png
   :width: 88px
   :height: 31px
   :target: //www.mediawiki.org/
