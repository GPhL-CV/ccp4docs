.. raw:: html

   <div id="globalWrapper">

.. raw:: html

   <div id="column-content">

.. raw:: html

   <div id="content">

.. rubric:: Top Ten Tips
   :name: top-ten-tips
   :class: firstHeading

.. raw:: html

   <div id="bodyContent">

.. rubric:: From Phaserwiki
   :name: siteSub

.. raw:: html

   <div id="contentSub">

.. raw:: html

   </div>

Here are our Top Ten Tips for MR, SAD and MR-SAD phasing in Phaser

#. **Use the automated search**
   Don't have a quick read of the instructions and feel the need to
   change the defaults. If Phaser can solve your problem, it can
   probably solve it with the automated search using default inputs.
#. **Search for components concurrently**
   You should search for all components of the asymmetric unit at the
   same time, and not in independent jobs. Phaser is not the same as
   other MR programs in this respect. Prior knowledge is used to build
   up a tree search. The correct solution for the first component may
   not be obvious until the last component is placed.
#. **Search separately for domains of flexible proteins**
   If there are domain movements, the individual domains will be better
   models giving a clearer signal than the whole protein.
#. **Use sculptor**
   For low sequence identity (<30%) or other difficult cases for which a
   default Phaser run fails, use sculptor to trim off loops that don't
   align and to prune back non-identical side chains. Test models
   derived with all the protocols. The best results are obtained with
   accurate sequence alignments, which can be obtained for instance from
   HHpred, PROMALS3D or FFAS.
#. **Use ensembler**
   Molecular replacement can be difficult when the sequence identity is
   low, but then you probably have several choices of model with
   similarly low sequence identity. Construct an ensemble by using the
   multiple structure alignment in ensembler, and try the option to trim
   poorly-conserved regions to produce a conserved core.
#. **Homology models do not have 100% identity**
   If you mutate the side chains of your model so that the sequence is
   the same as the target structure, do not enter an identity of 100%
   into Phaser. The sequence identity is used to calculate the rms
   deviation in atomic positions between the search and target
   structure, and this will only be improved slightly by the most
   sophisticated modelling algorithms. The identity you enter should
   thus be the same as that of the original sequence of your model.
#. **You may need to change the packing criteria**
   Sometimes valid solutions are excluded because the number clashes is
   higher than the allowed default number. Look in the log and summary
   files to see how many high Z-score solutions Phaser is rejecting
   because of clashes, and how many clashes there are in each case. In
   recent versions of Phaser this is less of a problem than it used to
   be, but it is still worth being aware of.
#. **Search for different anomalous scatterers concurrently**
   If there are multiple types of anomalous scatterer present in your
   crystal (such as Fe and S), Phaser will get better results by
   distinguishing between them. If there is a good signal, the correct
   hand can be identified as the one with the highest LLG score.
#. **Use the MR-SAD mode**
   After molecular replacement, if there is significant anomalous signal
   in your data, use the MR-SAD mode to identify the anomalous
   scatterers and improve the phase information.
#. **Install the latest version of Phaser**
   We work hard so you don't have to!

.. raw:: html

   <div class="printfooter">

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="column-one">

.. raw:: html

   <div id="p-logo" class="portlet">

` <http://www.phaser.cimr.cam.ac.uk/index.php/Phaser_Crystallographic_Software>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-" class="portlet">

.. rubric:: 
   :name: section

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-PhaserWiki-Home">

   .. raw:: html

      </div>

   `PhaserWiki Home <../../../../index.html>`__
-  

   .. raw:: html

      <div id="n-Releases">

   .. raw:: html

      </div>

   `Releases <../../../../articles/r/e/l/Releases.html>`__
-  

   .. raw:: html

      <div id="n-Downloads">

   .. raw:: html

      </div>

   `Downloads <../../../../articles/d/o/w/Downloads.html>`__
-  

   .. raw:: html

      <div id="n-Manuals">

   .. raw:: html

      </div>

   `Manuals <../../../../articles/m/a/n/Manuals.html>`__
-  

   .. raw:: html

      <div id="n-Tutorials">

   .. raw:: html

      </div>

   `Tutorials <../../../../articles/t/u/t/Tutorials.html>`__
-  

   .. raw:: html

      <div id="n-FAQ">

   .. raw:: html

      </div>

   `FAQ <../../../../articles/f/a/q/FAQ_1fe9.html>`__
-  

   .. raw:: html

      <div id="n-Top-Ten-Tips">

   .. raw:: html

      </div>

   `Top Ten Tips <../../../../articles/t/o/p/Top_Ten_Tips_3fcd.html>`__
-  

   .. raw:: html

      <div id="n-Publications">

   .. raw:: html

      </div>

   `Publications <../../../../articles/p/u/b/Publications.html>`__
-  

   .. raw:: html

      <div id="n-External-Links">

   .. raw:: html

      </div>

   `External
   Links <../../../../articles/e/x/t/External_Links_ff0e.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-users" class="portlet">

.. rubric:: users
   :name: users

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-MR-Phasing">

   .. raw:: html

      </div>

   `MR
   Phasing <../../../../articles/m/o/l/Molecular_Replacement_a23a.html>`__
-  

   .. raw:: html

      <div id="n-SAD-Phasing">

   .. raw:: html

      </div>

   `SAD
   Phasing <../../../../articles/e/x/p/Experimental_Phasing_5400.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-developers" class="portlet">

.. rubric:: developers
   :name: developers

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Python-Interface">

   .. raw:: html

      </div>

   `Python
   Interface <../../../../articles/p/y/t/Python_Interface_5961.html>`__
-  

   .. raw:: html

      <div id="n-Contact-Developers">

   .. raw:: html

      </div>

   `Contact Developers <../../../../articles/c/o/n/Contact.html>`__
-  

   .. raw:: html

      <div id="n-Developer-Pages">

   .. raw:: html

      </div>

   `Developer Pages <../../../../articles/d/e/v/Developers.html>`__
-  

   .. raw:: html

      <div id="n-Licences">

   .. raw:: html

      </div>

   `Licences <../../../../articles/l/i/c/Licences.html>`__
-  

   .. raw:: html

      <div id="n-SVN-Access">

   .. raw:: html

      </div>

   `SVN Access <../../../../articles/s/v/n/SVN_Repository_93a5.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   <div id="footer">

.. raw:: html

   </div>

.. raw:: html

   </div>
