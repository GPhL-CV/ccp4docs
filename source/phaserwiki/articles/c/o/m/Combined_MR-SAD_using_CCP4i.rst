.. raw:: html

   <div id="globalWrapper">

.. raw:: html

   <div id="column-content">

.. raw:: html

   <div id="content">

.. rubric:: Combined MR-SAD using CCP4i
   :name: combined-mr-sad-using-ccp4i
   :class: firstHeading

.. raw:: html

   <div id="bodyContent">

.. rubric:: From Phaserwiki
   :name: siteSub

.. raw:: html

   <div id="contentSub">

.. raw:: html

   </div>

.. raw:: html

   <div id="mw-content-text" class="mw-content-ltr" lang="en" dir="ltr">

All files for this tutorial are distributed from the Phaser web page

::

    Reflection data: lyso2001_scala1.mtz
    Lactalbumin model: 1fkq_prot.pdb
    Sequence file: hewl.pir
    Alignment file: hewl_sequences.aln

This tutorial illustrates a common molecular replacement/experimental
phasing scenario, when refinement is hindered by very strong model bias,
but there is some experimental phasing signal available.

Goat α-lactalbumin is 45% identical to hen egg-white lysozyme. Although
it is possible to solve lysozyme using α-lactalbumin as a model, it is
very difficult to refine the structure, partly because of model bias.
Unfortunately, low solvent content of this crystal form limits the
ability of density modification to remove the bias. However, one can use
anomalous scattering from intrinsic sulfur atoms to improve phases
dramatically. It is noteworthy that the anomalous signal from the sulfur
atoms is not sufficient for ab initio phasing (it is not possible to
locate the anomalous scatterers from the data alone).

#. Solve the structure with the α-lactalbumin model. Follow the
   "Molecular replacement tutorial" if necessary. Better results will be
   achieved if you run sculptor to modify the model on the basis of the
   supplied sequence alignment.
#. For a fairer comparison of phase quality, we will treat the molecular
   replacement solution as a source of experimental phase information.
   (If you use the "automated model building starting from PDB file"
   mode, the current version of ARP/wARP will be able to build the
   structure, but older versions coupled with older versions of Refmac5
   failed.) Do a quick solvent flattening with Parrot (choose "Use
   PHI/FOM instead of HL coefficients" because the MTZ file produced
   after MR doesn't include Hendrickson-Lattman coefficients; set
   PHI=PHIC and FOM=FOM; select "Use map coefficients", then set F=FWT
   and PHI=PHWT).
#. Start up ARP/wARP Classic in "automated model building starting from
   experimental phases" mode. Select the MTZ file from Parrot. To start
   from the Parrot map, select "Use a different (pre-weighted) Fobs for
   initial map calculation" under the ARP/wARP flow parameters folder,
   then set FBEST=parrot.F\_phi.F, PHIB=parrot.F\_phi.phi and
   FOM=Unassigned. Select the sequence file, and note there are 129
   residues in lysozyme. Select "use" the Free R flag. To save time, do
   3 cycles of autobuilding instead of 10.
#. Now add the S-SAD phase information. Bring up the GUI for Phaser SAD
   pipeline in the Automated Search & Phasing section of the
   Experimental Phasing module
#. All the yellow boxes need to be filled in.

   -  Set "Mode for experimental phasing" to SAD with molecular
      replacement partial structure.
   -  Uncheck the Parrot and Buccaneer steps of the pipeline (to allow
      control for better comparison with the MR model alone).
   -  Set LLG-map calculation atom type to S.
   -  Under the "Define atoms" heading, set "Partial structure" to the
      molecular replacement solution (output PDB-file) you have obtained
      in step 1. You can enter either the refined estimated RMSD from
      the molecular replacement step or the sequence identity.

#. Run Phaser after you entered all the information.
#. Solvent flatten with Parrot using a similar protocol to step 2.
   However, you should \*not\* choose "Use PHI/FOM instead of HL
   coefficients". Choose the HLanomA/B/C/D coefficients because these
   describe the phase information obtained only from the anomalous
   scattering information and not from the molecular replacement model;
   using HLA/B/C/D would include the model phase information, which
   would bias maps from subsequent cycles of phase improvement to look
   like the model.
#. Run ARP/wARP using a similar protocol as in step 3, except you should
   open the Refmac parameters folder and choose the option to include HL
   phase restraints. In the MTZ data section, choose the HLanomA/B/C/D
   coefficients.
#. How many anomalous scatterers has Phaser found? Check them against
   the model and guess what they may be! Why is it not important to
   specify the exact element type in this case?
#. If you did not know the correct space group (from the MR step), would
   you have to run Phaser SAD-phasing twice?
#. Compare the two ARP/wARP runs! Which one has built more residues?

.. raw:: html

   </div>

.. raw:: html

   <div class="printfooter">

.. raw:: html

   </div>

.. raw:: html

   <div id="catlinks">

.. raw:: html

   <div id="catlinks" class="catlinks">

.. raw:: html

   <div id="mw-normal-catlinks" class="mw-normal-catlinks">

`Category <http://localhost/index.php/Special:Categories>`__:

-  `Tutorial <../../../../articles/t/u/t/Category:Tutorial.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="column-one">

.. raw:: html

   <div id="p-cactions" class="portlet">

.. rubric:: Views
   :name: views

-  

   .. raw:: html

      <div id="ca-nstab-main">

   .. raw:: html

      </div>

   `Page <../../../../articles/c/o/m/Combined_MR-SAD_using_CCP4i.html>`__
-  

   .. raw:: html

      <div id="ca-talk">

   .. raw:: html

      </div>

   `Discussion <http://localhost/index.php?title=Talk:Combined_MR-SAD_using_CCP4i&action=edit&redlink=1>`__
-  

   .. raw:: html

      <div id="ca-current">

   .. raw:: html

      </div>

   `Latest
   revision <http://localhost/index.php/Combined_MR-SAD_using_CCP4i>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-logo" class="portlet">

` <http://www.phaser.cimr.cam.ac.uk/index.php/Phaser_Crystallographic_Software>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="p-" class="portlet">

.. rubric:: 
   :name: section

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-PhaserWiki-Home">

   .. raw:: html

      </div>

   `PhaserWiki Home <../../../../index.html>`__
-  

   .. raw:: html

      <div id="n-Releases">

   .. raw:: html

      </div>

   `Releases <../../../../articles/r/e/l/Releases.html>`__
-  

   .. raw:: html

      <div id="n-Downloads">

   .. raw:: html

      </div>

   `Downloads <../../../../articles/d/o/w/Downloads.html>`__
-  

   .. raw:: html

      <div id="n-Manuals">

   .. raw:: html

      </div>

   `Manuals <../../../../articles/m/a/n/Manuals.html>`__
-  

   .. raw:: html

      <div id="n-Tutorials">

   .. raw:: html

      </div>

   `Tutorials <../../../../articles/t/u/t/Tutorials.html>`__
-  

   .. raw:: html

      <div id="n-FAQ">

   .. raw:: html

      </div>

   `FAQ <../../../../articles/f/a/q/FAQ.html>`__
-  

   .. raw:: html

      <div id="n-Top-Ten-Tips">

   .. raw:: html

      </div>

   `Top Ten Tips <../../../../articles/t/o/p/Top_Ten_Tips.html>`__
-  

   .. raw:: html

      <div id="n-Publications">

   .. raw:: html

      </div>

   `Publications <../../../../articles/p/u/b/Publications.html>`__
-  

   .. raw:: html

      <div id="n-External-Links">

   .. raw:: html

      </div>

   `External Links <../../../../articles/e/x/t/External_Links.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-users" class="portlet">

.. rubric:: users
   :name: users

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-MR-Phasing">

   .. raw:: html

      </div>

   `MR
   Phasing <../../../../articles/m/o/l/Molecular_Replacement.html>`__
-  

   .. raw:: html

      <div id="n-SAD-Phasing">

   .. raw:: html

      </div>

   `SAD
   Phasing <../../../../articles/e/x/p/Experimental_Phasing.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-developers" class="portlet">

.. rubric:: developers
   :name: developers

.. raw:: html

   <div class="pBody">

-  

   .. raw:: html

      <div id="n-Python-Interface">

   .. raw:: html

      </div>

   `Python
   Interface <../../../../articles/p/y/t/Python_Interface.html>`__
-  

   .. raw:: html

      <div id="n-Contact-Developers">

   .. raw:: html

      </div>

   `Contact Developers <../../../../articles/c/o/n/Contact.html>`__
-  

   .. raw:: html

      <div id="n-Developer-Pages">

   .. raw:: html

      </div>

   `Developer Pages <../../../../articles/d/e/v/Developers.html>`__
-  

   .. raw:: html

      <div id="n-Licences">

   .. raw:: html

      </div>

   `Licences <../../../../articles/l/i/c/Licences.html>`__
-  

   .. raw:: html

      <div id="n-SVN-Access">

   .. raw:: html

      </div>

   `SVN Access <../../../../articles/s/v/n/SVN_Repository.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="p-search" class="portlet">

.. rubric:: Search
   :name: search

.. raw:: html

   <div id="searchBody" class="pBody">

.. raw:: html

   <div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="visualClear">

.. raw:: html

   </div>

.. raw:: html

   <div id="footer">

.. raw:: html

   <div id="f-poweredbyico">

|Powered by MediaWiki|

.. raw:: html

   </div>

-  

   .. raw:: html

      <div id="f-credits">

   .. raw:: html

      </div>

   This page was last modified 10:56, 6 April 2016 by `Randy
   Read <../../../../articles/r/a/n/User:Randy.html>`__. Based on work
   by `WikiSysop <../../../../articles/w/i/k/User:WikiSysop.html>`__.
-  

   .. raw:: html

      <div id="f-about">

   .. raw:: html

      </div>

   `About
   Phaserwiki <../../../../articles/a/b/o/Phaserwiki:About.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. |Powered by MediaWiki| image:: ../../../../skins/common/images/poweredby_mediawiki_88x31.png
   :width: 88px
   :height: 31px
   :target: //www.mediawiki.org/
