PXXML (CCP4: Library)
=====================

NAME
----

**pxxml** - Subroutine Library for writing XML

CONTENTS
--------

#. `DESCRIPTION <#description>`__
#. `THE XML FILE FORMAT <#fileformat>`__
#. `SUBROUTINES IN PXXML <#subroutines>`__
#. `PXXML INTERNAL COMMON BLOCKS <#commons>`__
#. `EXAMPLE PROGRAM USING PXXML <#example>`__
#. `AUTHORS AND HISTORY <#authors>`__

DESCRIPTION
-----------

The library pxxml contains subroutines for writing XML files or data
islands.

All the routines contained in this library are written in ANSI standard
FORTRAN 77.

The strange library name is to avoid conflicts with existing freeware
software namely libxml.

The first argument to all the subroutine calls in PXXML is XMLFileUnit
which is the unit of the opened or to be opened XML file. If the
XMLFileUnit is 6 then the XML will go to STDOUT.

PXXML can be used to write standard HTML by use of the root html
element.

THE XML FILE FORMAT
-------------------

General description

::

    <?xml version="1.0"?>
     <run>
      <MATTHEWS_COEF
        ccp4_version="4.1" 
        date="17/ 9/01" 
       />
      <keyword
      >
      
      </keyword>
      <cell
        volume="  66089.8438" 
       />
      <result
        nmol_in_asu=" 1" 
        matth_coeff="  2.47929788" 
        percent_solvent="  50." 
       />
      <result
        nmol_in_asu=" 2" 
        matth_coeff="  1.23964894" 
        percent_solvent="  1.58794796E-06" 
       />
     </run>

Subroutines for writing XML files
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 `XMLOpen <#xmlopen>`__
    Open the XML file and write header info.
 `XMLOpenElement <#xmlopenelement>`__
    Open an element and leave incomplete (e.g. <run without a >)
 `XMLWriteAttribute <#xmlwriteattribute>`__
    Write an attribute to an element (form attribute="value")
 `XMLWriteElement <#xmlwriteelement>`__
    Write the value of an element (and element if not already opened)
 `XMLCloseElement <#xmlcloseelement>`__
    Close an element and file if element is the root element

 Subroutine XMLOpen
-------------------

Open the XML file and write header info.

Arguments:

 XMLFileUnit (I/O) INTEGER
    file unit for XML file to be opened. If set to 6 STDOUT will be
    used.
 ROOTVALUE (I) CHARACTER\*(\*)
    Name of the root element. If set to HTML then standard PI for XML is
    not outputed.
 IFAIL (O) INTEGER
    error indicator: meaning: on output

        =0
        OK
        =-1
        no such file
        all other errors cause stop

When called the subroutine will firstly open a file if appropriate. If
the XMLFileUnit has already been set to 6 then STDOUT will be used and a
data island is written. Otherwise an xml file is opened with the name
CALLINGPROGRAM.xml. This filename can be overwritten with the
environment variable or command line option XMLFILE.

The subroutine will write the following XML to XMLFileUnit (as long as
the root element is not html). The root element specified here was run.

::

    <?xml version="1.0"?>
     <run>
      <MATTHEWS_COEF
        ccp4_version="4.1" 
        date="17/ 9/01" 
       />

 Subroutine XMLOpenElement(XMLFileUnit, ElementName, ifail)
-----------------------------------------------------------

Subroutine to open an XML element.

Arguments:

 XMLFileUnit (I/O) INTEGER
    file unit for XML file to be opened. If set to 6 STDOUT will be
    used.
 ElementName (I) CHARACTER\*(\*)
    Name of the element.
 IFAIL (O) INTEGER
    error indicator: meaning: on output

        =0
        OK
        =-1
        no such file
        all other errors cause stop

If the subroutine is called and XMLOpen had not been previously called
then XMLOpen will be called and the ElementName is assumed to be the
root element.

If XMLOpenElement is called after XMLOpen then an element of the
following style is opened:

::

      <elementname

The element name is not closed with a > as this will allow attributes to
be added.

Subroutine XMLWriteAttribute(XMLFileUnit, ElementName, AttributeName, AttributeValue, ifail)
--------------------------------------------------------------------------------------------

Write an attribute to an element (form attribute="value")

Arguments:

 XMLFileUnit (I/O) INTEGER
    file unit for XML file to be opened. If set to 6 STDOUT will be
    used.
 ElementName (I) CHARACTER\*(\*)
    Name of the element.
 AttributeName (I) CHARACTER\*(\*)
    Name of the attribute.
 AttributeValue (I) CHARACTER\*(\*)
    Value of the attribute.
 IFAIL (O) INTEGER
    error indicator: meaning: on output

        =0
        OK
        =-1
        no such file
        all other errors cause stop

The XML file unit must be opened when XMLWriteAttribute is called or the
results will be unexpected!

The actions of the subroutine are shown below

The subroutine call is:

::

     XMLWriteAttribute(XMLFileUnit, "cell", "x", "90", ifail)

1) **Example 1:** XML before the above call:

::

    <?xml version="1.0"?>
     <run>
      <MATTHEWS_COEF
        ccp4_version="4.1" 
        date="17/ 9/01" 
       />

After:

::

    <?xml version="1.0"?>
     <run>
      <MATTHEWS_COEF
        ccp4_version="4.1" 
        date="17/ 9/01" 
       />
       <cell 
        x="90"

1) **Example 2:** XML before the above call:

Subroutine XMLWriteElement(XMLFileUnit, ElementName, ElementValue, ifail)
-------------------------------------------------------------------------

Write the value of an element (and element if not already opened)

Arguments:

 XMLFileUnit (I/O) INTEGER
    file unit for XML file to be opened. If set to 6 STDOUT will be
    used.
 ElementName (I) CHARACTER\*(\*)
    Name of the element.
 ElementValue (I) CHARACTER\*(\*)
    Value of the element.
 IFAIL (O) INTEGER
    error indicator: meaning: on output

        =0
        OK
        =-1
        no such file
        all other errors cause stop

The XML file does not have to be opened when XMLWriteElement is called.
This routine can simply be called to write a file/island with the root
element and value and then close itself.

The actions of the subroutine are shown below

The subroutine call is:

::

     XMLWriteElement(XMLFileUnit, "cell", "73.5820   38.7330   23.1890 90 90 90 ", ifail)

1) **Example 1:** XML before the above call:

::

    <?xml version="1.0"?>
     <run>
      <MATTHEWS_COEF
        ccp4_version="4.1" 
        date="17/ 9/01" 
       />

After:

::

    <?xml version="1.0"?>
     <run>
      <MATTHEWS_COEF
        ccp4_version="4.1" 
        date="17/ 9/01" 
       />
       <cell>
        73.5820   38.7330   23.1890 90 90 90
       </cell>

1) **Example 2:** XML before the above call:

Subroutine XMLCloseElement(XMLFileUnit, ElementName, ifail)
-----------------------------------------------------------

Close an element and the file if its the root element

Arguments:

 XMLFileUnit (I/O) INTEGER
    file unit for XML file to be opened. If set to 6 STDOUT will be
    used.
 ElementName (I) CHARACTER\*(\*)
    Name of the element.
 IFAIL (O) INTEGER
    error indicator: meaning: on output

        =0
        OK
        =-1
        no such file
        all other errors cause stop

This subroutine will close an element. If the element specified is not
the current open element the subroutine will close the current element
then check if the next element is the specified element. The subroutine
will continue to do this until it either finds a match or reaches the
root element. When the subroutine reaches the root element it will close
it and the file.

PXXML INTERNAL COMMON BLOCKS
----------------------------

The following common block is used by PXXML and is listed here for
completeness: ccp4xmld

::

     
          integer elementstatus(80), elementnumber 
          character*80 openelements(80), XMLFileName
          
          external lenstr

          common /ccp4xmld/ elementnumber, elementstatus, openelements,
         $     XMLFileName

the element number records the depth of the element i.e. root element =
1, next one down is 2 and so on.

The purpose of the array elementstatus is to store the status of the
element - i.e. 0=closed, 1=open (not complete), 2=open (complete with
>).

The purpose of the openelements is to remember the name of all the open
elements (with or without the trailing >). The name of closed elements
is not remembered.

XMLFileName.... speaks for itself!

EXAMPLE PROGRAM USING AN XML FILE
---------------------------------

See `matthews\_coef </ccp4/src/matthews_coef.f>`__ available in version
4.2+ of ccp4.

AUTHORS AND HISTORY
-------------------

Alun Ashton Version 1.0 2001
