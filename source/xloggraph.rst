XLOGGRAPH (CCP4: Deprecated Program)
====================================

NAME
----

**xloggraph** - X-windows tool, a viewer of specially formatted \`log'
files.

SYNOPSIS
--------

**xloggraph** *logfilename*

DESCRIPTION
-----------

| xloggraph belongs to the XCCPJiffy class of simple X11 graphic based
  programs. xloggraph reads specially formatted log file produced by
  some of the CCP4 programs, recognises tables and graphs described in
  it and produces their graphic representation.
| *NB: the `loggraph <loggraph.html>`__ program offers an alternative to
  xloggraph.*

xloggraph uses an X-window device and has an option to create a
PostScript file.

Running the program xloggraph creates a Header window with boxes
showing:

 Quit
    to exit the program
 (unnamed)
    box containing current log file name
 Show Table List
    Clicking here returns to complete list of Tables in the file. This
    box is sensitive only if the index to Graphs is displayed.
 View File
    Allows one to scroll through the log file (Try CTRL/S when the
    pointer is in text window)
 Select from Tables
 Select from Graphs
    A list of the Tables/Graphs stored within the log file. Clicking on
    the particular Table name will create an index of the Graphs
    available from that particular table Clicking on the particular
    Graph name will generate a Graph in an X-window.
    Quit - in that window closes the window
    Plot - creates a PostScript version. (If you want a file go to the
    Control Panel first.)
 Control Panel
    Clicking Left Mouse button here allows one to Toggle between
    Portrait and Landscape for the output Postscript file conversion and
    the option to specify a Output File for the Postscript file. The
    print command is controlled by the XCCPJiffy\*psPlotCommand
    resource. The Quit button exits the control panel.

REMARK FOR APPLICATION PROGRAMMERS
----------------------------------

The sections on the syntax for graphs, text, keytext and summaries can
be found in a separate document on the `loggraph
format <loggraphformat.html>`__.

NOTE ON COLOURS AND LINE STYLES - For Users and Programmers
-----------------------------------------------------------

xloggraph works in pen graphic mode, each object is painted by a
different virtual pen. By definition, xloggraph uses pen1 for Graph axes
and texts, pen2 for 1st graph, pen3 for the second etc. Currently 10
pens are available.

Colour, Line width and Postscript properties of virtual pens are
controlled via the entries in X-resource database (see X manual). It is
recommended to load X-resources into the X-server database (xrdb). Once
the database is loaded, one can optimise it using xrdb to load/merge a
personalised set of X-Application Resources.

X pen is controlled by:

::


    XCCPJiffy.graph*draw2d.background:              white
    XCCPJiffy.graph*draw2d.pen1Color:               black
    XCCPJiffy.graph*draw2d.pen1LineWidth:           1
       .
       .
       .
    XCCPJiffy.graph*draw2d.pen10Color:              indigo
    XCCPJiffy.graph*draw2d.pen10LineWidth:          1

Postscript pen by:

::


    XCCPJiffy*psPen1Attr:   1 setlinejoin 0.5 setlinewidth [] 0 setdash
       .
       .
       .
    XCCPJiffy*psPen10Attr:  1 setlinejoin 0.9 setlinewidth [8 2] 0 setdash

| To check your current setting use for instance
| xrdb -query \| grep XCCPJiffy \| more

SEE ALSO
--------

`loggraph format <loggraphformat.html>`__, `loggraph <loggraph.html>`__,
X (1), xccpjiffy2idraw (1), xrdb (1)

ENVIRONMENT
-----------

DISPLAY to figure out which display to use.

AUTHOR
------

Jan Zelinka - York University
