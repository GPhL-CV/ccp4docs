`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

csymlib\_f.c File Reference
===========================

Fortran API for symmetry information. `More... <#_details>`__

| ``#include <stdio.h>``
| ``#include <string.h>``
| ``#include <stdlib.h>``
| ``#include <math.h>``
| ``#include "ccp4_fortran.h"``
| ``#include "ccp4_general.h"``
| ``#include "ccp4_parser.h"``
| ``#include "csymlib.h"``
| ``#include "cmtzlib.h"``
| ``#include "cvecmat.h"``

| 

Defines
-------

 #define 

**CSYMLIB\_DEBUG**\ (x)

 #define 

**MSPAC**   4

 #define 

**MAXSYM**   192

 #define 

**MAXSYMOPS**   20

 #define 

**MAXLENSYMOPSTR**   80

| 

Functions
---------

 void 

**ccp4spg\_mem\_tidy** (void)

  

**FORTRAN\_SUBR** (INVSYM, invsym,(const float a[4][4], float
ai[4][4]),(const float a[4][4], float ai[4][4]),(const float a[4][4],
float ai[4][4]))

 int 

**symfr\_driver** (const char \*line, float rot[MAXSYMOPS][4][4])

  

**FORTRAN\_SUBR** (SYMFR3, symfr3,(const fpstr icol, const int \*i1, int
\*nsym, float rot[MAXSYM][4][4], int \*eflag, int icol\_len),(const
fpstr icol, const int \*i1, int \*nsym, float rot[MAXSYM][4][4], int
\*eflag),(const fpstr icol, int icol\_len, const int \*i1, int \*nsym,
float rot[MAXSYM][4][4], int \*eflag))

  

**FORTRAN\_SUBR** (SYMFR2, symfr2,(fpstr symchs, int \*icol, int \*nsym,
float rot[MAXSYM][4][4], int symchs\_len),(fpstr symchs, int \*icol, int
\*nsym, float rot[MAXSYM][4][4]),(fpstr symchs, int symchs\_len, int
\*icol, int \*nsym, float rot[MAXSYM][4][4]))

 

`FORTRAN\_SUBR <csymlib__f_8c.html#a14>`__ (SYMTR3, symtr3,(const int
\*nsm, const float rsm[MAXSYM][4][4], fpstr symchs, const int \*iprint,
int symchs\_len),(const int \*nsm, const float rsm[MAXSYM][4][4], fpstr
symchs, const int \*iprint),(const int \*nsm, const float
rsm[MAXSYM][4][4], fpstr symchs, int symchs\_len, const int \*iprint))

 

`FORTRAN\_SUBR <csymlib__f_8c.html#a15>`__ (SYMTR4, symtr4,(const int
\*nsm, const float rsm[MAXSYM][4][4], fpstr symchs, int
symchs\_len),(const int \*nsm, const float rsm[MAXSYM][4][4], fpstr
symchs),(const int \*nsm, const float rsm[MAXSYM][4][4], fpstr symchs,
int symchs\_len))

  

**FORTRAN\_SUBR** (PGMDF, pgmdf,(int \*jlass, int \*jcentr, int
jscrew[3]),(int \*jlass, int \*jcentr, int jscrew[3]),(int \*jlass, int
\*jcentr, int jscrew[3]))

  

**FORTRAN\_SUBR** (PGDEFN, pgdefn,(fpstr nampg, int \*nsymp, const int
\*nsym, float rsmt[192][4][4], const ftn\_logical \*lprint, int
nampg\_len),(fpstr nampg, int \*nsymp, const int \*nsym, float
rsmt[192][4][4], const ftn\_logical \*lprint),(fpstr nampg, int
nampg\_len, int \*nsymp, const int \*nsym, float rsmt[192][4][4], const
ftn\_logical \*lprint))

 

`FORTRAN\_SUBR <csymlib__f_8c.html#a18>`__ (PGNLAU, pgnlau,(const fpstr
nampg, int \*nlaue, fpstr launam, int nampg\_len, int
launam\_len),(const fpstr nampg, int \*nlaue, fpstr launam),(const fpstr
nampg, int nampg\_len, int \*nlaue, fpstr launam, int launam\_len))

 

`FORTRAN\_SUBR <csymlib__f_8c.html#a19>`__ (CCP4SPG\_F\_GET\_LAUE,
ccp4spg\_f\_get\_laue,(const int \*sindx, int \*nlaue, fpstr launam, int
launam\_len),(const int \*sindx, int \*nlaue, fpstr launam),(const int
\*sindx, int \*nlaue, fpstr launam, int launam\_len))

 

`FORTRAN\_SUBR <csymlib__f_8c.html#a20>`__ (HKLRANGE, hklrange,(int
\*ihrng0, int \*ihrng1, int \*ikrng0, int \*ikrng1, int \*ilrng0, int
\*ilrng1),(int \*ihrng0, int \*ihrng1, int \*ikrng0, int \*ikrng1, int
\*ilrng0, int \*ilrng1),(int \*ihrng0, int \*ihrng1, int \*ikrng0, int
\*ikrng1, int \*ilrng0, int \*ilrng1))

 

`FORTRAN\_SUBR <csymlib__f_8c.html#a21>`__ (PATSGP, patsgp,(const fpstr
spgnam, const fpstr pgname, fpstr patnam, int \*lpatsg, int spgnam\_len,
int pgname\_len, int patnam\_len),(const fpstr spgnam, const fpstr
pgname, fpstr patnam, int \*lpatsg),(const fpstr spgnam, int
spgnam\_len, const fpstr pgname, int pgname\_len, fpstr patnam, int
patnam\_len, int \*lpatsg))

 

`FORTRAN\_SUBR <csymlib__f_8c.html#a22>`__ (ASUSET, asuset,(fpstr
spgnam, int \*numsgp, fpstr pgname, int \*msym, float rrsym[192][4][4],
int \*msymp, int \*mlaue, ftn\_logical \*lprint, int spgnam\_len, int
pgname\_len),(fpstr spgnam, int \*numsgp, fpstr pgname, int \*msym,
float rrsym[192][4][4], int \*msymp, int \*mlaue, ftn\_logical
\*lprint),(fpstr spgnam, int spgnam\_len, int \*numsgp, fpstr pgname,
int pgname\_len, int \*msym, float rrsym[192][4][4], int \*msymp, int
\*mlaue, ftn\_logical \*lprint))

 

`FORTRAN\_SUBR <csymlib__f_8c.html#a23>`__ (ASUSYM, asusym,(float
rassym[384][4][4], float rinsym[384][4][4], int \*nisym),(float
rassym[384][4][4], float rinsym[384][4][4], int \*nisym),(float
rassym[384][4][4], float rinsym[384][4][4], int \*nisym))

 

`FORTRAN\_SUBR <csymlib__f_8c.html#a24>`__ (ASUPUT, asuput,(const int
ihkl[3], int jhkl[3], int \*isym),(const int ihkl[3], int jhkl[3], int
\*isym),(const int ihkl[3], int jhkl[3], int \*isym))

 

`FORTRAN\_SUBR <csymlib__f_8c.html#a25>`__ (ASUGET, asuget,(const int
ihkl[3], int jhkl[3], const int \*isym),(const int ihkl[3], int jhkl[3],
const int \*isym),(const int ihkl[3], int jhkl[3], const int \*isym))

 

`FORTRAN\_SUBR <csymlib__f_8c.html#a26>`__ (ASUPHP, asuphp,(const int
jhkl[3], const int \*lsym, const int \*isign, const float \*phasin,
float \*phasout),(const int jhkl[3], const int \*lsym, const int
\*isign, const float \*phasin, float \*phasout),(const int jhkl[3],
const int \*lsym, const int \*isign, const float \*phasin, float
\*phasout))

 

`FORTRAN\_SUBR <csymlib__f_8c.html#a27>`__ (CCP4SPG\_F\_LOAD\_BY\_NAME,
ccp4spg\_f\_load\_by\_name,(const int \*sindx, fpstr namspg, int
namspg\_len),(const int \*sindx, fpstr namspg),(const int \*sindx, fpstr
namspg, int namspg\_len))

 

`FORTRAN\_SUBR <csymlib__f_8c.html#a28>`__ (CCP4SPG\_F\_LOAD\_BY\_OPS,
ccp4spg\_f\_load\_by\_ops,(const int \*sindx, int \*msym, float
rrsym[192][4][4]),(const int \*sindx, int \*msym, float
rrsym[192][4][4]),(const int \*sindx, int \*msym, float
rrsym[192][4][4]))

 

`FORTRAN\_FUN <csymlib__f_8c.html#a29>`__ (int,
CCP4SPG\_F\_EQUAL\_OPS\_ORDER, ccp4spg\_f\_equal\_ops\_order,(int
\*msym1, float rrsym1[192][4][4], int \*msym2, float
rrsym2[192][4][4]),(int \*msym1, float rrsym1[192][4][4], int \*msym2,
float rrsym2[192][4][4]),(int \*msym1, float rrsym1[192][4][4], int
\*msym2, float rrsym2[192][4][4]))

 

`FORTRAN\_SUBR <csymlib__f_8c.html#a30>`__ (CCP4SPG\_F\_ASUPUT,
ccp4spg\_f\_asuput,(const int \*sindx, const int ihkl[3], int jhkl[3],
int \*isym),(const int \*sindx, const int ihkl[3], int jhkl[3], int
\*isym),(const int \*sindx, const int ihkl[3], int jhkl[3], int \*isym))

 

`FORTRAN\_FUN <csymlib__f_8c.html#a31>`__ (int, INASU, inasu,(const int
ihkl[3], const int \*nlaue),(const int ihkl[3], const int
\*nlaue),(const int ihkl[3], const int \*nlaue))

 

`FORTRAN\_FUN <csymlib__f_8c.html#a32>`__ (int, CCP4SPG\_F\_INASU,
ccp4spg\_f\_inasu,(const int \*sindx, const int ihkl[3]),(const int
\*sindx, const int ihkl[3]),(const int \*sindx, const int ihkl[3]))

  

**FORTRAN\_SUBR** (PRTRSM, prtrsm,(const fpstr pgname, const int
\*nsymp, const float rsymiv[192][4][4], int pgname\_len),(const fpstr
pgname, const int \*nsymp, const float rsymiv[192][4][4]),(const fpstr
pgname, int pgname\_len, const int \*nsymp, const float
rsymiv[192][4][4]))

 void 

**ccp4spg\_register\_by\_ccp4\_num** (int numspg)

 void 

**ccp4spg\_register\_by\_symops** (int nops, float rsm[][4][4])

 

`FORTRAN\_SUBR <csymlib__f_8c.html#a36>`__ (MSYMLB3, msymlb3,(const int
\*ist, int \*lspgrp, fpstr namspg\_cif, fpstr namspg\_cifs, fpstr nampg,
int \*nsymp, int \*nsym, float rlsymmmatrx[192][4][4], int
namspg\_cif\_len, int namspg\_cifs\_len, int nampg\_len),(const int
\*ist, int \*lspgrp, fpstr namspg\_cif, fpstr namspg\_cifs, fpstr nampg,
int \*nsymp, int \*nsym, float rlsymmmatrx[192][4][4]),(const int \*ist,
int \*lspgrp, fpstr namspg\_cif, int namspg\_cif\_len, fpstr
namspg\_cifs, int namspg\_cifs\_len, fpstr nampg, int nampg\_len, int
\*nsymp, int \*nsym, float rlsymmmatrx[192][4][4]))

  

**FORTRAN\_SUBR** (MSYMLB, msymlb,(const int \*ist, int \*lspgrp, fpstr
namspg\_cif, fpstr nampg, int \*nsymp, int \*nsym, float
rlsymmmatrx[192][4][4], int namspg\_cif\_len, int nampg\_len),(const int
\*ist, int \*lspgrp, fpstr namspg\_cif, fpstr nampg, int \*nsymp, int
\*nsym, float rlsymmmatrx[192][4][4]),(const int \*ist, int \*lspgrp,
fpstr namspg\_cif, int namspg\_cif\_len, fpstr nampg, int nampg\_len,
int \*nsymp, int \*nsym, float rlsymmmatrx[192][4][4]))

  

**FORTRAN\_SUBR** (MSYGET, msyget,(const int \*ist, int \*lspgrp, int
\*nsym, float rlsymmmatrx[192][4][4]),(const int \*ist, int \*lspgrp,
int \*nsym, float rlsymmmatrx[192][4][4]),(const int \*ist, int
\*lspgrp, int \*nsym, float rlsymmmatrx[192][4][4]))

 

`FORTRAN\_SUBR <csymlib__f_8c.html#a39>`__ (EPSLN, epsln,(const int
\*nsm, const int \*nsmp, const float rsm[192][4][4], const int
\*iprint),(const int \*nsm, const int \*nsmp, const float
rsm[192][4][4], const int \*iprint),(const int \*nsm, const int \*nsmp,
const float rsm[192][4][4], const int \*iprint))

  

**FORTRAN\_SUBR** (EPSLON, epslon,(const int ih[3], float \*epsi, int
\*isysab),(const int ih[3], float \*epsi, int \*isysab),(const int
ih[3], float \*epsi, int \*isysab))

  

**FORTRAN\_SUBR** (CCP4SPG\_F\_EPSLON, ccp4spg\_f\_epslon,(const int
\*sindx, const int ih[3], float \*epsi, int \*isysab),(const int
\*sindx, const int ih[3], float \*epsi, int \*isysab),(const int
\*sindx, const int ih[3], float \*epsi, int \*isysab))

  

**FORTRAN\_SUBR** (SYSAB, sysab,(const int in[3], int \*isysab),(const
int in[3], int \*isysab),(const int in[3], int \*isysab))

  

**FORTRAN\_SUBR** (CCP4SPG\_F\_IS\_SYSABS, ccp4spg\_f\_is\_sysabs,(const
int \*sindx, const int in[3], int \*isysab),(const int \*sindx, const
int in[3], int \*isysab),(const int \*sindx, const int in[3], int
\*isysab))

 

`FORTRAN\_SUBR <csymlib__f_8c.html#a44>`__ (CENTRIC, centric,(const int
\*nsm, const float rsm[192][4][4], const int \*iprint),(const int \*nsm,
const float rsm[192][4][4], const int \*iprint),(const int \*nsm, const
float rsm[192][4][4], const int \*iprint))

  

**FORTRAN\_SUBR** (CENTR, centr,(const int ih[3], int \*ic),(const int
ih[3], int \*ic),(const int ih[3], int \*ic))

  

**FORTRAN\_SUBR** (CCP4SPG\_F\_IS\_CENTRIC,
ccp4spg\_f\_is\_centric,(const int \*sindx, const int ih[3], int
\*ic),(const int \*sindx, const int ih[3], int \*ic),(const int \*sindx,
const int ih[3], int \*ic))

  

**FORTRAN\_SUBR** (CENTPHASE, centphase,(const int ih[3], float
\*cenphs),(const int ih[3], float \*cenphs),(const int ih[3], float
\*cenphs))

  

**FORTRAN\_SUBR** (CCP4SPG\_F\_CENTPHASE, ccp4spg\_f\_centphase,(const
int \*sindx, const int ih[3], float \*cenphs),(const int \*sindx, const
int ih[3], float \*cenphs),(const int \*sindx, const int ih[3], float
\*cenphs))

  

**FORTRAN\_SUBR** (SETLIM, setlim,(const int \*lspgrp, float
xyzlim[3][2]),(const int \*lspgrp, float xyzlim[3][2]),(const int
\*lspgrp, float xyzlim[3][2]))

  

**FORTRAN\_SUBR** (SETGRD, setgrd,(const int \*nlaue, const float
\*sample, const int \*nxmin, const int \*nymin, const int \*nzmin, int
\*nx, int \*ny, int \*nz),(const int \*nlaue, const float \*sample,
const int \*nxmin, const int \*nymin, const int \*nzmin, int \*nx, int
\*ny, int \*nz),(const int \*nlaue, const float \*sample, const int
\*nxmin, const int \*nymin, const int \*nzmin, int \*nx, int \*ny, int
\*nz))

  

**FORTRAN\_SUBR** (FNDSMP, fndsmp,(const int \*minsmp, const int \*nmul,
const float \*sample, int \*nsampl),(const int \*minsmp, const int
\*nmul, const float \*sample, int \*nsampl),(const int \*minsmp, const
int \*nmul, const float \*sample, int \*nsampl))

  

**FORTRAN\_SUBR** (CALC\_ORIG\_PS, calc\_orig\_ps,(fpstr namspg\_cif,
int \*nsym, float rsym[192][4][4], int \*norig, float orig[96][3],
ftn\_logical \*lpaxisx, ftn\_logical \*lpaxisy, ftn\_logical \*lpaxisz,
int namspg\_cif\_len),(fpstr namspg\_cif, int \*nsym, float
rsym[192][4][4], int \*norig, float orig[96][3], ftn\_logical \*lpaxisx,
ftn\_logical \*lpaxisy, ftn\_logical \*lpaxisz, int
namspg\_cif\_len),(fpstr namspg\_cif, int namspg\_cif\_len, int \*nsym,
float rsym[192][4][4], int \*norig, float orig[96][3], ftn\_logical
\*lpaxisx, ftn\_logical \*lpaxisy, ftn\_logical \*lpaxisz))

  

**FORTRAN\_SUBR** (SETRSL, setrsl,(const float \*a, const float \*b,
const float \*c, const float \*alpha, const float \*beta, const float
\*gamma),(const float \*a, const float \*b, const float \*c, const float
\*alpha, const float \*beta, const float \*gamma),(const float \*a,
const float \*b, const float \*c, const float \*alpha, const float
\*beta, const float \*gamma))

  

**FORTRAN\_SUBR** (STHLSQ1, sthlsq1,(float \*reso, const int \*ih, const
int \*ik, const int \*il),(float \*reso, const int \*ih, const int \*ik,
const int \*il),(flaot \*reso, const int \*ih, const int \*ik, const int
\*il))

  

**FORTRAN\_SUBR** (STS3R41, sts3r41,(float \*reso, const int \*ih, const
int \*ik, const int \*il),(float \*reso, const int \*ih, const int \*ik,
const int \*il),(float \*reso, const int \*ih, const int \*ik, const int
\*il))

  

**FORTRAN\_SUBR** (HANDCHANGE, handchange,(const int \*lspgrp, float
\*cx, float \*cy, float \*cz),(const int \*lspgrp, float \*cx, float
\*cy, float \*cz),(const int \*lspgrp, float \*cx, float \*cy, float
\*cz))

--------------

Detailed Description
--------------------

Fortran API for symmetry information.

 **Author:**
    Martyn Winn

--------------

Function Documentation
----------------------

FORTRAN\_FUN

( 

int 

 ,

CCP4SPG\_F\_INASU 

 ,

ccp4spg\_f\_inasu 

 ,

(const int \*sindx, const int ihkl[3]) 

 ,

(const int \*sindx, const int ihkl[3]) 

 ,

(const int \*sindx, const int ihkl[3]) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Test whether reflection or it's      |
|                                      | Friedel mate is in the asymmetric    |
|                                      | unit of the spacegroup on index      |
|                                      | "sindx".                             |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | -----------+                         |
|                                      |     | *sindx*    | index of this spa |
|                                      | cegroup.   |                         |
|                                      |     +------------+------------------ |
|                                      | -----------+                         |
|                                      |     | *ihkl*     | reflection indice |
|                                      | s.         |                         |
|                                      |     +------------+------------------ |
|                                      | -----------+                         |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if in asu, -1 if -h -k -l is   |
|                                      |     in asu, 0 otherwise              |
+--------------------------------------+--------------------------------------+

FORTRAN\_FUN

( 

int 

 ,

INASU 

 ,

inasu 

 ,

(const int ihkl[3], const int \*nlaue) 

 ,

(const int ihkl[3], const int \*nlaue) 

 ,

(const int ihkl[3], const int \*nlaue) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Test whether reflection or it's      |
|                                      | Friedel mate is in asu. The argument |
|                                      | nlaue is checked against the value   |
|                                      | for the current spacegroup: if it    |
|                                      | differs then spacegroup->nlaue is    |
|                                      | updated temporarily.                 |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | -----+                               |
|                                      |     | *ihkl*     | reflection indice |
|                                      | s.   |                               |
|                                      |     +------------+------------------ |
|                                      | -----+                               |
|                                      |     | *nlaue*    | Laue group number |
|                                      | .    |                               |
|                                      |     +------------+------------------ |
|                                      | -----+                               |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if in asu, -1 if -h -k -l is   |
|                                      |     in asu, 0 otherwise              |
+--------------------------------------+--------------------------------------+

FORTRAN\_FUN

( 

int 

 ,

CCP4SPG\_F\_EQUAL\_OPS\_ORDER 

 ,

ccp4spg\_f\_equal\_ops\_order 

 ,

(int \*msym1, float rrsym1[192][4][4], int \*msym2, float
rrsym2[192][4][4]) 

 ,

(int \*msym1, float rrsym1[192][4][4], int \*msym2, float
rrsym2[192][4][4]) 

 ,

(int \*msym1, float rrsym1[192][4][4], int \*msym2, float
rrsym2[192][4][4]) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Compare two sets of symmetry         |
|                                      | operators to see if they are in the  |
|                                      | same order. This is important for    |
|                                      | the consistent use of ISYM which     |
|                                      | encodes the operator position in the |
|                                      | list.                                |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |     | *msym1*     | number of symmet |
|                                      | ry matrices passed in first list.    |
|                                      |  |                                   |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |     | *rrsym1*    | first list of sy |
|                                      | mmetry matrices.                     |
|                                      |  |                                   |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |     | *msym2*     | number of symmet |
|                                      | ry matrices passed in second list.   |
|                                      |  |                                   |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |     | *rrsym2*    | second list of s |
|                                      | ymmetry matrices.                    |
|                                      |  |                                   |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | -+                                   |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     1 if operator lists are equal    |
|                                      |     and in the same order, 0         |
|                                      |     otherwise                        |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

CENTRIC 

 ,

centric 

 ,

(const int \*nsm, const float rsm[192][4][4], const int \*iprint) 

 ,

(const int \*nsm, const float rsm[192][4][4], const int \*iprint) 

 ,

(const int \*nsm, const float rsm[192][4][4], const int \*iprint) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Set up centric zones based on        |
|                                      | symmetry operators. Convention:      |
|                                      | translations are in rsm[isym][3][\*] |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | --------+                            |
|                                      |     | *nsm*       | number of symmet |
|                                      | ry matrices passed.                  |
|                                      |         |                            |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | --------+                            |
|                                      |     | *rsm*       | symmetry matrice |
|                                      | s.                                   |
|                                      |         |                            |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | --------+                            |
|                                      |     | *iprint*    | If iprint > 0 th |
|                                      | en a summary of centric zones is pri |
|                                      | nted.   |                            |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | --------+                            |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

EPSLN 

 ,

epsln 

 ,

(const int \*nsm, const int \*nsmp, const float rsm[192][4][4], const
int \*iprint) 

 ,

(const int \*nsm, const int \*nsmp, const float rsm[192][4][4], const
int \*iprint) 

 ,

(const int \*nsm, const int \*nsmp, const float rsm[192][4][4], const
int \*iprint) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Epsilon zones currently set up in    |
|                                      | ccp4spg\_load\_spacegroup If these   |
|                                      | are not available, use lookup by     |
|                                      | symops.                              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | --------+                            |
|                                      |     | *nsm*       | number of symmet |
|                                      | ry operators.                        |
|                                      |         |                            |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | --------+                            |
|                                      |     | *nsmp*      | number of primit |
|                                      | ive symmetry operators.              |
|                                      |         |                            |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | --------+                            |
|                                      |     | *rsm*       | symmetry matrice |
|                                      | s.                                   |
|                                      |         |                            |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | --------+                            |
|                                      |     | *iprint*    | If iprint > 0 th |
|                                      | en a summary of epsilon zones is pri |
|                                      | nted.   |                            |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | --------+                            |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

MSYMLB3 

 ,

msymlb3 

 ,

(const int \*ist, int \*lspgrp, fpstr namspg\_cif, fpstr namspg\_cifs,
fpstr nampg, int \*nsymp, int \*nsym, float rlsymmmatrx[192][4][4], int
namspg\_cif\_len, int namspg\_cifs\_len, int nampg\_len) 

 ,

(const int \*ist, int \*lspgrp, fpstr namspg\_cif, fpstr namspg\_cifs,
fpstr nampg, int \*nsymp, int \*nsym, float rlsymmmatrx[192][4][4]) 

 ,

(const int \*ist, int \*lspgrp, fpstr namspg\_cif, int namspg\_cif\_len,
fpstr namspg\_cifs, int namspg\_cifs\_len, fpstr nampg, int nampg\_len,
int \*nsymp, int \*nsym, float rlsymmmatrx[192][4][4]) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Fortran wrapper for                  |
|                                      | ccp4spg\_load\_by\_\* functions.     |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------------+----------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
|                                      |     | *ist*             | Obsolete p |
|                                      | arameter.                            |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                |                     |
|                                      |     +-------------------+----------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
|                                      |     | *lspgrp*          | Spacegroup |
|                                      |  number in CCP4 convention. If set o |
|                                      | n entry, used to search for spacegro |
|                                      | up. Returned value is that found.    |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                |                     |
|                                      |     +-------------------+----------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
|                                      |     | *namspg\_cif*     | Spacegroup |
|                                      |  name. If set on entry, used to sear |
|                                      | ch for spacegroup. Returned value is |
|                                      |  the full extended Hermann Mauguin s |
|                                      | ymbol, with one slight alteration. S |
|                                      | ymbols such as 'R 3 :H' are converte |
|                                      | d to 'H 3'. This is for backwards co |
|                                      | mpatibility.   |                     |
|                                      |     +-------------------+----------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
|                                      |     | *namspg\_cifs*    | On output, |
|                                      |  contains the spacegroup name withou |
|                                      | t any spaces.                        |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                |                     |
|                                      |     +-------------------+----------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
|                                      |     | *nampg*           | On output, |
|                                      |  the point group name.               |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                |                     |
|                                      |     +-------------------+----------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
|                                      |     | *nsymp*           | On output, |
|                                      |  the number of primitive symmetry op |
|                                      | erators.                             |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                |                     |
|                                      |     +-------------------+----------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
|                                      |     | *nsym*            | On output, |
|                                      |  the total number of symmetry operat |
|                                      | ors.                                 |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                |                     |
|                                      |     +-------------------+----------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
|                                      |     | *rlsymmmatrx*     | On output, |
|                                      |  the symmetry operators.             |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                                      |
|                                      |                |                     |
|                                      |     +-------------------+----------- |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | ---------------+                     |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

CCP4SPG\_F\_ASUPUT 

 ,

ccp4spg\_f\_asuput 

 ,

(const int \*sindx, const int ihkl[3], int jhkl[3], int \*isym) 

 ,

(const int \*sindx, const int ihkl[3], int jhkl[3], int \*isym) 

 ,

(const int \*sindx, const int ihkl[3], int jhkl[3], int \*isym) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Put reflection in asymmetric unit of |
|                                      | spacegroup on index sindx.           |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | ---------------------------+         |
|                                      |     | *sindx*    | index of this spa |
|                                      | cegroup.                   |         |
|                                      |     +------------+------------------ |
|                                      | ---------------------------+         |
|                                      |     | *ihkl*     | input indices.    |
|                                      |                            |         |
|                                      |     +------------+------------------ |
|                                      | ---------------------------+         |
|                                      |     | *jhkl*     | output indices.   |
|                                      |                            |         |
|                                      |     +------------+------------------ |
|                                      | ---------------------------+         |
|                                      |     | *isym*     | symmetry operatio |
|                                      | n applied (ISYM number).   |         |
|                                      |     +------------+------------------ |
|                                      | ---------------------------+         |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

CCP4SPG\_F\_LOAD\_BY\_OPS 

 ,

ccp4spg\_f\_load\_by\_ops 

 ,

(const int \*sindx, int \*msym, float rrsym[192][4][4]) 

 ,

(const int \*sindx, int \*msym, float rrsym[192][4][4]) 

 ,

(const int \*sindx, int \*msym, float rrsym[192][4][4]) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Loads a spacegroup onto index        |
|                                      | "sindx". The spacegroup is           |
|                                      | identified by the set of symmetry    |
|                                      | matrices.                            |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | ---------------------+               |
|                                      |     | *sindx*    | index of this spa |
|                                      | cegroup.             |               |
|                                      |     +------------+------------------ |
|                                      | ---------------------+               |
|                                      |     | *msym*     | number of symmetr |
|                                      | y matrices passed.   |               |
|                                      |     +------------+------------------ |
|                                      | ---------------------+               |
|                                      |     | *rrsym*    | symmetry matrices |
|                                      | .                    |               |
|                                      |     +------------+------------------ |
|                                      | ---------------------+               |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

CCP4SPG\_F\_LOAD\_BY\_NAME 

 ,

ccp4spg\_f\_load\_by\_name 

 ,

(const int \*sindx, fpstr namspg, int namspg\_len) 

 ,

(const int \*sindx, fpstr namspg) 

 ,

(const int \*sindx, fpstr namspg, int namspg\_len) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Loads a spacegroup onto index        |
|                                      | "sindx". The spacegroup is           |
|                                      | identified by the spacegroup name.   |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *sindx*     | index of this sp |
|                                      | acegroup.   |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *namspg*    | spacegroup name. |
|                                      |             |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

ASUPHP 

 ,

asuphp 

 ,

(const int jhkl[3], const int \*lsym, const int \*isign, const float
\*phasin, float \*phasout) 

 ,

(const int jhkl[3], const int \*lsym, const int \*isign, const float
\*phasin, float \*phasout) 

 ,

(const int jhkl[3], const int \*lsym, const int \*isign, const float
\*phasin, float \*phasout) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Generate phase of symmetry           |
|                                      | equivalent JHKL from that of IHKL.   |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------+---------------- |
|                                      | -----------------------+             |
|                                      |     | *jhkl*       | indices hkl gen |
|                                      | erated in ASUPUT       |             |
|                                      |     +--------------+---------------- |
|                                      | -----------------------+             |
|                                      |     | *lsym*       | symmetry number |
|                                      |  for generating JHKL   |             |
|                                      |     +--------------+---------------- |
|                                      | -----------------------+             |
|                                      |     | *isign*      | 1 for I+ , -1 f |
|                                      | or I-                  |             |
|                                      |     +--------------+---------------- |
|                                      | -----------------------+             |
|                                      |     | *phasin*     | phase for refle |
|                                      | ction IHKL             |             |
|                                      |     +--------------+---------------- |
|                                      | -----------------------+             |
|                                      |     | *phasout*    | phase for refle |
|                                      | ction JHKL             |             |
|                                      |     +--------------+---------------- |
|                                      | -----------------------+             |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

ASUGET 

 ,

asuget 

 ,

(const int ihkl[3], int jhkl[3], const int \*isym) 

 ,

(const int ihkl[3], int jhkl[3], const int \*isym) 

 ,

(const int ihkl[3], int jhkl[3], const int \*isym) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Get the original indices jkhl from   |
|                                      | input indices ihkl generated under   |
|                                      | symmetry operation isym.             |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-----------+------------------- |
|                                      | --------------------------------+    |
|                                      |     | *ihkl*    | input indices.     |
|                                      |                                 |    |
|                                      |     +-----------+------------------- |
|                                      | --------------------------------+    |
|                                      |     | *jhkl*    | output indices (re |
|                                      | covered original indices).      |    |
|                                      |     +-----------+------------------- |
|                                      | --------------------------------+    |
|                                      |     | *isym*    | symmetry operation |
|                                      |  to be applied (ISYM number).   |    |
|                                      |     +-----------+------------------- |
|                                      | --------------------------------+    |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

ASUPUT 

 ,

asuput 

 ,

(const int ihkl[3], int jhkl[3], int \*isym) 

 ,

(const int ihkl[3], int jhkl[3], int \*isym) 

 ,

(const int ihkl[3], int jhkl[3], int \*isym) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Put reflection in asymmetric unit,   |
|                                      | as set up by ASUSET.                 |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-----------+------------------- |
|                                      | --------------------------+          |
|                                      |     | *ihkl*    | input indices.     |
|                                      |                           |          |
|                                      |     +-----------+------------------- |
|                                      | --------------------------+          |
|                                      |     | *jhkl*    | output indices.    |
|                                      |                           |          |
|                                      |     +-----------+------------------- |
|                                      | --------------------------+          |
|                                      |     | *isym*    | symmetry operation |
|                                      |  applied (ISYM number).   |          |
|                                      |     +-----------+------------------- |
|                                      | --------------------------+          |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

ASUSYM 

 ,

asusym 

 ,

(float rassym[384][4][4], float rinsym[384][4][4], int \*nisym) 

 ,

(float rassym[384][4][4], float rinsym[384][4][4], int \*nisym) 

 ,

(float rassym[384][4][4], float rinsym[384][4][4], int \*nisym) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Return symmetry operators and        |
|                                      | inverses, set up by ASUSET.          |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | -------------------------+           |
|                                      |     | *rassym*    | symmetry operato |
|                                      | rs.                      |           |
|                                      |     +-------------+----------------- |
|                                      | -------------------------+           |
|                                      |     | *rinsym*    | inverse symmetry |
|                                      |  operators.              |           |
|                                      |     +-------------+----------------- |
|                                      | -------------------------+           |
|                                      |     | *nisym*     | number of symmet |
|                                      | ry operators returned.   |           |
|                                      |     +-------------+----------------- |
|                                      | -------------------------+           |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

ASUSET 

 ,

asuset 

 ,

(fpstr spgnam, int \*numsgp, fpstr pgname, int \*msym, float
rrsym[192][4][4], int \*msymp, int \*mlaue, ftn\_logical \*lprint, int
spgnam\_len, int pgname\_len) 

 ,

(fpstr spgnam, int \*numsgp, fpstr pgname, int \*msym, float
rrsym[192][4][4], int \*msymp, int \*mlaue, ftn\_logical \*lprint) 

 ,

(fpstr spgnam, int spgnam\_len, int \*numsgp, fpstr pgname, int
pgname\_len, int \*msym, float rrsym[192][4][4], int \*msymp, int
\*mlaue, ftn\_logical \*lprint) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Set spacegroup for subsequent calls  |
|                                      | to ASUPUT, ASUGET, ASUSYM and        |
|                                      | ASUPHP.                              |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | --------------+                      |
|                                      |     | *spgnam*    | spacegroup name  |
|                                      |                                      |
|                                      |               |                      |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | --------------+                      |
|                                      |     | *numsgp*    | spacegroup numbe |
|                                      | r                                    |
|                                      |               |                      |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | --------------+                      |
|                                      |     | *pgname*    | On return, point |
|                                      |  group name                          |
|                                      |               |                      |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | --------------+                      |
|                                      |     | *msym*      | number of symmet |
|                                      | ry matrices passed.                  |
|                                      |               |                      |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | --------------+                      |
|                                      |     | *rrsym*     | symmetry matrice |
|                                      | s (preferred method of identifying s |
|                                      | pacegroup).   |                      |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | --------------+                      |
|                                      |     | *msymp*     | On return, numbe |
|                                      | r of primitive symmetry operators    |
|                                      |               |                      |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | --------------+                      |
|                                      |     | *mlaue*     | On return, numbe |
|                                      | r of Laue group.                     |
|                                      |               |                      |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | --------------+                      |
|                                      |     | *lprint*    | If true, print s |
|                                      | ymmetry information.                 |
|                                      |               |                      |
|                                      |     +-------------+----------------- |
|                                      | ------------------------------------ |
|                                      | --------------+                      |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

PATSGP 

 ,

patsgp 

 ,

(const fpstr spgnam, const fpstr pgname, fpstr patnam, int \*lpatsg, int
spgnam\_len, int pgname\_len, int patnam\_len) 

 ,

(const fpstr spgnam, const fpstr pgname, fpstr patnam, int \*lpatsg) 

 ,

(const fpstr spgnam, int spgnam\_len, const fpstr pgname, int
pgname\_len, fpstr patnam, int patnam\_len, int \*lpatsg) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Return the Patterson group name and  |
|                                      | number corresponding to a spacegroup |
|                                      | identified by spacegroup name and    |
|                                      | point group name.                    |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | --------------------------+          |
|                                      |     | *spgnam*    | On input, spaceg |
|                                      | roup name.                |          |
|                                      |     +-------------+----------------- |
|                                      | --------------------------+          |
|                                      |     | *pgname*    | On input, point  |
|                                      | group name.               |          |
|                                      |     +-------------+----------------- |
|                                      | --------------------------+          |
|                                      |     | *patnam*    | On return, Patte |
|                                      | rson spacegroup name.     |          |
|                                      |     +-------------+----------------- |
|                                      | --------------------------+          |
|                                      |     | *lpatsg*    | On return, Patte |
|                                      | rson spacegroup number.   |          |
|                                      |     +-------------+----------------- |
|                                      | --------------------------+          |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

HKLRANGE 

 ,

hklrange 

 ,

(int \*ihrng0, int \*ihrng1, int \*ikrng0, int \*ikrng1, int \*ilrng0,
int \*ilrng1) 

 ,

(int \*ihrng0, int \*ihrng1, int \*ikrng0, int \*ikrng1, int \*ilrng0,
int \*ilrng1) 

 ,

(int \*ihrng0, int \*ihrng1, int \*ikrng0, int \*ikrng1, int \*ilrng0,
int \*ilrng1) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Return ranges on H K L appropriate   |
|                                      | to spacegroup.                       |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *sindx*     | index of this sp |
|                                      | acegroup.   |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *nlaue*     | Laue number      |
|                                      |             |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *launam*    | Laue name        |
|                                      |             |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

CCP4SPG\_F\_GET\_LAUE 

 ,

ccp4spg\_f\_get\_laue 

 ,

(const int \*sindx, int \*nlaue, fpstr launam, int launam\_len) 

 ,

(const int \*sindx, int \*nlaue, fpstr launam) 

 ,

(const int \*sindx, int \*nlaue, fpstr launam, int launam\_len) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Return Laue number and name for a    |
|                                      | spacegroup onto index "sindx".       |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *sindx*     | index of this sp |
|                                      | acegroup.   |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *nlaue*     | Laue number      |
|                                      |             |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
|                                      |     | *launam*    | Laue name        |
|                                      |             |                        |
|                                      |     +-------------+----------------- |
|                                      | ------------+                        |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

PGNLAU 

 ,

pgnlau 

 ,

(const fpstr nampg, int \*nlaue, fpstr launam, int nampg\_len, int
launam\_len) 

 ,

(const fpstr nampg, int \*nlaue, fpstr launam) 

 ,

(const fpstr nampg, int nampg\_len, int \*nlaue, fpstr launam, int
launam\_len) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Return Laue number and name for      |
|                                      | current spacegroup.                  |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | -----------------------------------+ |
|                                      |     | *nampg*     | Point group name |
|                                      |  (unused in this implementation)   | |
|                                      |     +-------------+----------------- |
|                                      | -----------------------------------+ |
|                                      |     | *nlaue*     | Laue number      |
|                                      |                                    | |
|                                      |     +-------------+----------------- |
|                                      | -----------------------------------+ |
|                                      |     | *launam*    | Laue name        |
|                                      |                                    | |
|                                      |     +-------------+----------------- |
|                                      | -----------------------------------+ |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

SYMTR4 

 ,

symtr4 

 ,

(const int \*nsm, const float rsm[MAXSYM][4][4], fpstr symchs, int
symchs\_len) 

 ,

(const int \*nsm, const float rsm[MAXSYM][4][4], fpstr symchs) 

 ,

(const int \*nsm, const float rsm[MAXSYM][4][4], fpstr symchs, int
symchs\_len) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Fortran wrapper for mat4\_to\_symop. |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
|                                      |     | *nsm*       | number of symmet |
|                                      | ry matrices passed.   |              |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
|                                      |     | *rsm*       | symmetry matrice |
|                                      | s.                    |              |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
|                                      |     | *symchs*    | symmetry strings |
|                                      |  returned.            |              |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
+--------------------------------------+--------------------------------------+

FORTRAN\_SUBR

( 

SYMTR3 

 ,

symtr3 

 ,

(const int \*nsm, const float rsm[MAXSYM][4][4], fpstr symchs, const int
\*iprint, int symchs\_len) 

 ,

(const int \*nsm, const float rsm[MAXSYM][4][4], fpstr symchs, const int
\*iprint) 

 ,

(const int \*nsm, const float rsm[MAXSYM][4][4], fpstr symchs, int
symchs\_len, const int \*iprint) 

 

) 

+--------------------------------------+--------------------------------------+
|                                      | Fortran wrapper for mat4\_to\_symop. |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
|                                      |     | *nsm*       | number of symmet |
|                                      | ry matrices passed.   |              |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
|                                      |     | *rsm*       | symmetry matrice |
|                                      | s.                    |              |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
|                                      |     | *symchs*    | symmetry strings |
|                                      |  returned.            |              |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
|                                      |     | *iprint*    | print flag.      |
|                                      |                       |              |
|                                      |     +-------------+----------------- |
|                                      | ----------------------+              |
+--------------------------------------+--------------------------------------+
