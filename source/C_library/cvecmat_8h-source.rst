`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

cvecmat.h
=========

.. raw:: html

   <div class="fragment">

::

    00001 /*
    00002      cvecmat.h: header file for cvecmat.c
    00003      Copyright (C) 2001  CCLRC, Martyn Winn
    00004 
    00005      This code is distributed under the terms and conditions of the
    00006      CCP4 Program Suite Licence Agreement as a CCP4 Library.
    00007      A copy of the CCP4 licence can be obtained by writing to the
    00008      CCP4 Secretary, Daresbury Laboratory, Warrington WA4 4AD, UK.
    00009 */
    00010 #ifndef __CCP4_VECMAT
    00011 #define __CCP4_VECMAT
    00012 
    00013 #ifdef  __cplusplus
    00014 extern "C" {
    00015 #endif
    00016 static char rcsidhv[] = "$Id$";
    00017 
    00018 void ccp4_dcross(const double a[3], const double b[3], double c[3]);
    00019 void ccp4_3matmul(double c[3][3], const double a[3][3], const double b[3][3]);
    00020 void ccp4_4matmul( float c[4][4], const float  a[4][4], const float b[4][4]);
    00021 double invert3matrix(const double a[3][3], double ai[3][3]);
    00022 float invert4matrix(const float a[4][4], float ai[4][4]);
    00023 
    00024 float ccp4_pow_ii(const float base, const int power);
    00025 
    00026 #ifdef __cplusplus
    00027 }
    00028 #endif
    00029 
    00030 #endif  

.. raw:: html

   </div>
