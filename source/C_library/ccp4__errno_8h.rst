`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

ccp4\_errno.h File Reference
============================

| ``#include <errno.h>``

`Go to the source code of this file. <ccp4__errno_8h-source.html>`__

| 

Defines
-------

 #define 

**CCP4\_ERRSYSTEM**\ (x)   (((x)&0xfff)<<24)

 #define 

**CCP4\_ERRLEVEL**\ (x)   (((x)&0xf)<<16)

 #define 

**CCP4\_ERRSETLEVEL**\ (y, x)   ((y) & (~CCP4\_ERRLEVEL(0xf)) \|
CCP4\_ERRLEVEL(x)))

 #define 

**CCP4\_ERRGETSYS**\ (x)   (((x)>>24)&0xfff)

 #define 

**CCP4\_ERRGETLEVEL**\ (x)   (((x)>>16)&0xf)

 #define 

**CCP4\_ERRGETCODE**\ (x)   ((x)&0xffff)

 #define 

**CCP4\_ERR\_SYS**   CCP4\_ERRSYSTEM(0x0)

 #define 

**CCP4\_ERR\_FILE**   CCP4\_ERRSYSTEM(0x1)

 #define 

**CCP4\_ERR\_COORD**   CCP4\_ERRSYSTEM(0x2)

 #define 

**CCP4\_ERR\_MTZ**   CCP4\_ERRSYSTEM(0x3)

 #define 

**CCP4\_ERR\_MAP**   CCP4\_ERRSYSTEM(0x4)

 #define 

**CCP4\_ERR\_UTILS**   CCP4\_ERRSYSTEM(0x5)

 #define 

**CCP4\_ERR\_PARS**   CCP4\_ERRSYSTEM(0x6)

 #define 

**CCP4\_ERR\_SYM**   CCP4\_ERRSYSTEM(0x7)

 #define 

**CCP4\_ERR\_GEN**   CCP4\_ERRSYSTEM(0x8)

 #define 

**CCP4\_COUNT**\ (x)   sizeof(x)/sizeof(x[0])

| 

Functions
---------

void 

`ccp4\_error <ccp4__errno_8h.html#a18>`__ (const char \*)

const char \* 

`ccp4\_strerror <ccp4__errno_8h.html#a19>`__ (int)

void 

`ccp4\_fatal <ccp4__errno_8h.html#a20>`__ (const char \*)

int 

`ccp4\_liberr\_verbosity <ccp4__errno_8h.html#a21>`__ (int iverb)

void 

`ccp4\_signal <ccp4__errno_8h.html#a22>`__ (const int, const char
\*const, void(\*)())

 int 

**cfile\_perror** (const char \*)

| 

Variables
---------

int 

`ccp4\_errno <ccp4__errno_8h.html#a17>`__

--------------

Detailed Description
--------------------

Header file for error handling routines base error codes on system
errors.

--------------

Function Documentation
----------------------

+--------------------------------------------------------------------------+
| +--------------------+------+------------------+-----------+------+----+ |
| | void ccp4\_error   | (    | const char \*    |   *msg*   | )    |    | |
| +--------------------+------+------------------+-----------+------+----+ |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Print out passed message and         |
|                                      | internal message based upon          |
|                                      | ccp4\_errno "message : error message |
|                                      | "                                    |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------+---------------- |
|                                      | ---+                                 |
|                                      |     | *message*    | (const char \*) |
|                                      |    |                                 |
|                                      |     +--------------+---------------- |
|                                      | ---+                                 |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     void                             |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +--------------------+------+------------------+---------------+------+- |
| ---+                                                                     |
| | void ccp4\_fatal   | (    | const char \*    |   *message*   | )    |  |
|    |                                                                     |
| +--------------------+------+------------------+---------------+------+- |
| ---+                                                                     |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Wrapper for ccp4\_error which also   |
|                                      | calls exit(1)                        |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +--------------+---------------- |
|                                      | ---+                                 |
|                                      |     | *message*    | (const char \*) |
|                                      |    |                                 |
|                                      |     +--------------+---------------- |
|                                      | ---+                                 |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     void                             |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +-------------------------------+------+--------+-------------+------+-- |
| --+                                                                      |
| | int ccp4\_liberr\_verbosity   | (    | int    |   *iverb*   | )    |   |
|   |                                                                      |
| +-------------------------------+------+--------+-------------+------+-- |
| --+                                                                      |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Function to set verbosity level for  |
|                                      | messages from ccp4\_signal.          |
|                                      | Currently just off (0) and on (1).   |
|                                      | It should be generalised to be able  |
|                                      | to switch individual components on   |
|                                      | and off, i.e. replace 1 by a mask.   |
|                                      | cf. ccp4VerbosityLevel which sets    |
|                                      | the verbosity level for ccp4printf   |
|                                      | These are separate as they may be    |
|                                      | used differently.                    |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |     | *iverb*    | If >= 0 then set  |
|                                      | the verbosity level to the value of  |
|                                      | iverb. If < 0 (by convention -1) the |
|                                      | n report current level.   |          |
|                                      |     +------------+------------------ |
|                                      | ------------------------------------ |
|                                      | ------------------------------------ |
|                                      | --------------------------+          |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     current verbosity level          |
+--------------------------------------+--------------------------------------+

void ccp4\_signal

( 

const int 

  *code*,

const char \*const 

  *msg*,

void(\* 

  *callback*)()

) 

+--------------------------------------+--------------------------------------+
|                                      | Routine to set ccp4\_errno and print |
|                                      | out message for error tracing. This  |
|                                      | should be the only way in which      |
|                                      | ccp4\_errno is set. See error codes  |
|                                      | above for levels and systems. A      |
|                                      | callback with prototype void         |
|                                      | function(void) may also be passed to |
|                                      | the routine. Note: FATAL calls       |
|                                      | exit(1). If ccp4\_liberr\_verbosity  |
|                                      | returns 0, then ccp4\_signal sets    |
|                                      | ccp4\_errno and returns without      |
|                                      | doing anything else.                 |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +---------------+--------------- |
|                                      | ------------------------+            |
|                                      |     | *error*       | code (int)     |
|                                      |                         |            |
|                                      |     +---------------+--------------- |
|                                      | ------------------------+            |
|                                      |     | *message*     | (const char \* |
|                                      |  const)                 |            |
|                                      |     +---------------+--------------- |
|                                      | ------------------------+            |
|                                      |     | *callback*    | (point to rout |
|                                      | ine void (\*)(void) )   |            |
|                                      |     +---------------+--------------- |
|                                      | ------------------------+            |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     void                             |
+--------------------------------------+--------------------------------------+

+--------------------------------------------------------------------------+
| +-------------------------------+------+--------+-------------+------+-- |
| --+                                                                      |
| | const char\* ccp4\_strerror   | (    | int    |   *error*   | )    |   |
|   |                                                                      |
| +-------------------------------+------+--------+-------------+------+-- |
| --+                                                                      |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | Obtain character string based upon   |
|                                      | error code. Typical use              |
|                                      | ccp4\_strerror(ccp4\_errno) The      |
|                                      | returned string is statically        |
|                                      | allocated in the                     |
|                                      | `library\_err.c <library__err_8c.htm |
|                                      | l>`__                                |
|                                      | file and should not be freed.        |
|                                      |                                      |
|                                      |  **Parameters:**                     |
|                                      |     +------------+--------------+    |
|                                      |     | *error*    | code (int)   |    |
|                                      |     +------------+--------------+    |
|                                      |                                      |
|                                      |  **Returns:**                        |
|                                      |     const pointer to error message   |
|                                      |     (const char \*)                  |
+--------------------------------------+--------------------------------------+

--------------

Variable Documentation
----------------------

+--------------------------------------------------------------------------+
| +-------------------+                                                    |
| | int ccp4\_errno   |                                                    |
| +-------------------+                                                    |
+--------------------------------------------------------------------------+

+--------------------------------------+--------------------------------------+
|                                      | @global ccp4\_errno: global variable |
|                                      | that stores the error last error     |
|                                      | code from the ccp4 libraries \| 12   |
|                                      | bits - library \| 4 bits - level \|  |
|                                      | 16 bits - code \|                    |
|                                      |                                      |
|                                      | associated macros CCP4\_ERR\_SYS 0   |
|                                      | OS error CCP4\_ERR\_FILE 1 io        |
|                                      | library CCP4\_ERR\_COORD 2 mmdb      |
|                                      | CCP4\_ERR\_MTZ 3 cmtz CCP4\_ERR\_MAP |
|                                      | 4 map io CCP4\_ERR\_UTILS 5 utility  |
|                                      | routines CCP4\_ERR\_PARS 6 parser    |
|                                      | routines CCP4\_ERR\_SYM 7 csymlib    |
|                                      |                                      |
|                                      | and bit manipulation CCP4\_ERRSYSTEM |
|                                      | system mask for setting              |
|                                      | CCP4\_ERRLEVEL error level mask      |
|                                      | CCP4\_ERRSETLEVEL error level mask   |
|                                      | for setting error level              |
|                                      | CCP4\_ERRGETSYS mask for returning   |
|                                      | system CCP4\_ERRGETLEVEL mask for    |
|                                      | returning level CCP4\_ERRGETCODE     |
|                                      | mask for returning the code          |
|                                      |                                      |
|                                      | error levels 0 Success 1             |
|                                      | Informational 2 Warning 3 Error 4    |
|                                      | Fatal                                |
+--------------------------------------+--------------------------------------+
