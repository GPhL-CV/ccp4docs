`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

ccp4\_fortran.h
===============

`Go to the documentation of this file. <ccp4__fortran_8h.html>`__

.. raw:: html

   <div class="fragment">

::

    00001 /*
    00002      ccp4_fortran.h: header file for Fortran APIs
    00003      Copyright (C) 2001   Eugene Krissinel
    00004 
    00005      This code is distributed under the terms and conditions of the
    00006      CCP4 Program Suite Licence Agreement as a CCP4 Library.
    00007      A copy of the CCP4 licence can be obtained by writing to the
    00008      CCP4 Secretary, Daresbury Laboratory, Warrington WA4 4AD, UK.
    00009 */
    00010 
    00016 #ifndef __CCP4_FORTRAN
    00017 #define __CCP4_FORTRAN
    00018 
    00019 #include "ccp4_types.h"
    00020 static char rcsidhh[] = "$Id$";
    00021 
    00022 /* stardent is now obsolete, but we retain this category in case it is useful later */
    00023 #ifdef CALL_LIKE_STARDENT
    00024   /* SStrParam is used in Ardent-like machines' fortran calls */
    00025   /* for passing a string parameter */
    00026   DefineStructure(SStrPar)
    00027   struct SStrPar  {
    00028     pstr S;
    00029     int  len;
    00030     int  id;
    00031   };
    00032 #endif
    00033 
    00034 #define _LVTOB(l) ((long) ((l) == 0 ? 0 : 1))  
    00035 #define _BTOLV(l) ((int) ((l) == 0 ? 0 : 1))
    00036 #if defined (__OSF1__) || defined (__osf__)
    00037 #undef _BTOLV
    00038 #define _BTOLV(l) ((int) ((l) == 0 ? 0 : -1))  
    00039 #endif    
    00040 
    00041 /*
    00042      Macro  FORTRAN_SUBR(NAME,name,p_send,p_sstruct,p_sflw)
    00043    makes function header statements that allow for linking with
    00044    programs written in FORTRAN.
    00045   
    00046      Parameters:
    00047   
    00048      NAME      name of the FORTRAN subroutine in capital letters
    00049      name      name of the FORTRAN subroutine in small letters
    00050      p_send    parameter list (in brackets) with string lengths
    00051                attached to the end of it (see below)
    00052      p_sstruct parameter list (in brackets) with strings passed
    00053                as complex parameters, or structures
    00054      p_sflw    parameter list (in brackets) with string lengths
    00055                following immediately the string parameters
    00056                (see below)
    00057   
    00058      All non-string parameters must be passed as pointers, in
    00059    the same order as they enter the FORTRAN call. Rules for
    00060    the string parameters are as follows.
    00061   
    00062      1. All strings should be specified as of 'fpstr' type.
    00063         The 'fpstr' type is defined below and depends on the
    00064         platform:
    00065   
    00066           a) whenever length of string is passed as a separate
    00067              parameter ( CALL_LIKE_SUN, CALL_LIKE_HPUX,
    00068              CALL_LIKE_MVS )  'fpstr' is identical to 'pstr'.
    00069              You may choose arbitrary name for the string,
    00070              but you MUST use the same name, appended with
    00071              suffix '_len', for its length (see example below).
    00072   
    00073           b) whenever string and its length are passed as
    00074              complex parameter, 'fpstr' is identical to the
    00075              pointer on the corresponding structure:
    00076                CALL_LIKE_STARDENT :
    00077                    'fpstr' is identical to 'PSStrPar'
    00078                CALL_LIKE_VMS      :
    00079                    'fpstr' is identical to 'dsc$descriptor_s *'
    00080   
    00081         With 'fpstr' type, two important macro definition come:
    00082   
    00083           i)  FTN_STR(s)  - returns pointer to fortran-passed
    00084                             string s. This pointer is always
    00085                             of 'pstr' type
    00086           ii) FTN_LEN(s)  - returns integer length of fortran-
    00087                             passed string s. For this macro to
    00088                             work properly with SUN- and MVS-like
    00089                             machines, always use suffix '_len' 
    00090                             for the string length parameters as
    00091                             described in a) above.
    00092   
    00093      2. Three parameter lists, each enclosed in brackets, should
    00094         be given. These lists retain the general order of
    00095         parameters in the corresponding fortran call. Non-string
    00096         parameters are passed as pointers. String parameters
    00097         and their lengths are passed differently in different
    00098         lists:
    00099   
    00100          p_send    strings enter their place in the list as in
    00101                    the corresponding FORTRAN call, having 'fpstr'
    00102                    parameter type. Their lengths are appended as
    00103                    'int' to the end of the list. They should
    00104                    retain the order in which the strings appear
    00105                    in the list.
    00106   
    00107          p_sstruct strings enter their place in the list as in
    00108                    the corresponding FORTRAN call, having 'fpstr'
    00109                    parameter type.
    00110   
    00111          p_sflw    strings enter their place in the list as in
    00112                    the corresponding FORTRAN call, having 'fpstr'
    00113                    type and being immediately followed by their
    00114                    lengths as 'int' parameters.
    00115   
    00116   
    00117   
    00118    Example:
    00119   
    00120      FORTRAN statement
    00121   
    00122        subroutine  SomeSub ( k,s1,a,s2,m )
    00123        integer       k,m
    00124        real          a
    00125        character*(*) s1,s2
    00126   
    00127      is translated to
    00128   
    00129        FORTRAN_SUBR ( SOMESUB, somesub,
    00130          ( int * k, fpstr s1, float * a, fpstr s2, int * m,
    00131            int s1_len, int s2_len ),
    00132          ( int * k, fpstr s1, float * a, fpstr s2, int * m ),
    00133          ( int * k, fpstr s1, int s1_len, float * a,
    00134            fpstr s2, int s2_len, int * m ) )
    00135   
    00136   
    00137      The macro should replace ordinary function header
    00138    statements to assure compatibility with FORTRAN links.
    00139    In header files, do not forget to add semicolumn:
    00140   
    00141      FORTRAN_SUBR ( .... );
    00142   
    00143    while in source files use simply
    00144   
    00145      FORTRAN_SUBR ( .... )  {
    00146       <source body, operators>
    00147      }
    00148   
    00149   
    00150   
    00151      Macro  FORTRAN_CALL(NAME,name,p_send,p_sstruct,p_sflw)
    00152    calls function defined with macro FORTRAN_SUBR(...), from
    00153    a C/C++ application. Its parameters and their meaning are
    00154    exactly identical to those of FORTRAN_SUBR(...).
    00155    FORTRAN_CALL(...) should be followed by semicolon.                    */
    00156 
    00157 
    00158 #if  defined(CALL_LIKE_SUN)
    00159 
    00160   typedef pstr fpstr;
    00161 
    00162 #define FTN_STR(s)  s
    00163 #define FTN_LEN(s)  s##_len
    00164 
    00165 #define char_struct(s)           \
    00166     pstr  s;                       \
    00167     int   s##_len;
    00168 #define fill_char_struct(s,str)  \
    00169     s  = str;                      \
    00170     s##_len = strlen(str);
    00171 #define init_char_struct(s,str,size)  \
    00172     s  = str;                      \
    00173     s##_len = size;
    00174 
    00175 #define FORTRAN_SUBR(NAME,name,p_sun,p_stardent,p_mvs) \
    00176     void name##_ p_sun
    00177 #define FORTRAN_CALL(NAME,name,p_sun,p_stardent,p_mvs) \
    00178     name##_ p_sun
    00179 #define FORTRAN_FUN(val,NAME,name,p_sun,p_stardent,p_mvs) \
    00180     val name##_ p_sun
    00181 #elif defined(CALL_LIKE_HPUX)
    00182 
    00183   typedef pstr fpstr;
    00184 
    00185 #  define FTN_STR(s)  s
    00186 #  define FTN_LEN(s)  s##_len
    00187 
    00188 #  define char_struct(s)  \
    00189     pstr  s;              \
    00190     int   s##_len;
    00191 #  define fill_char_struct(s,str)  \
    00192     s  = str;                      \
    00193     s##_len = strlen(str);
    00194 #  define init_char_struct(s,str,size)  \
    00195     s  = str;                      \
    00196     s##_len = size;
    00197 
    00198 #  define FORTRAN_SUBR(NAME,name,p_sun,p_stardent,p_mvs) \
    00199     void name p_sun
    00200 #  define FORTRAN_CALL(NAME,name,p_sun,p_stardent,p_mvs) \
    00201     name p_sun
    00202 #  define FORTRAN_FUN(val,NAME,name,p_sun,p_stardent,p_mvs) \
    00203     val name p_sun
    00204 #elif defined(CALL_LIKE_STARDENT)
    00205 
    00206   typedef PStrPar fpstr;
    00207 
    00208 #  define FTN_STR(s)  s->Str_pointer
    00209 #  define FTN_LEN(s)  s->Str_length
    00210 
    00211 #  define char_struct(s)           \
    00212     SStrPar s;
    00213 #  define fill_char_struct(s,str)  \
    00214     s.S   = str;                   \
    00215     s.len = strlen(FName);         \
    00216     s.id  = 0;
    00217 #  define init_char_struct(s,str,size)  \
    00218     s.S   = str;                   \
    00219     s.len = size;         \
    00220     s.id  = 0;
    00221 
    00222 #  define FORTRAN_SUBR(NAME,name,p_send,p_sstruct,p_sflw) \
    00223     void NAME p_stardent
    00224 #  define FORTRAN_CALL(NAME,name,p_send,p_sstruct,p_sflw) \
    00225     NAME p_stardent
    00226 #  define FORTRAN_FUN(val,NAME,name,p_send,p_sstruct,p_sflw) \
    00227     val NAME p_stardent
    00228 
    00229 #elif defined(CALL_LIKE_VMS)
    00230 
    00231   typedef dsc$descriptor_s * fpstr;
    00232 
    00233 #  define FTN_STR(s)  s->dsc$a_pointer;
    00234 #  define FTN_LEN(s)  s->dsc$w_length;
    00235 
    00236 #  define char_struct(s)                \
    00237     dsc$descriptor_s s;
    00238 #  define fill_char_struct(s,str)     \
    00239     s.dsc$a_pointer = str;            \
    00240     s.dsc$w_length  = strlen(str);    \
    00241     s.dsc$b_dtype   = DSC$K_DTYPE_T;  \
    00242     s.dsc$b_class   = DSC$K_CLASS_S;
    00243 #  define init_char_struct(s,str,size)     \
    00244     s.dsc$a_pointer = str;            \
    00245     s.dsc$w_length  = size;    \
    00246     s.dsc$b_dtype   = DSC$K_DTYPE_T;  \
    00247     s.dsc$b_class   = DSC$K_CLASS_S;
    00248 
    00249 #  define FORTRAN_SUBR(NAME,name,p_sun,p_stardent,p_mvs) \
    00250     void NAME p_stardent
    00251 #  define FORTRAN_CALL(NAME,name,p_sun,p_stardent,p_mvs) \
    00252     NAME p_stardent
    00253 #  define FORTRAN_FUN(val,NAME,name,p_sun,p_stardent,p_mvs) \
    00254     val NAME p_stardent
    00255 
    00256 #elif defined(CALL_LIKE_MVS)
    00257 
    00258 #if (CALL_LIKE_MVS == 2)
    00259 
    00260   typedef pstr fpstr;
    00261 
    00262 #define FTN_STR(s)  s
    00263 #define FTN_LEN(s)  s##_len
    00264 
    00265 #define char_struct(s)           \
    00266     pstr  s;                       \
    00267     int   s##_len;
    00268 #define fill_char_struct(s,str)  \
    00269     s  = str;          \
    00270         s##_len = strlen(str);
    00271 #define init_char_struct(s,str,size)  \
    00272     s  = str;                   \
    00273         s##_len = size;
    00274 
    00275 #define FORTRAN_SUBR(NAME,name,p_sun,p_stardent,p_mvs) \
    00276     void NAME p_sun
    00277 #define FORTRAN_CALL(NAME,name,p_sun,p_stardent,p_mvs) \
    00278     NAME p_sun
    00279 #define FORTRAN_FUN(val,NAME,name,p_sun,p_stardent,p_mvs) \
    00280     val NAME p_sun
    00281 
    00282 #else
    00283 
    00284   typedef pstr fpstr;
    00285 
    00286 #  define FTN_STR(s)  s
    00287 #  define FTN_LEN(s)  s##_len
    00288 
    00289 #  define char_struct(s)  \
    00290     pstr  s;              \
    00291     int   s##_len;
    00292 #  define fill_char_struct(s,str)  \
    00293     s  = str;                      \
    00294     s##_len = strlen(str);
    00295 #  define init_char_struct(s,str,size)  \
    00296     s  = str;                      \
    00297     s##_len = size;
    00298 
    00299 #  define FORTRAN_SUBR(NAME,name,p_sun,p_stardent,p_mvs) \
    00300     void __stdcall NAME p_mvs
    00301 #  define FORTRAN_CALL(NAME,name,p_sun,p_stardent,p_mvs) \
    00302     NAME p_mvs
    00303 #  define FORTRAN_FUN(val,NAME,name,p_sun,p_stardent,p_mvs) \
    00304     val __stdcall NAME p_mvs
    00305 
    00306 # endif
    00307 
    00308 #else
    00309 
    00310 #  error  Unknown machine!!!
    00311 
    00312   typedef pstr fpstr;
    00313 
    00314 #  define FTN_STR(s)  s
    00315 #  define FTN_LEN(s)  s##_len
    00316 
    00317 #  define char_struct(s)  \
    00318     pstr  s;              \
    00319     int   s##_len;
    00320 #  define fill_char_struct(s,str)  \
    00321     s  = str;                      \
    00322     s##_len = strlen(str);
    00323 #  define init_char_struct(s,str,size)  \
    00324     s  = str;                      \
    00325     s##_len = size;
    00326 
    00335 #  define FORTRAN_SUBR(NAME,name,p_sun,p_stardent,p_mvs) \
    00336     void name##_ p_sun
    00337 
    00345 #  define FORTRAN_CALL(NAME,name,p_sun,p_stardent,p_mvs) \
    00346     name##_ p_sun
    00347 
    00357 #  define FORTRAN_FUN(val,NAME,name,p_sun,p_stardent,p_mvs) \
    00358     val name##_ p_sun
    00359 
    00360 #endif
    00361 
    00362 /* Define Fortran logical */
    00363 typedef unsigned int ftn_logical;
    00364 #if defined (KNOWN_MACHINE)
    00365 #  define FORTRAN_LOGICAL_TRUE  1
    00366 #  define FORTRAN_LOGICAL_FALSE 0
    00367 #endif
    00368 #if defined (__OSF1__) || defined (__osf__)
    00369 #  undef FORTRAN_LOGICAL_TRUE
    00370 #  define FORTRAN_LOGICAL_TRUE  -1
    00371 #endif     
    00372 
    00373 char *ccp4_FtoCString(fpstr str1, int str1_len);
    00374 void ccp4_CtoFString(fpstr str1, int str1_len, const char *cstring);
    00375 
    00376 #endif    /* __CCP4_FORTRAN */

.. raw:: html

   </div>
