`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

ccp4\_general.h
===============

.. raw:: html

   <div class="fragment">

::

    00001 /*
    00002      ccp4_general.h: header for general library functions and utilities.
    00003      Copyright (C) 2001  CCLRC, Peter Briggs et al
    00004 
    00005      This code is distributed under the terms and conditions of the
    00006      CCP4 Program Suite Licence Agreement as a CCP4 Library.
    00007      A copy of the CCP4 licence can be obtained by writing to the
    00008      CCP4 Secretary, Daresbury Laboratory, Warrington WA4 4AD, UK.
    00009 */
    00010 
    00011 /*   ccp4_general.c
    00012 
    00013      Header file for CCP4 library clones
    00014      Peter Briggs et al CCP4 April 2001
    00015 */
    00016 
    00017 /*------------------------------------------------------------------*/
    00018 
    00019 /* Macro definitions */
    00020 
    00021 /*------------------------------------------------------------------*/
    00022 
    00023 #ifndef __CCPGeneral__
    00024 #define __CCPGeneral__
    00025 
    00026 static char rcsidhl[] = "$Id$";
    00027 
    00028 /* note order: this must be outside CCP4 namespace */
    00029 #include "ccp4_parser.h"
    00030 
    00031 #ifdef  __cplusplus
    00032 namespace CCP4 {
    00033 extern "C" {
    00034 #endif
    00035 
    00036 /* MAXLINE = maximum number of characters in lines read
    00037    from environ.def and default.def files (ccp4fyp)
    00038    MAXTOKS = maximum number of tokens in lines read from
    00039    environ.def and default.def files (ccp4fyp)
    00040    MAXNAMES = maximum number of logical names that can be
    00041    read and stored from environ.def (ccp4fyp)
    00042 */
    00043 #define CCP4_MAXLINE  200
    00044 #define CCP4_MAXTOKS  3
    00045 #define CCP4_MAXNAMES 150
    00046 #define CCP4_MODULO   100000
    00047 
    00048 /* stuff for error reporting */
    00049 #define CGEN_ERRNO(n) (CCP4_ERR_GEN | (n))
    00050 
    00051 /* error defs */
    00052 #define  CGENERR_Ok                  0
    00053 #define  CGENERR_AllocFail           1
    00054 #define  CGENERR_CantSetEnvironment  2
    00055 #define  CGENERR_MaxNamesExceeded    3
    00056 #define  CGENERR_EOptionUseError     4
    00057 #define  CGENERR_DOptionUseError     5
    00058 #define  CGENERR_LogicalNameUseError 6
    00059 #define  CGENERR_CantOpenEnvFile     7
    00060 #define  CGENERR_CantOpenDefFile     8
    00061 #define  CGENERR_ParseEnvFail        9
    00062 #define  CGENERR_ParseDefFail        10
    00063 #define  CGENERR_CantFindInFile      11
    00064 #define  CGENERR_EnvPathFail         12
    00065 #define  CGENERR_DefPathFail         13
    00066 #define  CGENERR_CantGetClibd        14
    00067 #define  CGENERR_CantGetCcp4Scr      15
    00068 
    00069 /*------------------------------------------------------------------*/
    00070 
    00071 /* Structures and typedefs */
    00072 
    00073 /*------------------------------------------------------------------*/
    00074 
    00075 /* <None declared> */
    00076 
    00077 /*------------------------------------------------------------------*/
    00078 
    00079 /* Function Prototypes */
    00080 
    00081 /*------------------------------------------------------------------*/
    00082 
    00083 void ccp4f_mem_tidy(void);
    00084 
    00085 int ccperror(int ierr, const char *message);
    00086 
    00087 int ccperror_noexit(int ierr, const char *message);
    00088 
    00089 int ccp4printf(int level, char *format, ...);
    00090 
    00091 int ccp4fyp(int argc, char **argv);
    00092 
    00093 int ccp4fyp_cleanup(int ienv, char **envname, char **envtype, char **envext,
    00094                     char *logical_name, char *file_name, char *file_type,
    00095                     char *file_ext, char *env_file, char *def_file,
    00096                     char *dir, CCP4PARSERARRAY *parser);
    00097 
    00098 int ccp4setenv(char *logical_name, char* value, char **envname,
    00099                char **envtype, char **envext, int *ienv, int no_overwrt);
    00100 
    00101 int ccp4setenv_cleanup(char *file_ext, char *file_root, char *file_path,
    00102                        char *file_name);
    00103 
    00104 int ccpexists(char *filename);
    00105 
    00106 int ccpputenv(char *logical_name, char *file_name);
    00107 
    00108 void ccp4_banner(void);
    00109 
    00110 #ifdef __cplusplus
    00111 }
    00112 }
    00113 #endif
    00114 
    00115 #endif   /* __CCPGeneral__ */

.. raw:: html

   </div>
