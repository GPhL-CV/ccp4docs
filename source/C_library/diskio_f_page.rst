`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

Fortran API for low level input/output.
---------------------------------------

File list
---------

-  `ccp4\_diskio\_f.c <ccp4__diskio__f_8c.html>`__

Overview
--------

This library consists of a set of wrappers to C functions which perform
random access input/output (I/O) on various data items, including bytes,
to stream-mode files.
