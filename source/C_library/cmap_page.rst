`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

CMAP library
------------

.. raw:: html

   <div class="fragment">

::




       

.. raw:: html

   </div>

File list
---------

-  `cmaplib.h <cmaplib_8h.html>`__ - contains details of the C/C++ API
-  cmap\_data.h
-  cmap\_header.h
-  cmap\_skew.h
-  cmap\_errno.h
-  cmap\_labels.h
-  cmap\_stats.h

Overview
--------

Functions defining the C-level API for accessing CCP4 map files.
