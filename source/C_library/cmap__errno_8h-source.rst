`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

cmap\_errno.h
=============

.. raw:: html

   <div class="fragment">

::

    00001 /*
    00002      cmap_errno.h: error codes for map handling functions
    00003      Copyright (C) 2001  CCLRC, Charles Ballard
    00004 
    00005      This code is distributed under the terms and conditions of the
    00006      CCP4 Program Suite Licence Agreement as a CCP4 Library.
    00007      A copy of the CCP4 licence can be obtained by writing to the
    00008      CCP4 Secretary, Daresbury Laboratory, Warrington WA4 4AD, UK.
    00009 */
    00010 #ifndef __GUARD_MAPLIB_ERR
    00011 #define __GUARD_MAPLIB_ERR
    00012 
    00013 #include "ccp4_errno.h"
    00014 
    00015 #ifdef __cplusplus
    00016 extern "C" {
    00017 #endif
    00018 
    00019 #define CMAP_ERRNO(n) (CCP4_ERR_MAP | (n))
    00020 
    00021 /* error defs */
    00022 #define  CMERR_Ok                  0
    00023 #define  CMERR_NoChannel           1
    00024 #define  CMERR_NoFile              2
    00025 #define  CMERR_NoLogicalName       3
    00026 #define  CMERR_CantOpenFile        4
    00027 #define  CMERR_NoHeader            5
    00028 #define  CMERR_ReadFail            6
    00029 #define  CMERR_WriteFail           7
    00030 #define  CMERR_ParamError          8
    00031 #define  CMERR_UnrecognK           9
    00032 #define  CMERR_FileStamp           10
    00033 #define  CMERR_SymErr              11
    00034 #define  CMERR_AllocFail           12
    00035 #define  CMERR_MaxFile             13
    00036 #define  CMERR_SeekFail            14
    00037 
    00038 #ifdef __cplusplus
    00039 }
    00040 #endif
    00041 
    00042 #endif     /* __GUARD_MAPLIB_ERR */
    00043 

.. raw:: html

   </div>
