`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

cmap\_header.h
==============

.. raw:: html

   <div class="fragment">

::

    00001 /*
    00002      cmap_header.h: header file for cmap_header.c
    00003      Copyright (C) 2001  CCLRC, Charles Ballard
    00004 
    00005      This code is distributed under the terms and conditions of the
    00006      CCP4 Program Suite Licence Agreement as a CCP4 Library.
    00007      A copy of the CCP4 licence can be obtained by writing to the
    00008      CCP4 Secretary, Daresbury Laboratory, Warrington WA4 4AD, UK.
    00009 */
    00010 #ifndef __GUARD_MAPLIB_HEADER
    00011 #define __GUARD_MAPLIB_HEADER
    00012 
    00013 #ifdef __cplusplus
    00014 extern "C" {
    00015 #endif
    00016 
    00017 int parse_mapheader(CMMFile *mfile);
    00018 
    00019 int write_mapheader(CMMFile *mfile);
    00020 
    00021 #ifdef __cplusplus
    00022 }
    00023 #endif
    00024 
    00025 #endif  /* __GUARD_MAPLIB_HEADER */

.. raw:: html

   </div>
