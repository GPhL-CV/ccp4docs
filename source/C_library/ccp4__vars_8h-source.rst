`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

ccp4\_vars.h
============

.. raw:: html

   <div class="fragment">

::

    00001 /*
    00002      ccp4_vars.h: Standard strings for certain quantites
    00003      Copyright (C) 2002  CCLRC, Martyn Winn
    00004 
    00005      This code is distributed under the terms and conditions of the
    00006      CCP4 Program Suite Licence Agreement as a CCP4 Library.
    00007      A copy of the CCP4 licence can be obtained by writing to the
    00008      CCP4 Secretary, Daresbury Laboratory, Warrington WA4 4AD, UK.
    00009 */
    00010 /*
    00011 */
    00012 
    00013 /* Author: Martyn Winn */
    00014 
    00015 /* Standard strings for certain quantites - for future use */
    00016 
    00017 #ifndef __CCP4_VARS__
    00018 #define __CCP4_VARS__
    00019 
    00020 #define MTZFILENAME "data::mtzfile::filename"
    00021 #define MTZTITLE "data::mtzfile::title"
    00022 #define MTZSPACEGROUP "data::mtzfile::spacegroup_num"        
    00023 #define MTZNUMREFLS "data::mtzfile::num_reflections"
    00024 #define MTZMNF "data::mtzfile::missing_number_flag"
    00025 #define MTZSORTORDER "data::mtzfile::sort_order"          
    00026 
    00027 #define CRYSTALXTALNAME "data::crystal::crystal_name"
    00028 #define CRYSTALPNAME "data::crystal::project_name"
    00029 #define CRYSTALCELL "data::crystal::cell"
    00030 
    00031 #define DATASETDNAME "data::crystal::dataset::dataset_name"
    00032 #define DATASETWAVELENGTH "data::crystal::dataset::wavelength"
    00033 
    00034 #define COLUMNLABEL "data::crystal_i::dataset_i::column_i::label"
    00035 #define COLUMNTYPE "data::crystal_i::dataset_i::column_i::type"
    00036 
    00037 #endif  

.. raw:: html

   </div>
