`Main Page <index.html>`__   `Compound List <annotated.html>`__   `File
List <files.html>`__   `Compound Members <functions.html>`__   `File
Members <globals.html>`__   `Related Pages <pages.html>`__  

--------------

CParser library
---------------

.. raw:: html

   <div class="fragment">

::




       

.. raw:: html

   </div>

File list
---------

-  `ccp4\_parser.h <ccp4__parser_8h.html>`__ - contains details of the
   C/C++ API

Overview
--------

These functions do CCP4-style parsing, as used for processing keywords
of CCP4 programs, `MTZ <structMTZ.html>`__ header records, etc.

Usage
-----

The following code snippets illustrate how the functions might be used
to read from stdin:

::


    int           ntok=0;
    char          line[201],*key;
    CCP4PARSERTOKEN * token=NULL;
    CCP4PARSERARRAY * parser;

      parser = (CCP4PARSERARRAY *) ccp4_parse_start(20);
      key   = parser->keyword;
      token = parser->token;

      RC   = 0;
      while (!RC) {

        line[0] = '\0';
        ntok = ccp4_parser(line,200,parser,1);

        if (ntok < 1) {

          RC = 111;

        } else {      

          if (ccp4_keymatch("MINDIST",key))  {
        if (ntok != 2) {
          ccperror ( 1,"MINDIST requires a single numerical argument" );
          RC = -100;
        } else {
          minDist = token[1].value;
            }
          } else  {
        printf ( "Unrecognised keyword \"s\"

    ",token[0].fullstring );
        RC = -118;
          }
        }
      }

      ccp4_parse_end ( parser );

Examples
--------

See the distributed programs `NCONT <../ncont.html>`__ and
`PDBCUR <../pdbcur.html>`__.
