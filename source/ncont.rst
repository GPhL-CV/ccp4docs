NCONT (CCP4: Supported Program)
===============================

NAME
----

**ncont** - analyses contacts between subsets of atoms in a PDB file.

SYNOPSIS
--------

| **ncont xyzin** *foo\_in.pdb*
| [Key-worded input file]

DESCRIPTION
-----------

``ncont`` analyses contacts between subsets of atoms in a PDB file. The
program is written using the new MMDB library for coordinate data, and
thus works with a hierarchical view of the atomic model. This hierarchy
is visible for example in the `atom selection
syntax <#atom_selection>`__ used.

INPUT AND OUTPUT FILES
----------------------

XYZIN
~~~~~

Input coordinate file.

KEYWORDED INPUT
---------------

source {selection of atoms}
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Selection of source atom set.

target {selection of atoms}
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Selection of target atom set.

mindist <mind>
~~~~~~~~~~~~~~

Minimal contact distance (in A), default 0.0

maxdist <maxd>
~~~~~~~~~~~~~~

Maximal contact distance (in A), default 4.0

seqdist <seqd>
~~~~~~~~~~~~~~

Minimal "sequence distance", or difference in indices (\_not\_ in
sequence numbers) of residues between those containing the contacting
atoms. The default value is 1, which means that contacting atoms may be
found in the neighbouring and further residues, but not in the same
residue).

sort [off\|source\|target\|distance] [inc\|dec]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

| ('off' and 'inc' default).
| This parameter specifies the subject and order for sorting the
  contacts on output. For example, 'sort source dec' means sorting by
  decreasing index of the source atoms (index is essentially the atom's
  serial number in a correct PDB file). Subject 'off' (default) means no
  sorting.

cells [off \| 0 \| 1 \| 2 \| symm \| inter]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

| ('off' default).
| If cells is set to 'off', the program seeks contacts only between the
  atoms found in the coordinate file. Other values induce construction
  of unit cell(s) and looking for contacts also between source atoms and
  target atoms generated in the cell(s).

0
    generates only one (primary) unit cell, in which the source and
    target atoms are originally found
1
    generates the primary cell +/- 1 cell in all directions (27 cells in
    total)
2
    generates the primary cell +/- 2 cells in all directions (125 cells
    in total)
symm
    same as 2 except that only symmetry-generated target atoms are
    included, i.e. contacts to the target atoms in the input file are
    excluded
inter
    same as 2 except intra-chain contacts are excluded from the contact
    listing, though they remain in the overall statistics. Here
    "intra-chain" means target atoms for which no symmetry operation or
    unit cell translation has been applied, and which have the same
    chain ID as the source atom.

symmetry <spgname>
~~~~~~~~~~~~~~~~~~

Input of the space group symmetry name, e.g. 'P 21', 'P 1 21 1' or
'P21'. The program should accept a variety of naming conventions, but it
does not currently accept spacegroup number. This parameter is mandatory
if coordinate file does not specify the space group symmetry on the
CRYST1 card.

geometry <a> <b> <c> <alpha> <beta> <gamma>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Input of the unit cell dimensions (space-separated real numbers). This
parameter is mandatory if coordinate file does not specify the cell
parameters.

ATOM SELECTION SYNTAX
---------------------

See the description in the `pdbcur <pdbcur.html#atom_selection>`__
documentation.

PROGRAM OUTPUT
--------------

The program currently gives a short summary of the commands received,
and a list of the contacts found.

EXAMPLES
--------

Runnable example
~~~~~~~~~~~~~~~~

`ncont.exam <../examples/unix/runnable/ncont.exam>`__

SEE ALSO
--------

| `pdbcur <pdbcur.html>`__ - MMDB application for manipulating PDB
  files.
| `act <act.html>`__ - traditional contact program.
| `contact <contact.html>`__ - traditional contact program.

AUTHORS
-------

Eugene Krissinel, European Bioinformatics Institute, Cambridge, UK.
