TLSANL (CCP4: Supported Program)
================================

NAME
----

**tlsanl** - analysis of TLS tensors and derived anisotropic U factors

SYNOPSIS
--------

| **tlsanl xyzin** *input.pdb* **tlsin** *input.tls* **xyzout**
  *output.pdb* [ **axes** *output.in* ] [ **sktout** *skttls.log* ]
| [`Keyworded input <#keywords>`__]

DESCRIPTION
-----------

The program analyses TLS tensors and is intended for use with the output
of `REFMAC <refmac5.html>`__ or `RESTRAIN <restrain.html>`__. It can be
used to generate atomic displacement parameters from the TLS parameters.

INPUT AND OUTPUT FILES
----------------------

TLSIN
~~~~~

An ASCII file containing details of the TLS groups and the values of the
corresponding T, L and S tensors. Typically, this will correspond to the
TLSOUT file of REFMAC or RESTRAIN. See the
`RESTRAIN <restrain.html#section3.4>`__ documentation for details of the
file format. In addition to the records described there, the file may
contain a line "REFMAC" denoting that the file has been output from the
refinement program `REFMAC <refmac5.html>`__, which has a different
order of tensor elements.

XYZIN
~~~~~

Input coordinates in PDB format. Typically, this will correspond to the
XYZOUT file of REFMAC or RESTRAIN. ANISOU records are ignored. ATOM or
HETATM records may contain the full B factor (as output by RESTRAIN and
REFMAC if TLSOUT ADDU is specified) or only the residual B factor, which
doesn't include the contribution from the TLS parameters (as for REFMAC
without TLSOUT ADDU). The program interprets the B factors according to
the keyword `BRESID <#bresid>`__.

XYZOUT
~~~~~~

Output coordinates and anisotropic tensors in PDB format. XYZOUT differs
from XYZIN only in the values of the B factors and the anisotropic U
tensors.

For atoms which are not in TLS groups but have ANISOU records in XYZIN,
those ANISOU records are written to XYZOUT without change to either the
ANISOU record or the B factor in the ATOM or HETATM record.

*NOTE:* This is a change to the previous behaviour (CCP4 6.1.1 and
earlier) where no ANISOU records from XYZIN were written to XYZOUT.

The ATOM records may contain the B factor derived from the TLS
parameters, the residual B factor, or their sum, as determined by the
keyword `ISOOUT <#isoout>`__. Similarly, the ANISOU records may contain
the anisotropic U tensors derived from the TLS parameters, the residual
B factors expressed as a tensor, or their sum, as determined by the
keyword `ISOOUT <#isoout>`__. The B factor in the ATOM line should
always correspond to the isotropic component of the anisotropic tensor
in the ANISOU line.

*NOTE:* This is a change to the previous behaviour (CCP4 5.0.2 and
earlier) where the ANISOU records contained the full ADPs irrespective
of the ISOOUT keyword. The ATOM and ANISOU records are now consistent,
as required by some other programs.

AXES
~~~~

Output file containing the axes for translation, libration,
screw-rotation, reduced translation and non-intersecting screw-rotation.
By default, the file is in a suitable format for including in a
MOLSCRIPT ``.in`` file. The file can also be written in mmCIF format:
this can be used, for example, by CCP4 Molecular Graphics. See
`**AXES** <#axes>`__ keyword below.

SKTOUT
~~~~~~

Output file containing results of several kinds of tests on the TLS
model (SKTTLS) comparing ADPs for the atoms in bonds between residues.
The same output is written to the log file after the analysis of
tensors. The SKTOUT file can be used for plotting with programs such as
gnuplot. See `**SKTTLS Report** <#skttls>`__ below.

KEYWORDED INPUT
---------------

Available keywords are:

    `**ANISO** <#aniso>`__, `**BINPUT** <#binput>`__,
    `**BRESID** <#bresid>`__, `**ISOOUT** <#isoout>`__,
    `**NUMERIC** <#numeric>`__, `**AXES** <#axes>`__,
    `**END** <#end>`__.

 ANISO
~~~~~~

Do anisotropic tensor analysis (default: don't). The principal axes of
the anisotropic tensor are determined for each atom in each TLS group
and written to the log output. If the tensor is non-positive definite,
then a warning is given. Note that the program
`ANISOANL <anisoanl.html>`__ also checks for non-positive definite
anisotropic tensors, and this may be more convenient. The anisotropic
tensor includes the isotropic contribution as well as the contribution
from the TLS parameters.

 BINPUT [true \| t \| false \| f]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If true (default), assume PDB file contains isotropic B factors. Else,
assume PDB file contains isotropic U factors (smaller by 8\*pi\*\*2).

 BRESID [true \| t \| false \| f]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If true, assume the ATOM records in XYZIN contain only residual B
factors, and don't include the contribution from the TLS parameters
(this is the case for Refmac if TLSOUT ADDU is *not* specified). If
false (default), assume the ATOM records contain the full B factor,
including the contribution from TLS (as for Refmac if TLSOUT ADDU is
specified).

 ISOOUT [FULL \| RESI \| TLSC]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Determine whether the ATOM and ANISOU records in XYZOUT contain B and U
factors calculated from the TLS parameters (TLSC), residual B factors
(RESI), or the sum of both (FULL). Default is FULL.

NUMERIC
~~~~~~~

If this keyword is present TLS ranges are assumed to refer to the
monotonically ascending numerical order of residue numbers within each
chain.  The keyword causes the ATOM & HETATM records in the input PDB to
be first sorted on chain ID and residue number before the TLS ranges are
interpreted (the order in the output PDB file is not affected).  The
default is the previous behaviour, *i.e.* to assume that the TLS ranges
refer to the order of residues in the input PDB file (again within each
chain).  The latter is the PDB convention for the order of linking
residues in the sequence so it seems logical that this should also be
the standard way of referring to the TLS residue ranges.  In most cases
hopefully the residues in each chain will already be in monotonically
ascending numerical order, in which case this option will have
absolutely no effect!

 AXES [FORMAT MOLSCRIPT \| MMCIF] [ < scale > ]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A file (logical name AXES) is output containing the axes for
translation, libration, screw-rotation, reduced translation and
non-intersecting screw-rotation, for each TLS group. All axes are
calculated in an orthogonal frame with respect to the centre of
reaction.

By default, or if FORMAT MOLSCRIPT is given, the file is in a suitable
format for including in a MOLSCRIPT ``.in`` file. If FORMAT MMCIF is
given, the file is written in mmCIF format. The axes are identified as
follows:

+-------------------------------+---------------------------------+-----------------------------+
| Axes                          | Description in Molscript file   | Description in mmCIF file   |
+===============================+=================================+=============================+
| translation T                 | Translation axes                | TRAN                        |
+-------------------------------+---------------------------------+-----------------------------+
| libration L                   | Libration axes                  | LIB                         |
+-------------------------------+---------------------------------+-----------------------------+
| screw tensor S                | Screw rotation axes             | SCREWROT                    |
+-------------------------------+---------------------------------+-----------------------------+
| reduced translation           | Reduced translation axes        | RTRAN                       |
+-------------------------------+---------------------------------+-----------------------------+
| non-intersecting screw axes   | Non-intersecting screw axes     | SCREW                       |
+-------------------------------+---------------------------------+-----------------------------+

Each axis is written as a pair of coordinates (in the coordinate system
of the input PDB file) representing the beginning and the end of the
axis. For the intersecting axes, the intersection is at the centre of
reaction of the TLS group. The length of each axis is proportional to
the magnitude of the corresponding motion. The proportionality factor
can be adjusted by the argument < scale >, which defaults to 10.

 END
~~~~

Terminate input.

PROGRAM OUTPUT
--------------

For each TLS group included in the file TLSIN, the program outputs
information about the TLS tensors in several representations. Two
coordinate origins are considered:

#. The origin used in refinement, and given by the ORIGIN record in
   TLSIN.
#. The centre of reaction, which is the origin that makes S symmetric.
   TLS tensors with respect to this origin are flagged with a prime (').

Three axial systems are considered:

#. Orthogonal axes, as used in XYZIN and TLSIN
#. Libration axes, *i.e.* the principal axes of the L tensor. TLS
   tensors in this axial system are flagged with a caret (^).
#. Rigid-body axes.

The values of the T, L and S tensors are given with respect to both
origins, and in the first two axial systems. The L tensor is in general
independent of the origin, and in particular is the same for both
origins considered here. In the axial system defined by the libration
axes only, the diagonal elements of S are also independent of origin.

The values of the T and S tensors held in TLSIN thus depend on the
origin chosen for their calculation. The Centre of Reaction, however, is
independent of the original origin, and hence so are quantities
calculated with respect to the Centre of Reaction.

The rigid body motion can also be described as 3 screw motions about 3
non-intersecting axes parallel to the libration axes, together with a
reduced translation tensor (see `Schomaker and Trueblood
(1968) <#reference2>`__). The program gives the absolute position of
these screw axes and the pitch of the screw motion along them, followed
by the reduced translation tensor.

SKTTLS Report:
^^^^^^^^^^^^^^

Several kinds of tests on the TLS model (SKTTLS) are reported following
the tensor analysis; if a SKTOUT file is given, a copy of this SKTTLS
report is also written to that file. Three residuals are calculated for
each bond between protein or nucleic acid residues:

#. CCuij, the correlation of anisotropic ADPs from `Merritt
   (1999) <#reference3>`__ (ranges from 1 down);
#. rSIMU, residual of the SIMU (BFAC) restraint, rmsd of ANISOU values
   (ranges from 0 up); and
#. rDELU, residual of the DELU (RBON) Rosenfeld rigid bond restraint
   (ranges from 0 up).

The 95th and 99th percentile values of these three residuals were
calculated from a survey of the REFMAC refinements with segmented TLS in
the PDB as of Sept. 2009. Extreme values of these residuals, *e.g.*
CCuij below the 99th percentile, may indicate problems with the ADPs for
the atoms in the bond: the TLS segment boundary was misassigned, or
something went wrong during refinement, or one or both atoms have
non-positive definite ADPs.

The SKTTLS report has summaries and the full table of these residuals
plus a table of the distribution of anisotropies. Summaries include:

-  the number of bonds for which residuals were counted, and the number
   of bonds with any residual beyond the 95th or 99th percentile;
-  the mean, standard deviation and worst value of each residual for
   this structure;
-  the mean and standard deviation anisotropy for all protein, nucleic
   acid and other non-solvent atoms in this structure;
-  a list of up to 100 outliers, bonds in this structure which are
   beyond the 95th percentils for any residual. If there are more than
   100 such outliers, this analysis should be run again after whatever
   caused those problems is fixed.

The table of all residuals, SKTTLS TABLE 1, and the table of anisotropy
distributions, SKTTLS TABLE 2, are formatted for loggraph or xloggraph
plotting. In addition, the files are formatted so that simple extracts
of these tables can be plotted by programs such as gnuplot: all
non-blank lines other than table data lines are prefixed with '#' and
each table is bracketted by lines of the form:
`` "### START OF SKTTLS TABLE N ###" `` and
`` "### END OF SKTTLS TABLE N ###"``.

SKTTLS TABLE 1 contains bond identifiers, TLS group identifiers, and
residual values. Bond identifiers are residue number, chain ID as text
and as a number (to facilitate plotting of separate chains in gnuplot)
and bond name *e.g.* C1-N2 or, if there are alternate atoms,
C(A)1-N(A)2. TLS group IDs are TLS group number (numbered sequentially
from 1 by their order in TLSIN, rather than the given group number) and
a point at each segment break. The opposite of rDELU is included after
the residual values so that all 3 residuals can be plotted by loggraph
with minimal overlapping. The first two lines of the table contain
values for the 95th and 99th percentile of each residual.

SKTTLS TABLE 2 contains anisotropy bin minimum, maximum and center, and
fractions and counts for protein, nucleic acid or other non-solvent
atoms. Non-positive definite ADPs are counted in the first bin, -0.05 to
0.00; isotropic ADPS are counted in the last bin, 1.00 to 1.00.

SKTTLS TABLE 3 is the list of outliers, containing bond identifiers and
all three residuals values for each outlying bond. Marks (? or !) show
which residuals were beyond the 95 or 99th percentile. This table is
**not** formatted for plotting.

*NOTE:* This is a change to the previous behaviour (CCP4 6.1.1 and
earlier) where no ANISOU records from XYZIN were written to XYZOUT.

EXAMPLES
--------

For files generated by `RESTRAIN <restrain.html>`__, defaults can be
accepted, although the ANISO keyword might be useful:

::


    tlsanl XYZIN foo_in.pdb TLSIN foo_in.tls XYZOUT foo_out.pdb <<eof
    ANISO
    END
    eof

For files generated by `REFMAC <refmac5.html>`__, the BRESID keyword
will be needed:

::


    tlsanl XYZIN foo_in.pdb TLSIN foo_in.tls XYZOUT foo_out.pdb SKTOUT foo_skt.log <<eof
    BRESID
    END
    eof

REFERENCES
----------

#.  B.Howlin, S.A.Butler, D.S.Moss, G.W.Harris and H.P.C.Driessen,
   "TLSANL: TLS parameter analysis program for segmented anisotropic
   refinement of macromolecular structures.", *J. Appl. Cryst.*, **26**,
   622-624 (1993)
#.  V.Schomaker and K.N.Trueblood *Acta Cryst.*, **B24**, 63 (1968)
#.  E.A.Merritt *Acta Cryst.*, **D55**, 1997 (1999)

SEE ALSO
--------

`restrain <restrain.html>`__, `refmac <refmac5.html>`__

AUTHORS
-------

Huub Driessen, David Moss, Ian Tickle *et al.*, Birkbeck College.

Contact: Ian Tickle (tickle@mail.cryst.bbk.ac.uk), Martyn Winn
(martyn.winn@stfc.ac.uk)
