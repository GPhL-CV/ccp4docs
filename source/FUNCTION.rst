CCP4 v7.0 Program Documentation
===============================

| **Reference:**
| Collaborative Computational Project, Number 4. 1994.
| "The CCP4 Suite: Programs for Protein Crystallography". Acta Cryst.
  D50, 760-763

There is a partial list of `program references <REFERENCES.html>`__
extracted from the individual program documentation.

--------------

| **Changes:**
| List of the `major program changes <CHANGESinV7_0.html>`__ since the
  previous release.

--------------

This list can also be seen in `alphabetical order. <INDEX.html>`__

AUTOMATED MODEL BUILDING
------------------------

-  `privateer <privateer.html>`__ - Validation of carbohydrate
   structures

COORDINATE FILE MANIPULATIONS
-----------------------------

-  `bones2pdb <bones2pdb.html>`__ - Make a PDB pseudo-coordinate file
   from a bones file
-  `chainsaw <chainsaw.html>`__ - Mutate a pdb file according to an
   input sequence alignment
-  `clustalw interface <clustalw.html>`__ - Graphical Interface to the
   ClustalW Program
-  `coordconv <coordconv.html>`__ - Interconvert various coordinate
   formats
-  `coord\_format <coord_format.html>`__ - fix PDB format and convert
   to/from other formats
-  `csymmatch <csymmatch.html>`__ - Use symmetry to match chains
-  `gensym <gensym.html>`__ - generate sites by symmetry
-  `geomcalc <geomcalc.html>`__ - molecular geometry calculations
-  `gesamt <gesamt.html>`__ - structural alignment
-  `hgen <hgen.html>`__ - generate hydrogen atom positions for proteins
-  `lsqkab <lsqkab.html>`__ - apply various transformations to
   coordinate files
-  `makedict <makedict.html>`__ - converts PDB file to TNT or PROTIN
   dictionaries and PROTIN to PDB
-  `pdbcur <pdbcur.html>`__ - various useful manipulations on coordinate
   files
-  `pdb\_merge <pdb_merge.html>`__ - merge two coordinate files into one
-  `pdbset <pdbset.html>`__ - various useful manipulations on coordinate
   files
-  `reforigin <reforigin.html>`__ - Apply best origin shift to PDB atom
   co-ords according to reference file (unsupported)
-  `rwcontents <rwcontents.html>`__ - Count atoms by type
-  `sortwater <sortwater.html>`__ - sort waters by the protein chain to
   which they "belong"
-  `superpose <superpose.html>`__ - structural alignment based on
   secondary structure matching
-  `watertidy <watertidy.html>`__ - rationalise waters at the end of
   refinement
-  `watpeak <watpeak.html>`__ - selects peaks from peakmax and puts them
   close to the respective protein atoms

DATA HARVESTING AND DATASETS
----------------------------

-  `cif2xml <cif2xml.html>`__ - Conversion of mmCIF files to XML
-  `cross\_validate <cross_validate.html>`__ - Validation of harvest
   files for deposition
-  `data harvesting manager <dhm_tool.html>`__ - Tool for managing Data
   Harvesting Files
-  `harvesting <harvesting.html>`__ - harvesting data automatically and
   using datasets
-  `harvlib <harvlib.html>`__ - Subroutine library for writing CIF
   harvest files
-  `pdb\_extract <pdb_extract.html>`__ - RCSB/PDB Programs for
   extracting harvest information from program log files
-  `r500 <r500.html>`__ - Checks PDB files for any problems before
   submission

DATA PROCESSING AND REDUCTION
-----------------------------

-  `aimless <aimless.html>`__ - scale together multiple observations of
   reflections
-  `blend <blend.html>`__ - multi-dataset combination and cluster
   analysis
-  `combat <combat.html>`__ - produces an MTZ file in multirecord form
   suitable for input to SCALA.
-  `crossec <crossec.html>`__ - interpolate X-ray cross sections and
   compute anomalous scattering factors
-  `ctruncate <ctruncate.html>`__ - Intensity to amplitude conversion
-  `detwin <detwin.html>`__ - detwins merohedrally twinned data
-  `dtrek2mtz <dtrek2mtz.html>`__ - converts d\*trek scalemerge output
   into MTZ format
-  `dtrek2scala <dtrek2scala.html>`__ - initial processing of intensity
   files from D\*TREK
-  `othercell <othercell.html>`__ - explore equivalent alternative unit
   cells
-  `pointless <pointless.html>`__ - determine Laue group
-  `rebatch <rebatch.html>`__ - alter batch numbers in an unmerged MTZ
   file
-  `reindex <reindex.html>`__ - produces an MTZ file with h k l
   reindexed and/or the symmetry changed
-  `reindexing <reindexing.html>`__ - information about changing
   indexing regime (see reindex)
-  `rotaprep <rotaprep.html>`__ - produces an MTZ file in multirecord
   form suitable for input to SCALA.
-  `scala <scala.html>`__ - scale together multiple observations of
   reflections
-  `scalepack2mtz <scalepack2mtz.html>`__ - converts merged scalepack
   output into MTZ format
-  `truncate <truncate.html>`__ - obtain structure factor amplitudes
   using Truncate procedure
-  `twinning <twinning.html>`__ - dealing with data from twinned
   crystals
-  `unique <unique.html>`__ - Generate a unique list of reflections
   (including uniqueify)
-  `wilson <wilson.html>`__ - Wilson plot, absolute scale and
   temperature factor
-  `xia2 <xia2.html>`__ - Automated data reduction

DENSITY FITTING AND MODEL BUILDING
----------------------------------

-  `arp\_waters <arp_waters.html>`__ - Automated Refinement Procedure
   for refining protein structures
-  `buccaneer <cbuccaneer.html>`__ - Statistical Model Building
-  `prodrg <cprodrg.html>`__ - generation of small molecule coordinates
-  `fffear fragment library <fffear_fraglib.html>`__ - map
   interpretation package
-  `fffear <fffear.html>`__ - map interpretation package
-  `ffjoin <ffjoin.html>`__ - joining model fragments from FFFEAR
-  `rapper <rapper.html>`__ - conformer modelling and building
-  `sequins <sequins.html>`__ - Statistical Model Building

DENSITY MODIFICATION - MOLECULAR AVERAGING
------------------------------------------

-  `hltofom <chltofom.html>`__ - Convert to/from Hendrickson-Lattman
   coefficients
-  `cmakereference <cmakereference.html>`__ - Generate reference
   structure for pirate/buccaneer
-  `pirate <cpirate.html>`__ - Statistical Phase Improvement
-  `dm <dm.html>`__ - density modification package
-  `dmmulti <dmmulti.html>`__ - multi-xtal density modification package
-  `dm\_ncs\_averaging <dm_ncs_averaging.html>`__ - dm for ncs averaging
   (see DM program)
-  `dm\_skeletonisation <dm_skeletonisation.html>`__ - iterative
   skeletonisation using dm (see DM program)
-  `matthews\_coef <matthews_coef.html>`__ - Misha Isupov's Jiffy to
   calculate Matthews coefficient
-  `ncsmask <ncsmask.html>`__ - averaging mask manipulation program
-  `parrot <parrot.html>`__ - density modification package
-  `professs <professs.html>`__ - determination of NCS operators from
   heavy atoms
-  `solomon <solomon.html>`__ - density modification (phase improvement)
   by solvent flipping

EXPERIMENTAL PHASING
--------------------

-  `crank <crank.html>`__ - Automated structure solution for
   experimental phasing
-  `rantan <rantan.html>`__ - Direct Method module for the determination
   of heavy atom positions

MAP CALCULATION AND MANIPULATION
--------------------------------

-  `edstats <edstats.html>`__ - Calculate electron density statistics
-  `extends <extends.html>`__ - Extend Fourier maps and compute standard
   uncertainty of electron density
-  `fft <fft.html>`__ - fast Fourier transform
-  `mama2ccp4 <mama2ccp4.html>`__ - Convert between \`mama' and
   Cambridge/CCP4 map formats
-  `map2fs <map2fs.html>`__ - convert CCP4 map to XtalView fsfour format
-  `mapdump <mapdump.html>`__ - print a dump of sections of a map file
-  `mapmask <mapmask.html>`__ - map/mask extend program
-  `maprot <maprot.html>`__ - map skewing, interpolating, rotating,
   averaging and correlation masking program
-  `mapsig <mapsig.html>`__ - print statistics on signal/noise for
   translation function map
-  `maptona4 <maptona4.html>`__ - Convert binary map file to and from
   na4 ascii format
-  `omit <omit.html>`__ - program to calculate omit-maps according to
   Bhat procedure
-  `overlapmap <overlapmap.html>`__ - calculates the average of two maps
-  `peakmax <peakmax.html>`__ - search for peaks in the electron density
   map
-  `sigmaa <sigmaa.html>`__ - Improved Fourier coefficients using
   calculated phases
-  `xdlmapman <xdlmapman.html>`__ - manipulation, analysis and
   reformatting of electron density maps (X-windows tool)

MAXIMUM LIKELIHOOD PHASING
--------------------------

-  `phaser-2.5.5 <phaser.html>`__ - Molecular Replacement, SAD Phasing,
   Anisotropy Correction, Cell Content Analysis, Translational NCSand
   Twin Analysis

MODEL ANALYSIS
--------------

-  `act <act.html>`__ - analyse coordinates
-  `areaimol <areaimol.html>`__ - Analyse solvent accessible areas
   (supported)
-  `baverage <baverage.html>`__ - averages B over main and side chain
   atoms
-  `cavenv <cavenv.html>`__ - Visualise cavities in macromolecular
   structures
-  `contact <contact.html>`__ - computes various types of contacts in
   protein structures
-  `distang <distang.html>`__ - Distances and angles calculation
-  `dyndom <dyndom.html>`__ - determine dynamic domains when two
   conformations are available
-  `ncont <ncont.html>`__ - computes various types of contacts in
   protein structures
-  `pandda <pandda.html>`__ - Multi-dataset crystallographic analysis
   methods of ligands
-  `polypose <polypose.html>`__ - program for superimposing many
   multi-domain structures
-  `procheck <procheck.html>`__ - programs to check the Stereochemical
   Quality of Protein Structures
-  `rampage <rampage.html>`__ - Ramachandran plots
-  `rotamer <rotamer.html>`__ - List amino acids whose side chain
   torsion angles deviate from the Penultimate Rotamer Library
-  `rotamer table <rotamer_table.html>`__ - rotamer tables used in the
   \`rotamer' program
-  `sc <sc.html>`__ - analyse shape complementarity (supported)
-  `seqwt <seqwt.html>`__ - Calculate molecular weight from sequence
-  `sfcheck <sfcheck.html>`__ - program for assessing the agreement
   between the atomic model and X-ray data
-  `surface <surface.html>`__ - surface accessibility program and for
   preparing input file to program volume
-  `topp <topp.html>`__ - a topological comparison program
-  `volume <volume.html>`__ - polyhedral volume around selected atoms

MODEL REFINEMENT
----------------

-  `anisoanl <anisoanl.html>`__ - analyses of anisotropic displacement
   parameters
-  `arp\_waters <arp_waters.html>`__ - Automated Refinement Procedure
   for refining protein structures
-  `dimple <dimple.html>`__ - automated difference map calculation
-  `intro\_mon\_lib <intro_mon_lib.html>`__ - description of monomer
   library used in CCP4
-  `libcheck <libcheck.html>`__ - monomer library management program
-  `lib\_list <lib_list.html>`__ - contents of multi-purpose monomer
   dictionary used by REFMAC
-  `mon\_lib <mon_lib.html>`__ - description of multi-purpose monomer
   dictionary used by REFMAC
-  `rdent <rdent.html>`__ - Create dictionary entries for Restrain from
   PDB file
-  `refmac5 <refmac5.html>`__ - macromolecular refinement program
-  `restrain <restrain.html>`__ - macromolecular refinement program
-  `sfall <sfall.html>`__ - Structure factor calculation and X-ray
   refinement using forward and reverse FFT
-  `sketcher <sketcher.html>`__ - monomer library sketcher
-  `tlsanl <tlsanl.html>`__ - analysis of TLS tensors and derived
   anisotropic U factors
-  `tlsextract <tlsextract.html>`__ - extract TLS group description from
   a PDB file
-  `watncs <watncs.html>`__ - Pick waters which follow NCS and sort out
   to NCS asymmetric unit

MOLECULAR REPLACEMENT
---------------------

-  `almn <almn.html>`__ - calculates rotation function overlap values
   using FFT techniques (alternative to AMORE)
-  `amore <amore.html>`__ - Jorge Navaza's state-of-the-art molecular
   replacement package
-  `beast <beast.html>`__ - Likelihood-based molecular replacement
-  `bulk <bulk.html>`__ - bulk solvent correction for translation search
   and rigid body refinement steps of AMoRe
-  `ecalc <ecalc.html>`__ - calculate normalised structure amplitudes
-  `fsearch <fsearch.html>`__ - 6-d molecular replacement (envelope)
   search
-  `getax <getax.html>`__ - real space correlation search
-  `molrep <molrep.html>`__ - automated program for molecular
   replacement
-  `mrbump <mrbump.html>`__ - automated search model generation and
   automated molecular replacement
-  `polarrfn <polarrfn.html>`__ - fast rotation function that works in
   polar angles
-  `rfcorr <rfcorr.html>`__ - Analysis of correlations between cross-
   and self-Rotation functions
-  `rotmat <rotmat.html>`__ - interconverts CCP4/MERLOT/X-PLOR rotation
   angles
-  `rsearch <rsearch.html>`__ - R-factor and correlation coefficient
   between Fcalc and Fobs
-  `seqwt <seqwt.html>`__ - Calculate molecular weight from sequence
-  `tffc <tffc.html>`__ - Translation Function Fourier Coefficients

MTZ MANIPULATIONS / CONVERSIONS
-------------------------------

-  `cad <cad.html>`__ - Collect and sort crystallographic reflection
   data from several files
-  `cif2mtz <cif2mtz.html>`__ - Convert an mmCIF reflection file to MTZ
   format
-  `convert2mtz <convert2mtz.html>`__ - CNS to MTZ conversion
-  `cphasematch <cphasematch.html>`__ - Calculate agreement between
   phase sets
-  `ecalc <ecalc.html>`__ - calculate normalised structure amplitudes
-  `f2mtz <f2mtz.html>`__ - Convert a formatted reflection file to MTZ
   format
-  `freerflag <freerflag.html>`__ - tags each reflection in an MTZ file
   with a flag for cross-validation
-  `freerunique <freerunique.html>`__ - convert FreeRflags between CCP4
   and other formats (see freerflag,uniqueify,mtz2various,f2mtz)
-  `mtz2cif <mtz2cif.html>`__ - produce mmCIF structure factor file
   suitable for deposition
-  `mtz2various <mtz2various.html>`__ - produces reflexion file for
   MULTAN, SHELX, TNT, X-PLOR/CNS, pseudo-SCALEPACK, XtalView, mmCIF or
   other ascii format
-  `mtzdump <mtzdump.html>`__ - dump data from an MTZ reflection data
   file
-  `mtzMADmod <mtzMADmod.html>`__ - Generate F+/F- or F/D from other for
   anomalous data
-  `mtzmnf <mtzmnf.html>`__ - Identify missing data entries in an MTZ
   file and replace with a Missing Number Flag (MNF)
-  `mtztona4 <mtztona4.html>`__ - interconvert MTZ reflection file and
   ASCII format
-  `mtzutils <mtzutils.html>`__ - Reflection data files utility program
-  `sftools <sftools.html>`__ - reflection data file utility program
-  `sortmtz <sortmtz.html>`__ - Sort a MTZ reflection data file
-  `xdldataman <xdldataman.html>`__ - manipulation, analysis and
   reformatting of reflection files (X-windows tool)

MULTIMER ANALYSIS
-----------------

-  `pisa <pisa.html>`__ - Protein interfaces, surfaces and assemblies

MULTIPLE ISOMORPHOUS REPLACEMENT
--------------------------------

-  `abs <abs.html>`__ - determine the absolute configuration (hand) of
   the heavy atom substructure
-  `afro <afro.html>`__ - multivariate substructure factor amplitude
   estimation for SAD/MAD and SIRAS
-  `bp3 <bp3.html>`__ - multivariate likelihood substructure refinement
   and phasing of S/MIR(AS) and/or S/MAD
-  `findncs <findncs.html>`__ - detect NCS operations automatically from
   heavy atom sites
-  `gcx <gcx.html>`__ - support program to generate crank XML and run
   crank via a script
-  `mlphare <mlphare.html>`__ - maximum likelihood heavy atom refinement
   and phase calculation
-  `oasis <oasis.html>`__ - A program for breaking phase ambiguity in
   OAS or SIR
-  `peakmax <peakmax.html>`__ - search for peaks in the electron density
   map
-  `rsps <rsps.html>`__ - heavy atom positions from derivative
   difference Patterson maps
-  `sapi <sapi.html>`__ - heavy atom site location
-  `vecref <vecref.html>`__ - Vector-space refinement of heavy atom
   sites in isomorphous derivatives
-  `vecsum <vecsum.html>`__ - Program to deconvolute a Patterson
   function and solve the structure (unsupported)
-  `vectors <vectors.html>`__ - generates Patterson vectors from atomic
   coordinates

MULTIWAVELENGTH ANOMALOUS DISPERSION
------------------------------------

-  `abs <abs.html>`__ - determine the absolute configuration (hand) of
   the heavy atom substructure
-  `afro <afro.html>`__ - multivariate substructure factor amplitude
   estimation for SAD/MAD and SIRAS
-  `bp3 <bp3.html>`__ - multivariate likelihood substructure refinement
   and phasing of S/MIR(AS) and/or S/MAD
-  `crossec <crossec.html>`__ - interpolate X-ray cross sections and
   compute anomalous scattering factors
-  `gcx <gcx.html>`__ - support program to generate crank XML and run
   crank via a script
-  `mlphare <mlphare.html>`__ - maximum likelihood heavy atom refinement
   and phase calculation
-  `oasis <oasis.html>`__ - A program for breaking phase ambiguity in
   OAS or SIR
-  `revise <revise.html>`__ - estimates optimised value of the
   normalised anomalous scattering using MAD data
-  `sapi <sapi.html>`__ - heavy atom site location

NATIVE PROTEIN DATA AT ATOMIC RESOLUTION
----------------------------------------

-  `acorn <acorn.html>`__ - Molecular Replacement, Sayre Equation and
   Dynamic Density Modification for the determination of a protein
   structure

OTHERS/JIFFIES
--------------

-  `phistats <phistats.html>`__ - Analysis of agreement between phase
   sets, and checking it against weighting factors
-  `stereo <stereo.html>`__ - Extract coordinates from stereo diagrams
-  `stgrid <stgrid.html>`__ - Generate plot to measure angular
   coordinates on a stereographic projection from polarrfn
-  `stnet <stnet.html>`__ - Generate plot to measure angles between
   points on a stereographic projection from polarrfn
-  `tracer <tracer.html>`__ - Lattice TRAnsformation and CEll Reduction

PLOTTING / GRAPHICS
-------------------

-  `openastexviewer <AstexViewer.html>`__ - Java program for display
   molecular structures and electron density maps
-  `ccp4mapwish <ccp4mapwish.html>`__ - custom wish interpreter required
   for MapSlicer
-  `hklplot <hklplot.html>`__ - plots a precession photo from an HKL
   data file
-  `hklview <hklview.html>`__ - displays zones of reciprocal space as
   pseudo-precession images (X-windows program)
-  `ipdisp <ipdisp.html>`__ - displays images from a variety of
   (crystallographic) sources (X-Windows tool)
-  `loggraph <loggraph.html>`__ - viewer for CCP4 formatted \`log' files
-  `mapslicer <mapslicer.html>`__ - interactive section viewer for CCP4
   map files
-  `npo <npo.html>`__ - Molecule and map plotting
-  `pltdev <pltdev.html>`__ - convert Plot84 meta-files to PostScript,
   Tektronix or HPGL
-  `topdraw <topdraw.html>`__ - Sketchpad for protein topology diagrams
   (supported)
-  `xloggraph <xloggraph.html>`__ - a viewer of specially formatted
   \`log' files (X-windows tool)
-  `xplot84driver <xplot84driver.html>`__ - a viewer for Plot84 meta
   files (X-windows tool)

PROTEIN SEQUENCE FILE MANIPULATIONS
-----------------------------------

-  `clustalw interface <clustalw.html>`__ - Graphical Interface to the
   ClustalW Program
-  `import/edit protein sequence <get_prot.html>`__ - Import and edit
   protein sequences

ROTATION MATRICES
-----------------

-  `rotationmatrices <rotationmatrices.html>`__ - on Eulerian angles,
   polar angles and direction cosines, and orthogonalisation codes

SCALING DATA
------------

-  `fhscal <fhscal.html>`__ - Scaling of isomorphous derivative data
   using Kraut's method (see also SCALEIT)
-  `icoefl <icoefl.html>`__ - vectorially combined scaling of Fobs
   (Iobs) with partial Fc's
-  `rstats <rstats.html>`__ - scale together two sets of F's
-  `scalechoose <scalechoose.html>`__ - information about choice of
   scaling program (see reindex)
-  `scaleit <scaleit.html>`__ - derivative to native scaling (see also
   FHSCAL)

SYMMETRY UTILITIES
------------------

-  `symconv <symconv.html>`__ - Fetch and convert symmetry and
   spacegroup information

VALIDATION AND DEPOSITION
-------------------------

-  `data harvesting manager <dhm_tool.html>`__ - Tool for managing Data
   Harvesting Files
-  `pdb\_extract <pdb_extract.html>`__ - RCSB/PDB Programs for
   extracting harvest information from program log files
-  `r500 <r500.html>`__ - Checks PDB files for any problems before
   submission
