LOGGRAPH FORMAT: (CCP4: Formats)
================================

NAME
----

**Loggraph format for CCP4** - the format for tables and graphs as used
in CCP4 log files

#. `INTRODUCTION <#introduction>`__
#. `Syntax of the Graph description in a log file <#graphsyntax>`__
#. `Parsing algorithm for the table <#parsingtables>`__
#. `Syntax of the Text description in log file <#textsyntax>`__
#. `Syntax of the Key Text description in log file <#keytextsyntax>`__
#. `Syntax of Summary description <#summarysyntax>`__
#. `SEE ALSO <#see_also>`__

1. INTRODUCTION
---------------

This document describes the formats recognised by the CCP4 programs
`loggraph <loggraph.html>`__ and `xloggraph <xloggraph.html>`__, and the
*Jloggraph* Java applet (see the `libhtml <libhtml.html#log_graphs>`__
documentation, where embedding loggraphs with HTML log files is
described further).

2. Syntax of the Graph description in a log file
------------------------------------------------

The following sections describe the syntax of TABLEs and GRAPHs.

::

    $TABLE :table name:
    $GRAPHS :graph1 name:graphtype:column_list: :graph2 name:graphtype:column_list:
            :graph 3 ...: ... $$
    column1_name column2_name ... $$ any_characters $$ numbers $$

where:

 table name, graphN name
    are arbitrary strings (without newline)
 columnN\_name
    are arbitrary strings without tab, space, newline
 graphtype
    is
 A[UTO]
    for fully automatic scaling (e.g. ... :A:1,2,4,5:)
 N[OUGHT]
    for automatic y coordinate scaling, where y lowest limit is 0 (e.g.
    ... :N:1,2,4,5:)
 XMIN\|XMAXxYMIN\|YMAX
    for user defined scaling where XMIN ... are axis limits (e.g. ...
    :0\|100x-1\|1:1,2,4,5:)
 any\_characters
    are treated as a comment. They can be eventually used as a human
    oriented table header
 numbers
    represents the table itself. (See parsing algorithm below)

Scatter Plots
~~~~~~~~~~~~~

loggraph can be directed to plot graphs as scatter plots (i.e. points
only, not connected by lines) by substituting the keyword ``SCATTER``
for the ``GRAPHS`` keyword:

::

    $TABLE :table name:
    $SCATTER :graph1 name:graphtype:column_list: :graph2 name:graphtype:column_list:
            :graph 3 ...: ... $$
    column1_name column2_name ... $$ any_characters $$ numbers $$

This functionality is not supported in either Xloggraph or Jloggraph.

3. Parsing algorithm for the table
----------------------------------

The table values (numbers) are read in free format. This implies, that
they MUST be separated by space(s) and/or tab(s) and/or newline(s).
Parsing is performed on a logical line basis, where a logical line
begins with a newline, followed by a number of values corresponding to
the number of table columns, an optional comment (any characters) up to
the next newline delimiter. The term \`logical' is used, because this
'logical' line can consist of several \`physical' lines, where newlines
between values are treated as ordinary delimiters.

Loggraph parsing algorithm
~~~~~~~~~~~~~~~~~~~~~~~~~~

Loggraph differs from Xloggraph and Jloggraph in that any table value
which is not recognised as a numerical value is counted but is not read.
This means it is possible to use non-numerical values as placeholders in
tables, e.g.:

::

    $TABLE: Example table from loggraph format documentation :
    $GRAPHS:Example graph 1:A:1,4,5:  $$

        N Run.Rot      Value1       Value2    Value3
    $$ $$
        1   1.1        343.50       1.1230    1.1044
        2   1.2        344.50       1.1632    *
        3   1.3        345.50       1.2099    1.1799
        4   1.4        346.50       1.2636    *
        5   1.5        347.50       1.3180    *
        6   1.6        348.50       1.3613    1.3061
    $$

Any non-numerical token can be used, however in practice the characters
\*, ? and . are most commonly used in CCP4.

4. Syntax of the Text description in log file
---------------------------------------------

These tags are only recognised by Xloggraph and their use is deprecated.

::

    $TEXT :text name: $$ junk (ignored) text $$any text characters$$

5. Syntax of the Key Text description in log file
-------------------------------------------------

These tags are only recognised by Xloggraph and their use is deprecated.

::

    $KEYTEXT :text name: $$any text characters$$keyword_with_values$$
      any_characters        - are the descriptive text
      keyword_with_values   - is a keyworded format of data

6. Syntax of Summary description
--------------------------------

These tags are only recognised by Xloggraph and their use is deprecated.
Use the appropriate functions described in section 6 (`Delimiting
summary sections <libhtml.html#log_summary>`__) in the ``libhtml``
documentation, to write summary tags which are recognised by CCP4i.

::

    $SUMMARY :summary_name: $$ any characters $$
         :component_type:component_name: :component_type:component_name: ...$$
      component_type        - is `TEXT` either `KEYTEXT`
      component_name        - is its name

Due to the single-pass parsing all TEXTs and KEYTEXTs must be defined
BEFORE the SUMMARY.

7. SEE ALSO
-----------

`loggraph <loggraph.html>`__, `xloggraph <xloggraph.html>`__,
`libhtml <libhtml.html>`__
