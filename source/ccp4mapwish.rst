CCP4MAPWISH (CCP4: Supported Program)
=====================================

NAME
----

**ccp4mapwish** - customised version of ``wish`` with additional
commands to read CCP4 mapfiles and display contoured sections on a Tk
canvas widget

SYNOPSIS
--------

**ccp4mapwish** ?fileName?

DESCRIPTION
-----------

ccp4mapwish is a customised version of the **wish** program, consisting
of the Tcl command language, the Tk toolkit, the CCP4 MapSlicer Command
Library, and a main program that reads commands from standard input or
from a file. It creates a main window and then processes Tcl commands.

The MapSlicer Command Library can also exist as a loadable library
**libccp4map.so**, which can be imported into wish at run-time using the
Tcl load command.

The program and library are currently supported on a number of platforms
(Irix, OSF1, SunOS, Linux, Windows). Please contact CCP4 if you are
interested in using either on other platforms.

USAGE
-----

The MapSlicer command library used in ccpmapwish offers a number of
commands in addition to those found in standard Tcl/Tk:

`readmap <#readmap>`__, `deletemap <#deletemap>`__,
`mapinfo <#mapinfo>`__, `section <#section>`__

readmap ?mapid? ?mapfile?
~~~~~~~~~~~~~~~~~~~~~~~~~

Tcl command to load a map into memory from the specified file

*Arguments:*

::

       mapid    "handle" string identifying the map in memory
       mapfile  file to read from

*Returns:*

::

       0  = completed ok
       -1 = map already loaded
       -2 = error reading from map file
       -3 = couldn't allocate memory

mapinfo ?mapid ?keyword??
~~~~~~~~~~~~~~~~~~~~~~~~~

Tcl command to interrogate the map held in memory.

mapid is the handle identifying the map stored in memory; if no mapid is
given then all the currently stored mapids are returned as a list.

A maximum of one keyword is allowed:

::

       [none]     = returns 1 if the map is currently held in memory
       spacegroup = returns spacegroup number
       maximum    = returns maximum density in map
       minimum    = returns minimum density
       mean       = returns mean density
       rms        = returns rms density
       cell       = returns cell info as a list of 6 numbers
       limits     = return start...stop on x..y..z as a list of 6 numbers
       map        = writes out map...careful!
       title      = returns the title from the map header
       grid       = returns the number of grid points along x,y,z cell
                    edges as list of 3 numbers
       type       = returns "map", "mask" or "other" depending on the
                    mode of the original map file
       mode       = mode of the map file

*Returns:*

::

       -1 = no map loaded
       Otherwise returns requested info, see keywords.

deletemap mapid
~~~~~~~~~~~~~~~

Tcl command to release the memory holding the map and remove it from the
array of stored maps.

mapid is the handle identifying the map which is to be deleted.

*Returns:*

::

       -1 = no map currently stored
       0  = successful

section <name> option ?arg arg ...?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This implements the Tcl command ``section`` for manipulating CCP4
section structures in memory.

*<name>* is the handle id for a section structure in memory. *option*
and the *arg*\ s determine the exact behaviour of the command. The
following options are available:

`init <#section_init>`__, `configure <#section_configure>`__,
`contours <#section_contours>`__, `coords <#section_coords>`__,
`render <#section_render>`__, `delete <#section_delete>`__,
`exists <#section_exists>`__, `info <#section_info>`__

In addition the *init* and *configure* options can accept a number of
`extra arguments <#section_arguments>`__.

 **section <name> init <mapid> <axis> <section> ?<section2>
?<slab\_step>?? ?option value ...?**

Initialise a new section structure called <name>, with section number
<section> on axis <axis> from map <mapid>

If <section2> is also specified then the section is actually a slab of
sections from <section> to <section2>. An optional third number
specifies the slab\_step size between the two limits.

See `below <#section_arguments>`__ for details of additional optional
arguments.

 **section <name> configure ?option? ?value? ?option value ...?**

Change the settings in the existing section structure <name> If *option*
is specified with no value then the current setting is returned.

See `below <#section_arguments>`__ for details of additional optional
arguments.

|  **section <name> contours <type> range <min> <max> <interval> ?option
  ...?**
| **section <name> contours <type> list value ?value ...? ?option ...?**
| **section <name> contours**

Set or update the contour levels for section <name>

::

         <type> is one of abs, sigma or frac

         range: contour levels will be set starting from <min> in steps
                of <interval> up to <max>
         list:  contour levels as an explicit list of values

         Option can be one or more of:
         -negcontours: generate equivalent negative contour levels
         -meanoffset : generate contour levels starting from the mean
                       density in the map (default is from zero)

         Without any arguments, return the contours as a list of 
         absolute values.

 **section <name> coords <canvas> x y ?-precision value?**

Return the fractional coordinates in section <name> corresponding to the
canvas coordinates x,y as rendered on <canvas>

The optional -precision argument specifies the number of decimal places
to return.

 **section <name> render <canvas> ?arg arg ...?**

Draw contours on <canvas> with the settings in <name>

::

         -scale <scale> : sets the scaling to <scal><units>/Angstrom
         if no units are specified then defaults to pixels
         -greyscale : draw a greyscale backdrop (default is not to draw
         greyscale)
         -nocontours : don't display contour lines (default is to draw contours)

 **section <name> delete**

Delete the section structure <name> from memory

 **section <name> exists**

Check whether the section <name> currently exists

Returns 1 if section is registered, 0 otherwise

 **section <name> info option**

Inquiry about general properties of the section <name>

*option* can be one of:

::

     
             exists    : return 1 if <name> exists, 0 otherwise
             extent    : return limits of the section within the map
                     nb these may be different from those used in
                 the display (see config -limits)
         mapid     : return the id of the parent map

**Arguments**

The *init* and *configure* options can take a number of additional
optional arguments:

::

       -axis <axis>            : set axis from which to take section
       -bbox <boolean>         : set whether bounding box is drawn
       -grid <boolean>         : set whether grid lines are drawn
       -gridlabels <boolean>   : set whether grid labels are drawn
       -gridspacing <u> <v>    : set the spacing of gridlines on each axis
       -limits <x1> <x2> <y1> <y2> : set limits of the section in the display
                             If <x1>...<y2> are all > 1 then the limits
                     are taken to be in grid units; otherwise they
                     are taken as fractional units.
       -precision <ndp>        : set the number of d.p. for gridlabels
       -section <sec> ?<sec2> ?<step>?? : change section number or slab
                                 definition
       -labels <boolean>       : set whether axis labels are drawn
       -axislabels <xlabel> <ylabel> <zlabel> : set the labels displayed for the x y and z axis
       -view <orientation>     : set the viewing orientation to "lhs" or "rhs" (default is "rhs")

TECHNICAL DETAILS
-----------------

As of CCP4 5.0, ccp4mapwish has been updated to use the C library
functions for handling map files which simplifies the requirements of
the program substantially.

Building ccp4mapwish requires the following files and libraries:

#. tcl.h file for tcl version 8.0 or better
#. Tcl and Tk libraries (libtcl and libtk respectively) version 8.0 or
   better
#. X11 libraries
#. Maths library (libm)
#. CCP4 map library header file cmaplib.h
#. CCP4 library (libccp4) (though it only uses the libcmap module)

These should automatically be detected by configure upon installation of
the suite.

SEE ALSO
--------

`mapslicer <mapslicer.html>`__

EXAMPLES
--------

Example script to display a section:

::

    # First set up a canvas to display sections
    set canv1 [canvas .c]
    pack $canv1
    # Read in a map with name m1
    readmap m1 toxd.map
    # Initialise a section called s1 from the map
    section s1 init z 5
    # Initialise contour levels to display
    section s1 contours sigma range 1.0 5.0 0.5 -negcontours
    # Display the section in the canvas
    section s1 render $canv1 -scale 2mm

AUTHOR
------

Peter Briggs, CCP4.
