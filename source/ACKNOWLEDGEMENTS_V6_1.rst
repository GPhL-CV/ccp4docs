CCP4 v6.1 Acknowledgements
==========================

CCP4 relies on the contribution of programs and software from many
protein crystallographers. We would like to thank all contributing
software authors for their efforts and their generosity in allowing us
to distribute their code, thus making it available to a wide community
of users. Details of authors of individual programs are given in the
corresponding `program documentation <INDEX.html>`__.

A large number of people have also given their time and energy to test
this version of the CCP4, and to report and fix bugs. We would like to
acknowledge these people (in no particular order):

-  Kevin Cowtan, Liz Potterton (York)
-  Eleanor Dodson (York)
-  Phil Evans (MRC Cambridge)
-  Randy Read, Airlie McCoy (Cambridge)
-  Clemens Vonrhein, Claus Flensburg (Global Phasing)
-  Ralph Grosse-Kunstleve (Lawrence Berkeley Laboratory)
-  Raj Pannu, Pavol Skubak (Leiden)
-  Matsuura Takanori (Osaka)
-  Bill Scott (UCSC)
-  Serge Cohen (NKI/Soleil)
-  Paul Emsley (Oxford)
-  Pete Dunten (Stanford)

Finally, we would like to thank all those people who made contributions
which are not explicitly acknowledged here, which includes all program
authors who contributed new or updated versions of their software. CCP4
remains a collaborative effort and as such these releases would not
possible without your help and assistance.

**Thank you!**

--------------

*On behalf of the CCP4 group at STFC Daresbury Laboratory
Peter Briggs, Charles Ballard, Martyn Winn, Francois Remacle, Ronan
Keegan, Norman Stein, Graeme Winter, Wendy Yang and Maeri Howard
Octobber 2008*

--------------
