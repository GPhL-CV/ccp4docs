|CCP4 web logo|

Basic Maths for Protein Crystallographers

Symmetry operators

+------------------------------------------------+
| |next button| |previous button| |top button|   |
+------------------------------------------------+

Since crystals are repeating lattices with straight edges, the only
symmetry elements compatible with crystallinity are:

+--------------------------------------+--------------------------------------+
| +---------+------------------------- | +--------------+-------------------- |
| -------------------------+           | ------+                              |
| |         | rotation axis perpendicu | | screw axis   | with translation st |
| lar to a crystal plane   |           | eps   |                              |
| +=========+========================= | +==============+==================== |
| =========================+           | ======+                              |
| | 2fold   | |tick|                   | | |tick|       | 1/2                 |
|                          |           |       |                              |
| +---------+------------------------- | +--------------+-------------------- |
| -------------------------+           | ------+                              |
| | 3fold   | |tick|                   | | |tick|       | 1/3 or 2/3          |
|                          |           |       |                              |
| +---------+------------------------- | +--------------+-------------------- |
| -------------------------+           | ------+                              |
| | 4fold   | |tick|                   | | |tick|       | 1/4, 2/4, or 3/4    |
|                          |           |       |                              |
| +---------+------------------------- | +--------------+-------------------- |
| -------------------------+           | ------+                              |
| | 5fold   | **×**                    | | **×**        | **×**               |
|                          |           |       |                              |
| +---------+------------------------- | +--------------+-------------------- |
| -------------------------+           | ------+                              |
| | 6fold   | |tick|                   | | |tick|       | 1/6,2/6,3/6,4/6,or  |
|                          |           | 5/6   |                              |
| +---------+------------------------- | +--------------+-------------------- |
| -------------------------+           | ------+                              |
+--------------------------------------+--------------------------------------+

| Symmetry-equivalent atoms are tabulated in the International Tables
  like this:
| (x,y,z), (-y, x-y, z+1/3), (y-x, -x, z+2/3) (this is for spacegroup
  P3\ :sub:`1`). But it is useful to think of symmetry operators as 3x4
  or 4x4 matrices and generate symmetry related atoms thus. Then the
  symmetry-related coordinates in P3\ :sub:`1` are |symmetry matrix
  notation|,    *i.e.*    |P31 symmetry operators|.

| If the [S:sub:`i`] is augmented to [S':sub:`i`], a 4x4 matrix, by the
  addition of a fourth row, [0 0 0 1], the augmented symmetry operators
  make a closed group, *i.e.* for some i,j and k:
| [S':sub:`i`][S'\ :sub:`j`]=[S'\ :sub:`k`]and
  [S':sub:`i`]\ :sup:`-1`\ =[S'\ :sub:`k`].

| 

--------------

.. |CCP4 web logo| image:: ../images/weblogo175.gif
   :width: 175px
   :height: 69px
.. |next button| image:: ../images/3Dnexttr.gif
   :width: 100px
   :height: 31px
   :target: bmg5.html
.. |previous button| image:: ../images/3Dprevtr.gif
   :width: 100px
   :height: 31px
   :target: bmg3.html
.. |top button| image:: ../images/3Dtoptr.gif
   :width: 100px
   :height: 31px
   :target: index.html
.. |tick| image:: ../images/ticktr.gif
   :width: 21px
   :height: 32px
.. |symmetry matrix notation| image:: ../images/si-matrixtr.gif
   :width: 59px
   :height: 70px
.. |P31 symmetry operators| image:: ../images/p31matricestr.gif
   :width: 408px
   :height: 72px
