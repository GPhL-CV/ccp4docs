CCPLIB (CCP4: Library)
======================

NAME
----

**ccplib** - CCP4 utility subroutine library

DESCRIPTION
-----------

**ccplib** contains various low-level utility routines which are
potentially machine-dependent. Since CCP4 version 5.0, the original
``ccplib.f`` has been split amongst several files. The most commonly
used routines have been re-implemented in C, with a Fortran interface,
while other routines remain in ``ccplib.f``. This document describes the
routines as seen from a Fortran programmer's perspective. For
information on the C functions, see `C
libraries <C_library/index.html>`__ and `CCP4 Library
Utilities <C_library/utilities_page.html>`__.

Contents of this file
---------------------

-  `General information <#general>`__

   -  `Using the pre-processor in programs <#pre_processor>`__
   -  `Initialisation <#initialisation>`__
   -  `Program termination <#termination>`__
   -  `File opening <#file_opening>`__
   -  `Version printing <#version>`__
   -  `Skeleton example CCP4 program <#skeleton>`__

-  `List of subroutines <#list_of_subroutines>`__
-  `Description of the subroutines <#description>`__

General information
-------------------

Using the pre-processor in programs
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To use the pre-processor in existing code you will need to add a call to
the routine `CCPFYP <#ccpfyp>`__ as the first executable statement in
the program. This routine performs all the pre-processing and should be
called before any other CCP4 library routines.

Initialisation
~~~~~~~~~~~~~~

Fortran programs should call the routine `CCPFYP <#ccpfyp>`__ initially
(see "USING THE PRE-PROCESSOR"), allowing one to run them using the
mechanism

::

            <program> hklin <file> hklout <file> ...

Program Termination
~~~~~~~~~~~~~~~~~~~

There should be no ``STOP`` statements within the code; rather, call the
subroutine `CCPERR <#ccperr>`__. This has the form

::

            CALL CCPERR( <num> '<string>')

where ``<num>`` is an exit value (0 for normal termination) and the
``<string>`` is a suitable message. Programs should terminate
successfully with the call

::

            CALL CCPERR(0, 'Normal termination')

To terminate a program because an error has been detected use

::

            CALL CCPERR(1, '<string>')

where <string> should contain any useful information about why the
program is being terminated abnormally.

CCPERR can also be used to send 'warnings' (CCPERR level 2) and messages
(CCPERR level 3).

File opening
~~~~~~~~~~~~

There should be no ``OPEN`` statements in Fortran code. Instead call
`CCPDPN <#ccpdpn>`__ for general files, and appropriate library routines
for the standard data formats (MTZ, PDB, CCIF, CCP4 maps etc).

Use the normal Fortran ``CLOSE`` to close files opened with CCPDPN. The
standard data formats have specific library routines for closing files
which should be used in preference to ``CLOSE`` in those cases.

Version printing
~~~~~~~~~~~~~~~~

The version of the CCP4 library being used can be queried using the
`CCP4\_VERSION <#ccp4_version>`__ routine. The program version number
may be different, and can be set (and later retrieved) using
`CCP4\_PROG\_VERSION <#ccp4_prog_version>`__. A call to
`CCPVRS <#ccpvrs>`__ will output a banner including this version
information. The command line switch -i can also be used to get program
and version information.

Skeleton example CCP4 program
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

          PROGRAM <name>
          <declarations>
    C define the program version
          CALL CCP4_PROG_VERSION('7.2',0)
    C call the pre-processor:
          CALL CCPFYP
    C advertise the version:
          CALL CCPRCS(LUNSTO(1), '<name>', '$Date$')
    C open an input file:
          CALL CCPDPN(IN1, 'INFILE1','READONLY','F','F')
          .
          .
          .
    C close the file
          CLOSE(IN1)
          CALL CCPERR(0, 'Normal termination')
          END

List of subroutines
-------------------

+--------------------------------------+--------------------------------------+
| Routine                              | Description                          |
+======================================+======================================+
| `CCFILL <#ccfill>`__                 |                                      |
| Set specified number of elements of  |                                      |
| byte array                           |                                      |
+--------------------------------------+--------------------------------------+
| `CCP4\_VERSION <#ccp4_version>`__    |                                      |
| Return current CCP4 library version  |                                      |
| as string                            |                                      |
+--------------------------------------+--------------------------------------+
| `CCP4\_PROG\_VERSION <#ccp4_prog_ver |                                      |
| sion>`__                             |                                      |
| Set or retrieve program version as   |                                      |
| string                               |                                      |
+--------------------------------------+--------------------------------------+
| `CCPALC <#ccpalc>`__                 |                                      |
| Call subroutine with allocated       |                                      |
| memory                               |                                      |
+--------------------------------------+--------------------------------------+
| `CCPALE <#ccpale>`__                 |                                      |
| Call subroutine with allocated       |                                      |
| memory set from environment          |                                      |
+--------------------------------------+--------------------------------------+
| `CCPBYI <#ccpbyi>`__                 |                                      |
| Copy array of unsigned (or signed)   |                                      |
| bytes into integer array             |                                      |
+--------------------------------------+--------------------------------------+
| `CCPBYT <#ccpbyt>`__                 |                                      |
| Indicate whether byte handling is    |                                      |
| available                            |                                      |
+--------------------------------------+--------------------------------------+
| `CCPCPI <#ccpcpi>`__                 |                                      |
| Copy array of BYTE or INTEGER\*2     |                                      |
| elements into integer array          |                                      |
+--------------------------------------+--------------------------------------+
| `CCPDAT <#ccpdat>`__                 |                                      |
| Get calendar date                    |                                      |
+--------------------------------------+--------------------------------------+
| `CCPDEX <#ccpdex>`__                 |                                      |
| Periodicity reduction of 1024 (for   |                                      |
| PROLSQ)                              |                                      |
+--------------------------------------+--------------------------------------+
| `CCPDPN <#ccpdpn>`__                 |                                      |
| more friendly CCPOPN                 |                                      |
+--------------------------------------+--------------------------------------+
| `CCPE2I <#ccpe2i>`__                 |                                      |
| read integer from logical name value |                                      |
+--------------------------------------+--------------------------------------+
| `CCPERR <#ccperr>`__                 |                                      |
| Report error or normal termination   |                                      |
| and stop                             |                                      |
+--------------------------------------+--------------------------------------+
| `CCPEXS <#ccpexs>`__                 |                                      |
| test if file exists                  |                                      |
+--------------------------------------+--------------------------------------+
| `CCPFYP <#ccpfyp>`__                 |                                      |
| Set up environment & parse command   |                                      |
| line arguments                       |                                      |
+--------------------------------------+--------------------------------------+
| `CCPGI2 <#ccpgi2>`__                 |                                      |
| Get unsigned integer\*2 value from 0 |                                      |
| to 65535 from N'th unsigned          |                                      |
| integer\*2 element of array.         |                                      |
+--------------------------------------+--------------------------------------+
| `CCPGTB <#ccpgtb>`__                 |                                      |
| Get unsigned byte value from 0 to    |                                      |
| 255 from N'th byte of array.         |                                      |
+--------------------------------------+--------------------------------------+
| `CCPI2I <#ccpi2i>`__                 |                                      |
| Copy an array of INTEGER\*2 elements |                                      |
| into an integer array                |                                      |
+--------------------------------------+--------------------------------------+
| `CCPIBY <#ccpiby>`__                 |                                      |
| Copy array of integers into array of |                                      |
| bytes.                               |                                      |
+--------------------------------------+--------------------------------------+
| `CCPII2 <#ccpii2>`__                 |                                      |
| Copy array of integers into array of |                                      |
| INTEGER\*2 elements.                 |                                      |
+--------------------------------------+--------------------------------------+
| `CCPLWC <#ccplwc>`__                 |                                      |
| Convert a string to lower case       |                                      |
+--------------------------------------+--------------------------------------+
| `CCPMDE <#ccpmde>`__                 |                                      |
| If byte handling available return    |                                      |
| nos. of bytes for map modes          |                                      |
+--------------------------------------+--------------------------------------+
| `CCPMVB <#ccpmvb>`__                 |                                      |
| Move bytes from one non-character    |                                      |
| array to another if byte handling is |                                      |
| available                            |                                      |
+--------------------------------------+--------------------------------------+
| `CCPMVI <#ccpmvi>`__                 |                                      |
| Move words from one integer array to |                                      |
| another                              |                                      |
+--------------------------------------+--------------------------------------+
| `CCPMVR <#ccpmvr>`__                 |                                      |
| Move words from one real array to    |                                      |
| another                              |                                      |
+--------------------------------------+--------------------------------------+
| `CCPNUN <#ccpnun>`__                 |                                      |
| Return an unconnected i/o unit       |                                      |
| number                               |                                      |
+--------------------------------------+--------------------------------------+
| `CCPONL <#ccponl>`__                 |                                      |
| See if program is being run          |                                      |
| interactively                        |                                      |
+--------------------------------------+--------------------------------------+
| `CCPPSF <#ccppsf>`__                 |                                      |
| Parse file name into components      |                                      |
+--------------------------------------+--------------------------------------+
| `CCPRCS <#ccprcs>`__                 |                                      |
| Like CCPVRS but use RCS-format date  |                                      |
| string                               |                                      |
+--------------------------------------+--------------------------------------+
| `CSETNV <#csetnv>`__                 |                                      |
| Associate logical name with file     |                                      |
| name                                 |                                      |
+--------------------------------------+--------------------------------------+
| `CCPPAG <#ccppag>`__                 |                                      |
| Set paging parameters if available   |                                      |
+--------------------------------------+--------------------------------------+
| `CCPSI2 <#ccpsi2>`__                 |                                      |
| Set integer value from 0 to 65535    |                                      |
| into the N'th unsigned integer\*2    |                                      |
| element of an array.                 |                                      |
+--------------------------------------+--------------------------------------+
| `CCPSTB <#ccpstb>`__                 |                                      |
| Set integer value from 0 to 255 into |                                      |
| N'th byte of array.                  |                                      |
+--------------------------------------+--------------------------------------+
| `CCPSUM <#ccpsum>`__                 |                                      |
| Sum the elements of an array         |                                      |
+--------------------------------------+--------------------------------------+
| `CCPTIM <#ccptim>`__                 |                                      |
| Get CPU and Elapsed times            |                                      |
+--------------------------------------+--------------------------------------+
| `CCPTOI <#ccpto1>`__                 |                                      |
| Convert n'th byte or I\*2 in a       |                                      |
| non-character array to an integer    |                                      |
+--------------------------------------+--------------------------------------+
| `CCPUFL <#ccpufl>`__                 |                                      |
| Suppress underflow messages          |                                      |
+--------------------------------------+--------------------------------------+
| `CCPUPC <#ccpupc>`__                 |                                      |
| Convert a string to upper case       |                                      |
+--------------------------------------+--------------------------------------+
| `CCPVRS <#ccpvrs>`__                 |                                      |
| Print program version number and     |                                      |
| date header                          |                                      |
+--------------------------------------+--------------------------------------+
| `CCPPNM <#ccppnm>`__                 |                                      |
| Entry: Return the program name       |                                      |
| previously set by CCPVRS             |                                      |
+--------------------------------------+--------------------------------------+
| `CCPZBI <#ccpzbi>`__                 |                                      |
| Sets an array of bytes to zero       |                                      |
+--------------------------------------+--------------------------------------+
| `CCPZI <#ccpzi>`__                   |                                      |
| Set 'n' words of an integer array to |                                      |
| zero using a simple loop             |                                      |
+--------------------------------------+--------------------------------------+
| `CCPZR <#ccpzr>`__                   |                                      |
| Set 'n' words of a real array to     |                                      |
| zero using a simple loop             |                                      |
+--------------------------------------+--------------------------------------+
| `FDIR <#fdir>`__                     |                                      |
| Returns the directory part of a file |                                      |
| name                                 |                                      |
+--------------------------------------+--------------------------------------+
| `FEXTN <#fextn>`__                   |                                      |
| Returns the extension of a file name |                                      |
+--------------------------------------+--------------------------------------+
| `FROOT <#froot>`__                   |                                      |
| Returns the root of a file name      |                                      |
+--------------------------------------+--------------------------------------+
| `LITEND <#litend>`__                 |                                      |
| determine endianness                 |                                      |
+--------------------------------------+--------------------------------------+
| `LENSTR <#lenstr>`__                 |                                      |
| length of string to last non-space   |                                      |
+--------------------------------------+--------------------------------------+
| `LUNSTI <#lunsti>`__                 |                                      |
| Get logical unit number for input    |                                      |
+--------------------------------------+--------------------------------------+
| `LUNSTO <#lunsto>`__                 |                                      |
| Get logical unit number for output   |                                      |
+--------------------------------------+--------------------------------------+
| `NBITST <#nbitst>`__                 |                                      |
| Return the (unsigned) integer value  |                                      |
| held within a bit field in a word    |                                      |
+--------------------------------------+--------------------------------------+
| `NOCRLF <#nocrlf>`__                 |                                      |
| write line suppressing cr/lf to      |                                      |
| standard output                      |                                      |
+--------------------------------------+--------------------------------------+
| `QPRINT <#qprint>`__                 |                                      |
| write debug messages                 |                                      |
+--------------------------------------+--------------------------------------+
| `QPRLVL <#qprlvl>`__                 |                                      |
| Entry: Return debug level used by    |                                      |
| QPRINT.                              |                                      |
+--------------------------------------+--------------------------------------+
| `STBITS <#stbits>`__                 |                                      |
| Set a bit field within a word to a   |                                      |
| given (unsigned) integer value       |                                      |
+--------------------------------------+--------------------------------------+
| `CCPLIC <#ccplic>`__                 |                                      |
| Check that license conditions have   |                                      |
| been agreed                          |                                      |
+--------------------------------------+--------------------------------------+

Descriptions of the Subroutines
-------------------------------

\ **SUBROUTINE CCFILL(ARR1,SCAL,NTIMES)**
    Set specified number of elements of byte array

    +--------------------------------------------------------------------------+
    | ARR1                                                                     |
    | (o)                                                                      |
    | (byte)                                                                   |
    | array to which bytes are to be copied                                    |
    +--------------------------------------------------------------------------+
    | SCAL                                                                     |
    | (i)                                                                      |
    | (byte)                                                                   |
    | byte value to be copied into ARR1                                        |
    +--------------------------------------------------------------------------+
    | NTIMES                                                                   |
    | (i)                                                                      |
    | (integer)                                                                |
    | the number of bytes to be copied                                         |
    +--------------------------------------------------------------------------+

\ **SUBROUTINE CCP4\_VERSION(VERSION)**
    Return current CCP4 version as string
    +--------------------------------------------------------------------------+
    | VERSION                                                                  |
    | (o)                                                                      |
    | (character\*(\*))                                                        |
    | current version of CCP4 suite                                            |
    +--------------------------------------------------------------------------+

\ **SUBROUTINE CCP4\_PROG\_VERSION(VERSION,IFLAG)**
    Set or retrieve program version as string
    +--------------------------------------------------------------------------+
    | VERSION                                                                  |
    | (i/o)                                                                    |
    | (character\*(\*))                                                        |
    | version of individual program                                            |
    +--------------------------------------------------------------------------+
    | IFLAG                                                                    |
    | (i)                                                                      |
    | (integer)                                                                |
    | if 0 then set version using VERSION argument, else if 1 then return      |
    | version in VERSION argument                                              |
    +--------------------------------------------------------------------------+

\ **SUBROUTINE CCPALC(ROUTNE, N, TYPE, LENGTH)**
    Arrange to call subroutine ROUTNE with N array arguments each of
    length LENGTH (i) and type indicated by TYPE (i): 'i' == integer,
    'r' == real, 'd' == double precision, 'c'==complex. TYPE elements
    may have either case. The calling of ROUTNE and allocation are
    performed by the machine dependant routine CCPAL1 (UNIX
    `library.c <./library.html>`__, VMS
    `vms.for <./unix.html#CCPAL1>`__, MVS
    `w32mvs.f <./unix.html#CCPAL1>`__). Consider

    ::

                call ccpalc (fred, 3, types, lens)

    with types = (/'i', 'r', 'c'/) and lens = (/1000, 2000, 3000/). This
    effectively does

    ::

               call fred (1000, arr1, 2000, arr2, 3000, arr3)

    with

    ::

                subroutine fred (n1, foo, n2, bar, n3, baz)
                       integer n1, n2, n3, foo (n1)
                       real bar (n2)
                       complex baz (n3)
                       ...

    Obviously all communication with ROUTNE must be by COMMON (or,
    possibly, extra ENTRYs). The allocated memory is freed on return
    from ROUTNE. As a concession, it's initially filled with zeroed
    bytes.

    +--------------------------------------------------------------------------+
    | ROUTNE                                                                   |
    | (i)                                                                      |
    | (external)                                                               |
    | routine to call                                                          |
    +--------------------------------------------------------------------------+
    | N                                                                        |
    | (i)                                                                      |
    | (integer)                                                                |
    | number of arguments to ROUTNE (<=12)                                     |
    +--------------------------------------------------------------------------+
    | TYPE                                                                     |
    | (i)                                                                      |
    | (character\*1 (\*))                                                      |
    | type of arguments to ROUTNE:                                             |
    | 'I': INTEGER                                                             |
    | 'R': REAL                                                                |
    | 'D': DOUBLE PRECISION                                                    |
    | 'C': COMPLEX                                                             |
    +--------------------------------------------------------------------------+
    | LENGTH                                                                   |
    | (i)                                                                      |
    | (integer\*(\*))                                                          |
    | number of elements in each (array) argument of ROUTNE                    |
    +--------------------------------------------------------------------------+

\ **SUBROUTINE CCPALE(ROUTNE, N, TYPE, LENGTH, LENDEF)**
    Arrange to call subroutine ROUTNE with N array arguments each of
    length LENGTH (i) and type indicated by TYPE (i): 'i' == integer,
    'r' == real, 'd' == double precision, 'c'==complex. TYPE elements
    may have either case. LENGTH points to an array of environment
    variable (logical) names from which integer values are read,
    `CCPE2I <#ccpe2i>`__. The lengths default to values from LENDEF.
    This is a convenient interface to `CCPALC <#ccpalc>`__ to allow
    configuring of the memory requirements on the command line where
    appropriate. This may be useful if the memory requirements can't be
    determined initially and it's necessary to guess.

    +--------------------------------------------------------------------------+
    | ROUTNE                                                                   |
    | (i)                                                                      |
    | (external)                                                               |
    | routine to call                                                          |
    +--------------------------------------------------------------------------+
    | N                                                                        |
    | (i)                                                                      |
    | (integer)                                                                |
    | number of arguments to ROUTNE (<=12)                                     |
    +--------------------------------------------------------------------------+
    | TYPE                                                                     |
    | (i)                                                                      |
    | (character\*1 (\*))                                                      |
    | type of arguments to ROUTNE:                                             |
    | 'I': INTEGER                                                             |
    | 'R': REAL                                                                |
    | 'D': DOUBLE PRECISION                                                    |
    | 'C': COMPLEX                                                             |
    +--------------------------------------------------------------------------+
    | LENGTH                                                                   |
    | (i)                                                                      |
    | (character\*(\*))                                                        |
    | logical names representing the number of elements in each (array)        |
    | argument of ROUTNE                                                       |
    +--------------------------------------------------------------------------+
    | LENDEF                                                                   |
    | (i)                                                                      |
    | (integer (\*))                                                           |
    | default lengths for the argument arrays used if the appropriate LENGTH   |
    | argument doesn't represent a defined logical                             |
    +--------------------------------------------------------------------------+
    | PRINT                                                                    |
    | (i)                                                                      |
    | (logical)                                                                |
    | whether or not to print the values of the array lengths                  |
    +--------------------------------------------------------------------------+

\ **SUBROUTINE CCPBYI(IA,IBYT,NB)**
    | Copy array of unsigned (or signed) bytes into integer array
    | To be implemented if `CCPBYT <#ccpbyt>`__ == .TRUE. [for LAUE
      program?].

    +--------------------------------------------------------------------------+
    | IA                                                                       |
    | (o)                                                                      |
    | (integer)                                                                |
    | array to return integer values                                           |
    +--------------------------------------------------------------------------+
    | IBYT                                                                     |
    | (i)                                                                      |
    | (byte)                                                                   |
    | array holding byte data (may be an integer array, for example, with data |
    | packed into adjacent bytes).                                             |
    +--------------------------------------------------------------------------+
    | NB                                                                       |
    | (i)                                                                      |
    | (integer)                                                                |
    | if >0, the number of unsigned bytes to be copied                         |
    | if <0, -the number of signed bytes to be copied                          |
    +--------------------------------------------------------------------------+

\ **LOGICAL FUNCTION CCPBYT(NBW)**
    | Indicate whether byte handling is available. Used in
      `maplib <./maplib.html>`__.
    | NOTE: is assumed to be available (.TRUE., NBW=4).

    NBW (o) (integer)

    returns the number of bytes per word or a value of 1 if no byte
    handling is available (4).

    +--------------------------------------------------------------------------+
    | Returns CCPBYT                                                           |
    | == .TRUE.                                                                |
    | byte handling and associated ccplib routines available (assumed).        |
    +--------------------------------------------------------------------------+
    | == .FALSE.                                                               |
    | no byte handling available.                                              |
    +--------------------------------------------------------------------------+

\ **SUBROUTINE CCPCPI(IA,IB,MINEL,MAXEL,ITYP)**
    Copy an array of BYTE or INTEGER\*2 elements into an integer array.
    NB is bytewise operation
    To be implemented if `CCPBYT <#ccpbyt>`__ function returns .TRUE.
    [for LAUE program?]
    +--------------------------------------------------------------------------+
    | IA                                                                       |
    | (o)                                                                      |
    | (integer (\*))                                                           |
    | to return values                                                         |
    +--------------------------------------------------------------------------+
    | IB                                                                       |
    | (i)                                                                      |
    | (integer (\*))                                                           |
    | holding data with data packed into adjacent BYTE or INTEGER\*2 elements  |
    | (in routine is byte)                                                     |
    +--------------------------------------------------------------------------+
    | MINEL                                                                    |
    | (i)                                                                      |
    | (integer)                                                                |
    | Minimum element to copy                                                  |
    +--------------------------------------------------------------------------+
    | MAXEL                                                                    |
    | (i)                                                                      |
    | (integer)                                                                |
    | Maximum element to copy                                                  |
    +--------------------------------------------------------------------------+
    | ITYP                                                                     |
    | (i)                                                                      |
    | (integer)                                                                |
    | Type =1 unsigned byte                                                    |
    | =2 signed byte                                                           |
    | =3 unsigned two byte integer                                             |
    | =4 signed two byte integer                                               |
    +--------------------------------------------------------------------------+

    Note: if MINEL > MAXEL elements will be copied in reverse order
\ **SUBROUTINE CCPDAT(CALDAT)**
    Get calendar date
    +--------------------------------------------------------------------------+
    | CALDAT                                                                   |
    | (o)                                                                      |
    | (character\*8)                                                           |
    | variable returning date as dd/mm/yy (returned as a blank string if not   |
    | available).                                                              |
    +--------------------------------------------------------------------------+

\ **SUBROUTINE CCPDEX(INDX,N)**
    Periodicity reduction of 1024 [for PROLSQ]
    +--------------------------------------------------------------------------+
    | INDX                                                                     |
    | (i/o)                                                                    |
    | (integer)                                                                |
    | array holding numbers for periodicity reduction                          |
    +--------------------------------------------------------------------------+
    | N                                                                        |
    | (i)                                                                      |
    | (integer)                                                                |
    | no. of elements in INDX                                                  |
    +--------------------------------------------------------------------------+

\ **SUBROUTINE CCPDPN(IUN,LOGNAM,STATUS,TYPE,LREC,IFAIL)**
    Calls `CCPOPN <./unix.html#ccpopn>`__ to open a file, but with
    mnemonic arguments.
    +--------------------------------------------------------------------------+
    | IUN                                                                      |
    | (i) (integer)                                                            |
    | unit number                                                              |
    +--------------------------------------------------------------------------+
    | LOGNAM                                                                   |
    | (i) (character\*(\*))                                                    |
    | logical file name                                                        |
    +--------------------------------------------------------------------------+
    | STATUS                                                                   |
    | (i) (character\*(\*))                                                    |
    | file status flag                                                         |
    | 'UNKNOWN'                                                                |
    | 'SCRATCH'                                                                |
    | 'OLD'                                                                    |
    | 'NEW'                                                                    |
    | 'READONLY'                                                               |
    | 'PRINTER'                                                                |
    +--------------------------------------------------------------------------+
    | TYPE                                                                     |
    | (i) (character\*(\*))                                                    |
    | file type flag                                                           |
    | ='F', 'SEQUENTIAL' 'FORMATTED'                                           |
    | ='U', 'SEQUENTIAL' 'UNFORMATTED'                                         |
    | ='DF', 'DIRECT' 'FORMATTED'                                              |
    | ='DU', 'DIRECT' 'UNFORMATTED'                                            |
    +--------------------------------------------------------------------------+
    | LREC                                                                     |
    | (i) (integer)                                                            |
    | record length for direct access file (no. of characters for a formatted  |
    | file or words for an unformatted file).                                  |
    | Not relevant for a sequential file.                                      |
    +--------------------------------------------------------------------------+
    | IFAIL                                                                    |
    | (i/o) (integer)                                                          |
    | on input                                                                 |
    | =0, stop on open failure                                                 |
    | =1, continue after open failure (only on file not found)                 |
    | on output                                                                |
    | unchanged if file open OK                                                |
    | =-1, error in opening file                                               |
    +--------------------------------------------------------------------------+

\ **INTEGER FUNCTION CCPE2I (NAME, DEFVAL)**
    Return an integer extracted from environment variable NAME. If NAME
    isn't defined, use DEFVAL as the default. If the value of NAME isn't
    a representation of an integer, abort.
    +--------------------------------------------------------------------------+
    | NAME                                                                     |
    | (I)                                                                      |
    | (character \*(\*))                                                       |
    +--------------------------------------------------------------------------+
    | DEFVAL                                                                   |
    | (I)                                                                      |
    | (integer)                                                                |
    +--------------------------------------------------------------------------+

\ **SUBROUTINE CCPERR(ISTAT,ERRSTR)**
    Report error or normal termination and stop. Also reports latest
    system error iff ISTAT<0.
    +--------------------------------------------------------------------------+
    | ISTAT                                                                    |
    | (I)                                                                      |
    | (integer)                                                                |
    | exit status (0 for normal termination, 1 for fatal error)                |
    +--------------------------------------------------------------------------+
    | ERRST                                                                    |
    | (I)                                                                      |
    | (character)\*(\*)                                                        |
    | message                                                                  |
    +--------------------------------------------------------------------------+

\ **LOGICAL FUNCTION CCPEXS(NAME)**
    Test if file or file assigned to logical name exists
    +--------------------------------------------------------------------------+
    | NAME                                                                     |
    | (i) (character\*(\*))                                                    |
    | file name/logical name                                                   |
    +--------------------------------------------------------------------------+

    +--------------------------------------------------------------------------+
    | Returns CCPEXS ==                                                        |
    | .TRUE. if file exists                                                    |
    +--------------------------------------------------------------------------+
    | .FALSE. if file does not exist                                           |
    +--------------------------------------------------------------------------+

\ **SUBROUTINE CCPFYP**
    Set up the environment in which the program runs (environ.def,
    default.def locations, etc) and then parses the command line
    arguments.

    Note: A call to CCPFYP should be the first executable statement in
    ccp4 programs.

\ **SUBROUTINE CCPGI2(IVAL,IA,N)**
    | Get unsigned integer\*2 value from 0 to 65535 from N'th unsigned
      integer\*2 element of array.
    | To be implemented if `CCPBYT <#ccpbyt>`__ == .TRUE. Used in
      density modification routines.

    +--------------------------------------------------------------------------+
    | IVAL                                                                     |
    | (o)                                                                      |
    | (integer)                                                                |
    | returned integer values from 0 to 65535                                  |
    +--------------------------------------------------------------------------+
    | IA                                                                       |
    | (i)                                                                      |
    | (integer\*2(\*))                                                         |
    | array from which the unsigned INTEGER\*2 value is to be retrieved        |
    +--------------------------------------------------------------------------+
    | N                                                                        |
    | (i)                                                                      |
    | (integer)                                                                |
    | position in 'IA' where the unsigned INTEGER\*2 value is to be retrieved  |
    +--------------------------------------------------------------------------+

\ **SUBROUTINE CCPGTB(IVAL,IA,N)**
    | Get unsigned byte value from 0 to 255 from N'th byte of array.
    | To be implemented if `CCPBYT <#ccpbyt>`__ == .TRUE. Used in
      density modification routines.

    +--------------------------------------------------------------------------+
    | IVAL                                                                     |
    | (o)                                                                      |
    | (integer)                                                                |
    | the returned integer value from 0 to 255                                 |
    +--------------------------------------------------------------------------+
    | IA                                                                       |
    | (i)                                                                      |
    | (byte(\*))                                                               |
    | array from which the byte value is to be retrieved                       |
    +--------------------------------------------------------------------------+
    | N                                                                        |
    | (i)                                                                      |
    | (integer)                                                                |
    | position in IA where the byte value is to be retrieved                   |
    +--------------------------------------------------------------------------+

\ **SUBROUTINE CCPI2I(IA,I2,NE,SIGNED,SWAPB)**
    | Copy an array of INTEGER\*2 elements into an integer array.
    | To be implemented if `CCPBYT <#ccpbyt>`__ == .TRUE. [for LAUE
      program?]

    +--------------------------------------------------------------------------+
    | IA                                                                       |
    | (o)                                                                      |
    | (integer(\*))                                                            |
    | Array to return INTEGER values                                           |
    +--------------------------------------------------------------------------+
    | I2                                                                       |
    | (i)                                                                      |
    | (integer\*2)                                                             |
    | Array holding INTEGER\*2 data (may be an INTEGER array for example with  |
    | data packed into adjacent INTEGER\*2 elements).                          |
    +--------------------------------------------------------------------------+
    | NE                                                                       |
    | (i)                                                                      |
    | (integer)                                                                |
    | The number of elements to be copied                                      |
    +--------------------------------------------------------------------------+
    | SIGNED                                                                   |
    | (i)                                                                      |
    | (logical)                                                                |
    | ==.TRUE. Copy as signed INTEGER\*2 values                                |
    | ==.FALSE. Copy as unsigned INTEGER\*2 values                             |
    +--------------------------------------------------------------------------+
    | SWAPB                                                                    |
    | (i)                                                                      |
    | (logical)                                                                |
    | ==.TRUE. Swap bytes in the INTEGER\*2 elements                           |
    | ==.FALSE. Do not swap bytes                                              |
    +--------------------------------------------------------------------------+

\ **SUBROUTINE CCPIBY(IBYT,IA,NB)**
    Copy array of integers into array of bytes. NOTE: No overflow
    checking is done (least significant figures only stored).
    To must be implemented if `CCPBYT <#ccpbyt>`__ == .TRUE. [for LAUE
    program?]
    +--------------------------------------------------------------------------+
    | IBYT                                                                     |
    | (o)                                                                      |
    | (byte(\*))                                                               |
    | Array returning byte data (may be an integer array for example with data |
    | packed into adjacent bytes).                                             |
    +--------------------------------------------------------------------------+
    | IA                                                                       |
    | (i)                                                                      |
    | (integer(\*))                                                            |
    | array holding integer values                                             |
    +--------------------------------------------------------------------------+
    | NB                                                                       |
    | (i)                                                                      |
    | (integer)                                                                |
    | if >0, the number of elements to be copied to unsigned bytes             |
    | if <0, -the number of elements to be copied to signed bytes              |
    +--------------------------------------------------------------------------+

\ **SUBROUTINE CCPII2(I2,IA,NE,SIGNED,SWAPB)**
    Copy array of integers into array of INTEGER\*2 elements. NOTE: No
    overflow checking is done.
    To be implemented if `CCPBYT <#ccpbyt>`__ == .TRUE.) [for LAUE
    program?]
    +--------------------------------------------------------------------------+
    | I2                                                                       |
    | (o)                                                                      |
    | (integer\*2)                                                             |
    | Array returning INTEGER\*2 data (may be an INTEGER array for example     |
    | with data packed into adjacent INTEGER\*2 elements                       |
    +--------------------------------------------------------------------------+
    | IA                                                                       |
    | (i)                                                                      |
    | (integer(\*))                                                            |
    | Array holding INTEGER values                                             |
    +--------------------------------------------------------------------------+
    | NE                                                                       |
    | (i)                                                                      |
    | (integer)                                                                |
    | The number of elements to be copied                                      |
    +--------------------------------------------------------------------------+
    | SIGNED                                                                   |
    | (i)                                                                      |
    | (logical)                                                                |
    | ==.TRUE. Copy as signed integer\*2 values                                |
    | ==.FALSE. Copy as unsigned integer\*2 values                             |
    +--------------------------------------------------------------------------+
    | SWAPB                                                                    |
    | (i)                                                                      |
    | (logical)                                                                |
    | ==.TRUE. Swap bytes in the integer\*2 elements                           |
    | ==.FALSE. Do not swap bytes                                              |
    +--------------------------------------------------------------------------+

\ **SUBROUTINE CCPLWC(STRING)**
    Convert a text string to lower case in situ

    +--------------------------------------------------------------------------+
    | STRING                                                                   |
    | (i/o)                                                                    |
    | (character\*(\*))                                                        |
    +--------------------------------------------------------------------------+

\ **SUBROUTINE CCPMDE(MODE,NBYT)**
    | If byte handling is available then this subroutine returns the
      number of bytes per data item for the different modes used, in
      particular, in the map handling subroutines. [is routine actually
      called? no reference in map routines]
    | NOTE. byte handling is assumed to be available with 4 byte/word,
      see `CCPBYT <#ccpbyt>`__.

    +--------------------------------------------------------------------------+
    | MODE                                                                     |
    | (i)                                                                      |
    | (integer)                                                                |
    | = 0, bytes                                                               |
    | = 1, short (2 byte) integers                                             |
    | = 2, real/integer (single word)                                          |
    | = 3, short complex (2 \* 2 byte integers)                                |
    | = 4, complex (two words)                                                 |
    +--------------------------------------------------------------------------+
    | NBYT                                                                     |
    | (o) (                                                                    |
    | integer)                                                                 |
    | >0,                                                                      |
    | CCPBYT == .TRUE. number of bytes infor the MODE                          |
    | CCPBYT == .FALSE. number of words                                        |
    | = 0, no value available for this mode                                    |
    | = -1, invalid mode                                                       |
    +--------------------------------------------------------------------------+

    +--------------------------------------------------------------------------+
    | Typical values:                                                          |
    | 1 2 4 4 8                                                                |
    | if byte handling available with 4 bytes/word                             |
    +--------------------------------------------------------------------------+
    | 0 0 1 0 2                                                                |
    | if byte handling unavailable                                             |
    +--------------------------------------------------------------------------+

\ **SUBROUTINE CCPMVB(ARR1,I1,ARR2,I2,NTOMOV)**
    Move bytes from one non-character array to another if byte handling
    is available (assumed available).
    [is routine actually called? no reference in map routines]
    +--------------------------------------------------------------------------+
    | ARR1                                                                     |
    | (i/o)                                                                    |
    | (byte(\*))                                                               |
    | Array to which bytes are to be copied                                    |
    +--------------------------------------------------------------------------+
    | I1                                                                       |
    | (i)                                                                      |
    | (integer)                                                                |
    | The start byte number in ARR1 where bytes are to be copied               |
    +--------------------------------------------------------------------------+
    | ARR2                                                                     |
    | (i)                                                                      |
    | (byte(\*))                                                               |
    | Array from which bytes are to be copied                                  |
    +--------------------------------------------------------------------------+
    | I2                                                                       |
    | (i)                                                                      |
    | (integer)                                                                |
    | The start byte number in ARR2 from which the bytes are to be copied      |
    +--------------------------------------------------------------------------+
    | NTOMOV                                                                   |
    | (i)                                                                      |
    | (integer)                                                                |
    | The number of bytes to be copied                                         |
    +--------------------------------------------------------------------------+

\ **SUBROUTINE CCPMVI(ARR1,ARR2,NUM)**
    Assign the first NUM words of ARR2 to ARR1
    +--------------------------------------------------------------------------+
    | ARR1                                                                     |
    | (o)                                                                      |
    | (integer(\*))                                                            |
    +--------------------------------------------------------------------------+
    | ARR2                                                                     |
    | (i)                                                                      |
    | (integer(\*))                                                            |
    +--------------------------------------------------------------------------+
    | NUM                                                                      |
    | (i)                                                                      |
    | (integer)                                                                |
    +--------------------------------------------------------------------------+

\ **SUBROUTINE CCPMVR(ARR1,ARR2,NUM)**
    Assign the first NUM words of ARR2 to ARR1
    +--------------------------------------------------------------------------+
    | ARR1                                                                     |
    | (o)                                                                      |
    | (real(\*))                                                               |
    +--------------------------------------------------------------------------+
    | ARR2                                                                     |
    | (i)                                                                      |
    | (real(\*))                                                               |
    +--------------------------------------------------------------------------+
    | NUM                                                                      |
    | (i)                                                                      |
    | (integer)                                                                |
    +--------------------------------------------------------------------------+

\ **INTEGER FUNCTION CCPNUN()**
    Return a currently unconnected i/o unit number. Use this to select
    an arbitrary unit for i/o to avoid clashes with other code. (The
    value returned will be the same until the unit in question is OPENed
    or a lower-numbered one is CLOSEd.)
\ **LOGICAL FUNCTION CCPONL(IDUM)**
    See if program is being run interactively if this information is
    available. Tests if ft06 is tty.
    +--------------------------------------------------------------------------+
    | IDUM                                                                     |
    | (i)                                                                      |
    | (integer)                                                                |
    | Dummy parameter                                                          |
    +--------------------------------------------------------------------------+

    +--------------------------------------------------------------------------+
    | Returns                                                                  |
    | .TRUE.                                                                   |
    | if program is being run on-line                                          |
    +--------------------------------------------------------------------------+
    | .FALSE.                                                                  |
    | if batch mode or status unknown                                          |
    +--------------------------------------------------------------------------+

\ **SUBROUTINE CCPPSF(FILNAM,PATH,NAME,TYPE,VERS)**
    | Parse file name into components
    | Note: This routine contains machine dependent code

    +--------------------------------------------------------------------------+
    | FILNAM                                                                   |
    | (i)                                                                      |
    | (character\*(\*))                                                        |
    | file name string (no embedded blanks assumed)                            |
    +--------------------------------------------------------------------------+
    | PATH                                                                     |
    | (o)                                                                      |
    | (character\*(\*))                                                        |
    | returns path or, for VAX VMS the part of the file specification up to    |
    | the end of the directory specification (includes terminating ] or : or   |
    | /)                                                                       |
    | (blank if none)                                                          |
    +--------------------------------------------------------------------------+
    | NAME                                                                     |
    | (o)                                                                      |
    | (character\*(\*))                                                        |
    | returns name (blank if none)                                             |
    +--------------------------------------------------------------------------+
    | TYPE                                                                     |
    | (o)                                                                      |
    | (character\*(\*))                                                        |
    | returns file type/extension (blank if none)                              |
    +--------------------------------------------------------------------------+
    | VERS                                                                     |
    | (o)                                                                      |
    | (character\*(\*))                                                        |
    | returns the version (blank if none)                                      |
    +--------------------------------------------------------------------------+

    After the removal of the path and version parts of the string then,
    if there is at least one dot, the name is the string up to the last
    dot remaining and the type is the part of the string after the dot.
    If no dot is present then the remaining string is the name and the
    type is blank.

\ **SUBROUTINE CCPRCS(ILP,PROG,RCSDAT)**
    Print program name and date of current version (also prints run date
    if available).

    | Interface to `CCPVRS <#ccpvrs>`__ passing date in dd/mm/yy format
      (DATE). Takes RCS-format date string, e.g. '$Date$' or its form
      expanded with RCS option \`-kv', as for a CVS export, in which
      case it will have the leading \`$Date$' stripped. Initially,
      RCSDAT should be '$Date$'.
    | See the RCS documentation.

    +--------------------------------------------------------------------------+
    | ILP                                                                      |
    | (i)                                                                      |
    | (integer)                                                                |
    | Unit number of printer output                                            |
    +--------------------------------------------------------------------------+
    | PROG                                                                     |
    | (i)                                                                      |
    | (character\*(\*))                                                        |
    | Variable holding program name (max of 10 characters)                     |
    +--------------------------------------------------------------------------+
    | VDATE                                                                    |
    | (i)                                                                      |
    | (character\*(\*))                                                        |
    | Character variable holding RCS date string                               |
    +--------------------------------------------------------------------------+

\ **SUBROUTINE CSETNV(LNAME,FILNAM,ENAME,ETYPE,EXTN,ICOUNT,LSKIP)**
    Associate \`logical name' LNAME with value FILNAM using an
    environment variable LNAME. It is passed arrays of (name, type,
    extension) for ICOUNT number of name lines read from environ.def.
    Doesn't re-define existing name if LSKIP is true.
    +--------------------------------------------------------------------------+
    | LNAME                                                                    |
    | (i)                                                                      |
    | (character\*(\*))                                                        |
    | Logical name (environment variable).                                     |
    +--------------------------------------------------------------------------+
    | FILNAM                                                                   |
    | (i)                                                                      |
    | (character\*(\*))                                                        |
    | File name, if extension is omitted it is appended                        |
    +--------------------------------------------------------------------------+
    | ENAME                                                                    |
    | (i/o)                                                                    |
    | (character(150)\*20)                                                     |
    | list of environment variables; if LNAME is not in list it is appended    |
    | (also to ETYPE & EXTN arrays).                                           |
    +--------------------------------------------------------------------------+
    | ETYPE                                                                    |
    | (i/o)                                                                    |
    | (character(150)\*5)                                                      |
    | list of in/out types.                                                    |
    +--------------------------------------------------------------------------+
    | EXTN                                                                     |
    | (i/o)                                                                    |
    | (character(150)\*4)                                                      |
    | list of extensions.                                                      |
    +--------------------------------------------------------------------------+
    | ICOUNT                                                                   |
    | (i/o)                                                                    |
    | (integer)                                                                |
    | lengths of arrays ENAME, ETYPE & EXTN.                                   |
    +--------------------------------------------------------------------------+
    | LSKIP                                                                    |
    | (i)                                                                      |
    | (logical)                                                                |
    | if .TRUE. existing name not re-defined.                                  |
    +--------------------------------------------------------------------------+

\ **SUBROUTINE CCPPAG(IUN,NCOL,NLIN)**
    Return the number of columns and lines for a printer output page on
    a given fortran unit number if the information is available

    +--------------------------------------------------------------------------+
    | IUN                                                                      |
    | (i)                                                                      |
    | (integer)                                                                |
    | Fortran unit number                                                      |
    +--------------------------------------------------------------------------+
    | NCOL                                                                     |
    | (o)                                                                      |
    | (integer)                                                                |
    | number of columns in the page                                            |
    +--------------------------------------------------------------------------+
    | NLIN                                                                     |
    | (o)                                                                      |
    | (integer)                                                                |
    | number of lines in the page                                              |
    +--------------------------------------------------------------------------+

    Note: Returns 80,132 unless output is terminal whence 0,80.

\ **SUBROUTINE CCPSI2(IVAL,IA,N)**
    Store integer in N'th unsigned integer\*2 element (range 0-65535) of
    an array. To be implemented if `CCPBYT <#ccpbyt>`__ == .TRUE. Used
    in density modification routines.
    Note: no overflow check is done.
    +--------------------------------------------------------------------------+
    | IVAL                                                                     |
    | (i)                                                                      |
    | (integer)                                                                |
    | Unsigned integer value. Should be in range 0 to 65535                    |
    +--------------------------------------------------------------------------+
    | IA                                                                       |
    | (o)                                                                      |
    | (integer\*2(\*))                                                         |
    | Array into which the unsigned INTEGER\*2 value is to be inserted         |
    +--------------------------------------------------------------------------+
    | N                                                                        |
    | (i)                                                                      |
    | (integer)                                                                |
    | Position in IA where the unsigned INTEGER\*2 value is to be inserted     |
    +--------------------------------------------------------------------------+

\ **SUBROUTINE CCPSTB(IVAL,IA,N)**
    Store integer value, from 0 to 255, into N'th byte of array. To be
    implemented if `CCPBYT <#ccpbyt>`__ == .TRUE. Used in density
    modification routines.
    Note: no overflow check is done.
    +--------------------------------------------------------------------------+
    | IVAL                                                                     |
    | (i)                                                                      |
    | (integer)                                                                |
    | Unsigned integer, value from 0 to 255                                    |
    +--------------------------------------------------------------------------+
    | IA                                                                       |
    | (o)                                                                      |
    | (byte\*2(\*))                                                            |
    | Array into which the byte value is to be inserted                        |
    +--------------------------------------------------------------------------+
    | N                                                                        |
    | (i)                                                                      |
    | (integer)                                                                |
    | Position in IA where the byte value is to be inserted                    |
    +--------------------------------------------------------------------------+

\ **REAL FUNCTION CCPSUM(A,N,L)**
    Returns sum of every l'th element of a vector array
    +--------------------------------------------------------------------------+
    | A                                                                        |
    | (i)                                                                      |
    | (real(N))                                                                |
    | Array to be summed                                                       |
    +--------------------------------------------------------------------------+
    | N                                                                        |
    | (i)                                                                      |
    | (integer)                                                                |
    | dimension of the array                                                   |
    +--------------------------------------------------------------------------+
    | L                                                                        |
    | (i)                                                                      |
    | (integer)                                                                |
    | sum every L'th element                                                   |
    +--------------------------------------------------------------------------+

\ **SUBROUTINE CCPTIM(IFLAG,CPU,ELAPS)**
    Get CPU and Elapsed times
    +--------------------------------------------------------------------------+
    | IFLAG                                                                    |
    | (i/o)                                                                    |
    | (integer)                                                                |
    | == 0, initialise                                                         |
    | <> 0, return times (documented 1, also -1 for dummy call which is not    |
    | implemented)                                                             |
    +--------------------------------------------------------------------------+
    | CPU                                                                      |
    | (o)                                                                      |
    | (real)                                                                   |
    | cpu time in seconds                                                      |
    +--------------------------------------------------------------------------+
    | ELAPS                                                                    |
    | (o)                                                                      |
    | (real)                                                                   |
    | elapsed time in seconds                                                  |
    +--------------------------------------------------------------------------+

\ **SUBROUTINE CCPTOI(ARRAY,N,II,ITYP,IFAIL)**
    Extract n'th byte or INTEGER\*2 element from non-character (real)
    array to and store in an integer. It is used by the map file
    handling routines and must be implemented if map modes 0,1,3 or 5
    are to be used.
    Overflow check is performed.
    +--------------------------------------------------------------------------+
    | ARRAY                                                                    |
    | (i)                                                                      |
    | (real(\*))                                                               |
    | Real array containing the element to be extracted                        |
    +--------------------------------------------------------------------------+
    | N                                                                        |
    | (i)                                                                      |
    | (integer)                                                                |
    | Number of element to be converted                                        |
    +--------------------------------------------------------------------------+
    | II                                                                       |
    | (i)                                                                      |
    | (integer)                                                                |
    | extracted value                                                          |
    +--------------------------------------------------------------------------+
    | ITYP                                                                     |
    | (i)                                                                      |
    | (integer)                                                                |
    | Conversion type                                                          |
    | =1, byte to integer                                                      |
    | =2, integer\*2 to integer                                                |
    +--------------------------------------------------------------------------+
    | FAIL                                                                     |
    | (i/o)                                                                    |
    | (integer)                                                                |
    | On input                                                                 |
    | =0, Stop if conversion not available                                     |
    | =1, Return from subroutine always                                        |
    | On output                                                                |
    | unchanged if conversion carried out                                      |
    | =-1 if conversion not available                                          |
    +--------------------------------------------------------------------------+

\ **SUBROUTINE CCPUFL()**
    Suppress underflow error messages if required and if the facility is
    available.
    (Currently dummy.)
\ **SUBROUTINE CCPUPC(STRING)**
    Convert a string to upper case
    +--------------------------------------------------------------------------+
    | STRING                                                                   |
    | (i/o)                                                                    |
    | (character\*(\*))                                                        |
    | String to be converted                                                   |
    +--------------------------------------------------------------------------+

\ **SUBROUTINE CCPVRS(ILP,PROG,VDATE)**
    Print program version number and date header. Extended to include
    html tags.
    +--------------------------------------------------------------------------+
    | ILP                                                                      |
    | (i)                                                                      |
    | (integer)                                                                |
    | Unit number of printer output                                            |
    +--------------------------------------------------------------------------+
    | PROG                                                                     |
    | (i)                                                                      |
    | (character\*(\*))                                                        |
    | Variable holding program name (max of 10 characters)                     |
    +--------------------------------------------------------------------------+
    | VDATE                                                                    |
    | (i)                                                                      |
    | (character\*(\*))                                                        |
    | Character variable holding date of the current version as dd/mm/yy       |
    +--------------------------------------------------------------------------+

\ **ENTRY CCPPNM(NAME)**
    Entry point in `CCPVRS <#ccpvrs>`__
    Returns the program name previously set by CCPVRS (/CCPRCS); if that
    isn't set, use arg(0).
    +--------------------------------------------------------------------------+
    | NAME                                                                     |
    | (o)                                                                      |
    | (character\*(\*))                                                        |
    | Program name                                                             |
    +--------------------------------------------------------------------------+

\ **SUBROUTINE CCPZBI(ARR1,NUM)**
    Set NUM bytes of the array ARR1 to zero
    +--------------------------------------------------------------------------+
    | ARR1                                                                     |
    | (i/o)                                                                    |
    | (byte(\*))                                                               |
    +--------------------------------------------------------------------------+
    | NUM                                                                      |
    | (i)                                                                      |
    | (integer)                                                                |
    +--------------------------------------------------------------------------+

\ **SUBROUTINE CCPZI(IARR1,NUM)**
    Set NUM words of an integer array IARR1 to zero.
    +--------------------------------------------------------------------------+
    | IARR1                                                                    |
    | (i/o)                                                                    |
    | (integer(\*))                                                            |
    +--------------------------------------------------------------------------+
    | NUM                                                                      |
    | (i)                                                                      |
    | (integer)                                                                |
    +--------------------------------------------------------------------------+

\ **SUBROUTINE CCPZR(ARR1,NUM)**
    Set NUM words of a real array ARR1 to zero.
    +--------------------------------------------------------------------------+
    | ARR1                                                                     |
    | (i/o)                                                                    |
    | (real(\*))                                                               |
    +--------------------------------------------------------------------------+
    | NUM                                                                      |
    | (i)                                                                      |
    | (integer)                                                                |
    +--------------------------------------------------------------------------+

\ **CHARACTER FUNCTION FDIR(FILNAM)**
    Returns the directory part of a file name.
    Uses `CCPPSF <#ccppsf>`__.
    +--------------------------------------------------------------------------+
    | FILNAM                                                                   |
    | (i)                                                                      |
    | (character\*(\*))                                                        |
    | filename                                                                 |
    +--------------------------------------------------------------------------+

\ **CHARACTER FUNCTION FEXTN(FILNAM)**
    Returns the extension of a file name.
    Uses `CCPPSF <#ccppsf>`__.
    +--------------------------------------------------------------------------+
    | FILNAM                                                                   |
    | (i)                                                                      |
    | (character\*(\*))                                                        |
    | filename                                                                 |
    +--------------------------------------------------------------------------+

\ **CHARACTER FUNCTION FROOT(FILNAM)**
    Returns the root of a file name (file name minus an extension).
    Uses `CCPPSF <#ccppsf>`__.
    +--------------------------------------------------------------------------+
    | FILNAM                                                                   |
    | (i)                                                                      |
    | (character\*(\*))                                                        |
    | filename                                                                 |
    +--------------------------------------------------------------------------+

\ **LOGICAL FUNCTION LITEND(IDUM)**
    Determine endianness. Assumes integer\*4 as the default.
    +--------------------------------------------------------------------------+
    | Returns                                                                  |
    | .TRUE.                                                                   |
    | if little endian (VAX, FX2800, Ultrix, Convex)                           |
    +--------------------------------------------------------------------------+
    | .FALSE.                                                                  |
    | if big endian (IBM,IRIS,ESV)                                             |
    +--------------------------------------------------------------------------+

\ **INTEGER FUNCTION LENSTR(STRING)**
    Returns length of string to last non-space
    +--------------------------------------------------------------------------+
    | STRING                                                                   |
    | (i)                                                                      |
    | (character\*(\*)                                                         |
    +--------------------------------------------------------------------------+

\ **INTEGER FUNCTION LUNSTI(IDUM)**
    Returns the fortran standard input unit number (=5)
\ **INTEGER FUNCTION LUNSTO(IDUM)**
    Returns the fortran standard output unit number (=6)
\ **INTEGER FUNCTION NBITST(IWORD,LSB,NBITS)**
    Return the (unsigned) integer value held within a bit field in a
    word
    [for LAUE program?]
    +--------------------------------------------------------------------------+
    | IWORD                                                                    |
    | (i)                                                                      |
    | (integer)                                                                |
    | The word containing the bits to be examined                              |
    +--------------------------------------------------------------------------+
    | LSB                                                                      |
    | (i)                                                                      |
    | (integer)                                                                |
    | The least significant bit offset for the bit field                       |
    +--------------------------------------------------------------------------+
    | NBITS                                                                    |
    | (i)                                                                      |
    | (integer)                                                                |
    | The number of bits in the bit field (Must be less than the word length)  |
    +--------------------------------------------------------------------------+

\ **SUBROUTINE NOCRLF(LINE)**
    Output LINE supressing cr/lf.
    +--------------------------------------------------------------------------+
    | LINE                                                                     |
    | (i)                                                                      |
    | (character\*(\*))                                                        |
    | Line to output.                                                          |
    +--------------------------------------------------------------------------+

\ **SUBROUTINE QPRINT(IFLAG,MSG)**
    Conditionally print information. Normally, MSG is printed iff IFLAG
    is not greater than the \`reference' level for messages. This
    reference level is set to the value of IFLAG on the first call
    (which won't print anything).
    +--------------------------------------------------------------------------+
    | IFLAG                                                                    |
    | (i)                                                                      |
    | (integer)                                                                |
    | debug level 0-9 higher numbers give more output                          |
    +--------------------------------------------------------------------------+
    | MSG                                                                      |
    | (i)                                                                      |
    | (character\*(\*))                                                        |
    | the output message itself                                                |
    +--------------------------------------------------------------------------+

\ **ENTRY QPRLVL(LEVEL)**
    Entry point in `QPRINT <#qprint>`__
    Returns the current \`debug level' used by QPRINT in the integer
    output variable LEVEL.
\ **SUBROUTINE STBITS (IWORD,LSB,NBITS,IVAL)**
    Set a bit field within a word to a given (unsigned) integer value
    [for LAUE program?]
    +--------------------------------------------------------------------------+
    | IWORD                                                                    |
    | (i/o)                                                                    |
    | (integer)                                                                |
    | The word in which the bits are to be set                                 |
    +--------------------------------------------------------------------------+
    | LSB                                                                      |
    | (i)                                                                      |
    | (integer)                                                                |
    | The least significant bit offset for the bit field                       |
    +--------------------------------------------------------------------------+
    | NBITS                                                                    |
    | (i)                                                                      |
    | (integer)                                                                |
    | The number of bits in the bit field (must be less than the word length)  |
    +--------------------------------------------------------------------------+
    | IVAL                                                                     |
    | (I)                                                                      |
    | (integer)                                                                |
    | The unsigned integer value to be set in the bit field (The user should   |
    | ensure that this value will fit within the requested bit field)          |
    +--------------------------------------------------------------------------+

\ **LOGICAL FUNCTION CCPLIC(NAME)**
    Returns TRUE if it finds evidence that the CCP4 license
    has been agreed to. It call CCPERR (1) (fail) if not.
    +--------------------------------------------------------------------------+
    | NAME                                                                     |
    | (i)                                                                      |
    | (character\*(\*))                                                        |
    | Which license to test for. Only CCP4 currently works.                    |
    +--------------------------------------------------------------------------+

--------------
