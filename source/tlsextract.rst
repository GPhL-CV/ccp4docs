TLSEXTRACT (CCP4: Supported Program)
====================================

NAME
----

**tlsextract** - extracts the TLS group description from a PDB file and
outputs a TLS description file in RESTRAIN/REFMAC format

SYNOPSIS
--------

| **tlsextract XYZIN** *input.pdb* **TLSOUT** *output.tls*

DESCRIPTION
-----------

This is a small utility program to extract one or more TLS group
descriptions from the REMARK records in a PDB file, and produce the
equivalent description used for input by other programs in the CCP4
suite such as: REFMAC, TLSANL, and ANISOANL.

INPUT AND OUTPUT FILES
----------------------

Input
~~~~~

XYZIN
    Coordinate PDB file with TLS group descriptions contained in REMARK
    records.

Output
~~~~~~

TLSOUT
    TLS tensors for the groups extracted from XYZIN. Note that TLSOUT
    contains a REFMAC record to flag the file contains tensor elements
    in the order used by REFMAC, rather than the order used by RESTRAIN.

KEYWORDED INPUT
---------------

There are no keywords.

EXAMPLES
--------

::

    tlsextract XYZIN 8rxn.pdb TLSOUT 8rxn.out.tls

The file 8rxn.out.tls contains:

::

    REFMAC
     
    TLS
    RANGE  'A   1.' 'A  52.' ALL
    ORIGIN   -0.050  -8.314   6.938
    T     0.0024  0.0127  0.0068 -0.0055 -0.0003 -0.0005
    L     0.5888  3.2351  0.5202 -0.0931 -0.0369  0.0968
    S     0.0281 -0.0265  0.0324  0.5202  0.0968 -0.0931  0.0545 -0.0127

BUGS
----

The atom selection keywords for the TLS group descriptions are not
written to the PDB REMARK records, so this program assumes all atoms are
selected.

AUTHOR
------

-  Jay Painter *jpaint@u.washington.edu*
-  Ethan Merritt *merritt@u.washington.edu*

SEE ALSO
--------

-  `refmac5 <refmac5.html>`__
-  `tlsanl <tlsanl.html>`__
-  `anisoanl <anisoanl.html>`__
