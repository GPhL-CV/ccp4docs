refmac 5.5 bug removal and new features

5.5.0050
~~~~~~~~

1) Bug introduced in 5.5.0047 related with HKLOUT (output file would not
be written if labout is not defined) has been removed

2) Inconsistency between document and refmac about alt codes in the
external distances restraints removed. Now refmac can take ALTE and
ALTC. Document is consistent with the program

3) More info about external restraints has been added (distance, angle,
torsion, plane, chiral, interval restraints can be defined for any pair
or triple etc atoms)

5.5.0047
~~~~~~~~

1) Several compatibility issues for irix and windows were resolved. Now
it should compile on windows, however this website does not have
makefiles for windows yet.

2) A bug of user output columns not working correctly under certain
circumstances has been removed

3) A bug related to ANOM MAPOnly use possibly leading to crash has been
fixed

4) Refmac can output F+,F-,SIGF+,SIGF- to the output mtz file
optionally: those of these that appear in both LABI and LABO will appear
in output mtz

5.5.0046
~~~~~~~~

Now unobserved reflections in the output mtz file also have free R flag
(equal to zero). It should make programs using this output file happier

5.5.0045
~~~~~~~~

1) Ligands con, com, prn were broken. Now they should be ok.

2) Added a keyword: weight auto adjust <yes\|no > If the value us yes
then weight will be adjusted to make sure that rmsbonds are around 0.02
or so. 'yes' is default value.

5.5.0044

1) Now the program can read intensities only (i.e. if sigmas for
intensities are not available they may not be defined). This option
should be avoided. However for some cases from pdb only intensities are
available. This option has been added to deal with such cases.

Line minimisation temporary disabled

5.5.0043
~~~~~~~~

1) A bug related with FAN and DELFAN maps has been removed. Now the
program produces anomalous and different anomalous maps if requested
(ANOM MAPOnly) or SAD refinement is performed.

Now refmac with twin refinement prints out all equivalent twin
operators. It should help comparing twin laws derived by different
programs.

2) A keyword allowing refinement of occupancies of anomalous scatterers
has been added. If the following keyword: REFINE OREFINE ANOMALOUS

then occupancies of anomalous scatterers will be refined

3) A keyword added that controls the level of small twin domains. The
keyword TWIN FILTerlevel <value>

will give a signal to the program that all domains with twin fraction
less than given value should be removed. After removal of these domain
the program will make sure that twin and crystallographic symmetry
operators together form a group. Default value is 0.05

5.5.0042
~~~~~~~~

1) A feature has been added: If the keyword ANOM MAPOnly

as well F+ and F- have been specified in the labin statement then refmac
will produce anomalous difference as well as difference anomalous maps.
Map coefficients are:

m DANO\_o and m DANO\_o - D DANO\_c

DANO\_o and DANO\_c are observed and calculated anomolous differences. m
is figure of merit (expected phase error) and D is the maximum
likelihood scale parameter that is related with <(cos(2 π s Δ x) >
(expectation value) with s a reciprocal space vector Δ x is error in
positional parameters

These map coefficients are calculated by default if SAD refinement is
performed, i.e. (i) F+ and F- have been given, (ii) coordinate file
contains anomolous scatterers and (iii) wavelength (either in mtz file
or using ANOM wave keyword) or ANOM FORM f' f'' statements have been
defined.

2) TLS refinement has been stabilised further

3) If keyword TLSO ADDU keyword is given then the program output
coordinate file will contain anisotropic U values calculated from TLS
parameters and isotropic equivalent of TLS contribution will be added to
B values. At this stage this keyword should be used for visualisation
and analysis purposes. Output file file thus produced should not be used
in the next stage of refinement

4) Bugs related with premature termination of refinement has been
removed

**5.5.0040**

An introduced bug affecting SAD refinement has been removed

**5.5.0039**

A bug related with the high B values of non-tls groups has been removed

Automatic definition of domains as segments or chains sometimes had some
problem

Stability of the multidimensional integration to derive the posterior
expectation values for twin and intensity based maximum likelihood
refinement has been improved

**5.5.0038**

Bug related with addition of non-tls group to one of the groups has been
removed

**5.5.0037**

If auto weighting is specified (note that it is default) then the
program will try to make sure that zbond is between 0.2 and 1.0.

If the input pdb file had anisou card and isotropic refinement is
performed then the output file contained aniosu card with only the first
element non-zero

Refinement exits if function value starts to increase.

Inaccuracy in mask based solvent contribution calculations has been
corrected. Now R/Rfree should look better.

If ligands are close to one of the tls groups they are not included in
these groups. Only metals and solvent molecules are added to TLS groups.
Note that it is users' responsibility to make sure that all atoms in the
asymmetric unit belong to one or another tls group.

**5.5.0036**

For some groups small twin domains were not removed correctly. Now they
are removed correctly. The program makes sure that space group symmetry
together with twin symmetry form a group

An option added not to add waters to tls groups. New keyword is:
tlsdetails waters exclude

A little bit better option for autoweighting

--------------

`Garib Murshudov <mailto:garib@deltora.local>`__

Last modified: Sun Aug 7 23:14:54 BST 2008
